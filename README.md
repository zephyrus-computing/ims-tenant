# IMS-Tenant
The Tenant add-on module for IMS provides tenancy for the application by extending the object schema and grouping objects together by Organizations. Users can belong to multiple Organizations but each object can only be assigned to a single Organization. This also removes all Anonymous access to IMS since there are new, more strict, permissions for all objects.

![List view of Organization](docs/ims-tenant-admin-organization-list-1.0.PNG)

![Detailed view of Organization](docs/ims-tenant-admin-organization-detail-1.0.PNG)

![List view of UserOrganization](docs/ims-tenant-admin-userorganization-list-1.0.PNG)

![User Profile view](docs/ims-tenant-profile-1.0.PNG)

![List view of Tenant Storage](docs/ims-tenant-storage-list-1.0.PNG)

![List view of Tenant Storage Hierarchy](docs/ims-tenant-hierarch-list-1.0.PNG)

![List view of Tenant Kit](docs/ims-tenant-kit-list-1.0.PNG)

![New Tenant Part Admin view](docs/ims-tenant-admin-new-part-1.0.PNG)

![New Tenant PartStorage Admin view](docs/ims-tenant-admin-new-partstorage1-1.0.PNG)

![New Tenant PartStorage Admin view2](docs/ims-tenant-admin-new-partstorage2-1.0.PNG)


## Installation
Quick installation instructions can be found in the [Quick Install](https://gitlab.com/zephyrus-computing/ims-tenant/-/wikis/Quick-Install) Wiki section.

Detailed instructions can be found in the [Admin Guide](https://gitlab.com/zephyrus-computing/ims-tenant/-/wikis/admin-guide) Wiki section.

The Debian package can be downloaded from the [Package Registry](https://gitlab.com/zephyrus-computing/ims-tenant/-/packages).

## Roadmap

- Updates from the base IMS application that need to be rewritten for IMS-Tenant use

- Security patching as necessary

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### Development Environment Setup
```
git clone https://gitlab.com/zephyrus-computing/inventory-management-system.git ims
cd ims
./dev.sh setup
cd ..
git clone https://gitlab.com/zephyrus-computing/ims-tenant.git ims-tenant
cd ims-tenant
./dev-tenant.sh setup
source venv-tenant/bin/activate
```
The dev.sh script with the setup option will configure a python virtual environment and install the packages specified in requirements.txt, create a dev db, and execute the superuser creation process. The dev-tenant.sh script will layer on the IMS-Tenant module to the IMS instance.

When you have finished your work, you can run ```deactivate``` to close the environment.

If you need to reset your environment, run the following:
```
./dev-tenant.sh delete
./dev.sh delete
./dev.sh setup
./dev-tenant.sh setup
```

## License
[GPL3](http://choosealicense.com/licenses/gpl-3.0/)
