#! /bin/bash
list=(setup delete help)
if [[ ! " ${list[*]} " =~ " ${1} " ]]; then
    echo -e "\e[31mInvalid option.\e[39m"
    echo $0 "[${list[*]}] [-v|--verbose]"
    exit
fi
[[ $2 == "-v" || $2 == "--verbose" ]] && verbose=1
if [[ $1 == "help" ]]; then
    echo "$0 help	This helpful text"
    echo "$0 setup	Create the virtual environment for development"
    echo "$0 delete	Delete th created virtual environment"
    echo ""
fi
folder=$(dirname $0)
parentfolder=$(dirname $folder)
if [ $1 == "setup" ]; then
    [ $verbose ] && echo -e "\e[32mChecking for virtualenv...\e[39m"
    if [[ $(which virtualenv) == "" ]]; then
        [ $verbose ] && echo -e "\e[32mChecking OS...\e[39m"
        release=$(lsb_release -i)
        if [[ "$release" =~ "Ubuntu" ]]; then
            [ $verbose ] && echo -e "\e[32mDetected Ubuntu\e[39m"
            sudo apt -y install virtualenv
        elif [[ "$release" =~ "Debian" ]]; then
            [ $verbose ] && echo -e "\e[32mDetected Debian\e[39m"
            su root -c apt -y install virtualenv
        else
            echo -e "\e[31mUnknown OS: $release\nPlease contact support for assistance.\e[39m"
            exit
        fi
    fi
    [ $verbose ] && echo -e "\e[32mChecking for IMS source code\e[39m"
    if [ -d "$parentfolder/ims" ]; then
        [ $verbose ] && echo -e "\e[32mCreating Virtual Environment\e[39m"
        virtualenv "$folder/venv"
        source "$folder/venv/bin/activate"
	[ $verbose ] && echo -e "\e[32mUpdating pip, setuptools, and wheel\e[39m"
        pip install -U pip setuptools wheel
        [ $verbose ] && echo -e "\e[32mInstalling required packages\e[39m"
        pip install -r "$parentfolder/ims/requirements.txt"
        export DJANGO_DEVENV=True
        export DJANGO_DEBUG=True
        [ $verbose ] && echo -e "\e[32mCreating soft links\e[39m"
        ln -s "$folder/ims/templates/tenant" "$parentfolder/ims/ims/templates/tenant"
        ln -s "$folder/ims/ims/addons/tenant" "$parentforlder/ims/ims/ims/addons/tenant"
        [ $verbose ] && echo -e "\e[32mUpdating conf files\e[39m"
        if [ ! $(grep "'name': 'Tenant'" "$parentfolder/ims/ims/ims/config/addons.conf") ]; then
            today=$(date +%y%m%d)
            cp -f "$parentfolder/ims/ims/ims/config/addons.conf" "$parentfolder/ims/ims/ims/config/addons.conf.orig"
            awk '/INSTALLED_ADDONS/ {go=1}
                go && /\135/ { print "    \173\047name\047: \047Tenant\047, \047version\047: \0471.0-dev\047, \047build\047: \047$today\047, \047enabled\047: True, \047admin_url\047: \047tenant\057admin\057\047, \047api_url\047: \047api\057v1\057tenant\057\047, \047other_urls\047: \133\173\047path\047: \047tenant\057\047, \047include\047: \047ims.addons.tenant.urls\047\175\135\175,\n\135; go=0; next}1' "$parentfolder/ims/ims/ims/config/addons.conf.orig" > "$parentfolder/ims/ims/ims/config/addons.conf"
            rm -f "$parentfolder/ims/ims/ims/config/addons.conf.orig"
        fi
        if [ ! $(grep 'ims.addons.tenant' "$parentfolder/ims/ims/ims/config/app.conf") ]; then
            cp -f "$parentfolder/ims/ims/ims/config/app.conf" "$parentfolder/ims/ims/ims/config/app.conf.orig"
            awk '/INSTALLED_APPS/ {go=1}
                go && /\135/ { print "    \047ims.addons.tenant\047,\n\135"; go=0; next}1' "$parentfolder/ims/ims/ims/config/app.conf.orig" > "$parentfolder/ims/ims/ims/config/app.conf"
            rm -f "$parentfolder/ims/ims/ims/config/app.conf.orig"
        fi
        [ $verbose ] && echo -e "\e[32mRemoving url redirects\e[39m"
        if [ $(grep "from inventory import views" "$parentfolder/ims/ims/inventory/urls.py") ]; then
            sed -i -e 's/from inventory import views/from ims.addons.tenant import views/' "$parentfolder/ims/ims/inventory/urls.py"
        fi
        if [ $(grep "from inventory.admin import views" "$parentfolder/ims/ims/inventory/admin/urls.py") ]; then
            sed -i -e 's/from inventory.admin import views/from ims.addons.tenant.admin import views/' "$parentfolder/ims/ims/inventory/admin/urls.py"
        fi
        if [ $(grep "from inventory.api import views" "$parentfolder/ims/ims/inventory/api/urls.py") ]; then
            sed -i -e 's/from inventory.api import views/from ims.addons.tenant.api import views/' "$parentfolder/ims/ims/inventory/api/urls.py"
        fi
        [ $verbose ] && echo -e "\e[32mGenerating new settings file\e[39m"
        python "$parentfolder/ims/ims/ims/config/generate.py"
        deactive
        [ $verbose ] && echo -e "\e[32mSetup complete. Run 'source venv/bin/activate' to begin working.\nNOTE: To launch the local dev server, run 'python $parentfolder/ims/ims/manage.py runserver'\e[39m"
    else
        [ $verbose ] && echo -e "\e[31mUnable to locate IMS source code\nPlease contact support for assistance.\e[39m"
        exit
    fi
fi

if [ $1 == "delete" ]; then
    [ $verbose ] && echo -e "\e[32mDeleting virtual environment...\e[39m"
    rm -rf "$folder/venv"
    [ $verbose ] && echo -e "\e[32mDeletion soft links...\e[39m"
    rm "$parentfolder/ims/ims/templates/tenant"
    rm "$parentfolder/ims/ims/ims/addons/tenant"
    [ $verbose ] && echo -e "\e[32mRemoving addons config\e[39m"
    if [ $(grep "'name': 'Tenant'" "$parentfolder/ims/ims/ims/config/addons.conf") ]; then
        cp -f "$parentfolder/ims/ims/ims/config/addons.conf" "$parentfolder/ims/ims/ims/config/addons.conf.orig"
        awk '/INSTALLED_ADDONS/ {go=1}
            go && /\173\047name\047: \047Tenant\047/ {next; go=0}1' "$parentfolder/ims/ims/ims/config/addons.conf.orig" > "$parentfolder/ims/ims/ims/config/addons.conf"
        rm -f "$parentfolder/ims/ims/ims/config/addons.conf.orig"
    fi
    [ $verbose ] && echo -e "\e[32mRemoving url redirects\e[39m"
    if [ $(grep "from ims.addons.tenant import views" "$parentfolder/ims/ims/inventory/urls.py") ]; then
        sed -i -e 's/from ims.addons.tenant import views/from inventory import views/' "$parentfolder/ims/ims/inventory/urls.py"
    fi
    if [ $(grep "from ims.addons.tenant.admin import views" "$parentfolder/ims/ims/inventory/admin/urls.py") ]; then
        sed -i -e 's/from ims.addons.tenant.admin import views/from inventory.admin import views/' "$parentfolder/ims/ims/inventory/admin/urls.py"
    fi
    if [ $(grep "from ims.addons.tenant.api import views" "$parentfolder/ims/ims/inventory/api/urls.py") ]; then
        sed -i -e 's/from ims.addons.tenant.api import views/from inventory.api import views/' "$parentfolder/ims/ims/inventory/api/urls.py"
    fi
    [ $verbose ] && echo -e "\e[32mGenerating new settings file\e[39m"
    python3 "$parentfolder/ims/ims/ims/config/generate.py"
    [ $verbose ] && echo -e "\e[32mDeletion Complete...\e[39m"
fi

