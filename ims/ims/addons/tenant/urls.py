from django.urls import re_path
from ims.addons.tenant import views

app_name='ims.addons.tenant'
urlpatterns = [
    re_path(r'^/$', views.part_list, name='home'),
    re_path(r'^ajax/(?P<model>[\w]+)/$', views.ajax, name='ajax_call'),
]
