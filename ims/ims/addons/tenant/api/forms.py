from django import forms
from ims.addons.tenant.models import Organization, UserOrganization

class OrganizationForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = ('name','description','parent','enabled')

class UserOrganizationForm(forms.ModelForm):
    class Meta:
        model = UserOrganization
        fields = ('user','organization')
