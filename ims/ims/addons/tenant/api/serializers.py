from django.contrib.auth.models import User
from rest_framework import serializers
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from ims.addons.tenant.models import Organization, UserOrganization

class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ('id','name','description','parent')

    def create(self, validated_data):
        return Organization.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.parent = validated_data.get('parent', instance.parent)
        instance.save()
        return instance

class UserOrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserOrganization
        fields = ('id','user','organization')

    def create(self, validated_data):
        return UserOrganization.objects.create(**validated_data)
    
    def update(self, instance, validated_data):
        instance.user = validated_data.get('user', instance.user)
        instance.organization = validated_data.get('organization', instance.organization)
        instance.save()
        return instance

class UserOrganizationReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserOrganization
        fields = ('id','user','organization')
        depth = 1

class PartReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Part
        fields = ('id','name','description','sku','price','cost','tenant')
        depth = 1

class StorageReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Storage
        fields = ('id','name','description','parent','tenant')
        depth = 1

class AlternateSKUReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlternateSKU
        fields = ('id','manufacturer','sku','tenant')
        depth = 1

class PartAlternateSKUReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartAlternateSKU
        fields = ('id','part','alt_sku','tenant')
        depth = 1

class PartStorageReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorage
        fields = ('id','part','storage','count','tenant')
        depth = 1

class AssemblyReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assembly
        fields = ('id','parent','part','count','tenant')
        depth = 1
        
class KitReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kit
        fields = ('id','name','description','sku','price','tenant')
        depth = 1

class KitPartStorageReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitPartStorage
        fields = ('id','kit','partstorage','count','tenant')
        depth = 1
