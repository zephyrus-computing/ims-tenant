from django.urls import re_path
from ims.addons.tenant.api import views

app_name = 'ims.addons.tenant.api'
urlpatterns = [
    re_path(r'^organization/$', views.OrganizationList.as_view(), name='organization_list'),
    re_path(r'^organization/(?P<id>[\.\d]+)/$', views.OrganizationDetail.as_view(), name='organization_detail'),
    re_path(r'^userorganization/$', views.UserOrganizationList.as_view(), name='UserOrganization_list'),
    re_path(r'^userorganization/(?P<id>[\.\d]+)/$', views.UserOrganizationDetail.as_view(), name='UserOrganization_detail'),
]

