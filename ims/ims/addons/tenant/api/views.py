from django.conf import settings
from django.core.exceptions import FieldError
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
import logging

from inventory.api.serializers import PartSerializer, StorageSerializer, AlternateSKUSerializer, PartAlternateSKUSerializer, PartStorageSerializer, AssemblySerializer, KitSerializer, KitPartStorageSerializer, PartStoragePartCountSerializer, PartStorageStorageCountSerializer, AssemblyPartCountSerializer, AssemblyParentCountSerializer, KitPartStoragePartStorageSerializer, KitPartStorageKitSerializer
from inventory.models import Part, Storage, AlternateSKU, PartStorage, PartAlternateSKU, Assembly, Kit, KitPartStorage, get_sentinel_part, get_sentinel_sku, get_sentinel_storage, get_sentinel_partstorage, get_sentinel_kit, IsInt
from inventory.search import Search
from ims.addons.tenant.api.serializers import PartReadSerializer, OrganizationSerializer, UserOrganizationSerializer, StorageReadSerializer, AlternateSKUReadSerializer, PartAlternateSKUReadSerializer, PartStorageReadSerializer, AssemblyReadSerializer, KitReadSerializer, KitPartStorageReadSerializer
from ims.addons.tenant.models import Organization, UserOrganization, TenantPart, TenantStorage, TenantAlternateSKU, TenantPartAlternateSKU, TenantPartStorage, TenantAssembly, TenantKit, TenantKitPartStorage
from log.logger import getLogger
import ims.addons.tenant.helpers as helpers

logger = logging.getLogger(__name__)
logger2 = getLogger('inventory-api')
logger3 = getLogger('tenant-api')

# Part API Views
class PartList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        return GetListAPIView(cname, request)
    def post(self, request):
        cname = self.__class__.__name__
        return PostListAPIView(cname, request)

class PartDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        return GetDetailAPIView(cname, request, id)
    def put(self, request, id):
        cname = self.__class__.__name__
        return PutDetailAPIView(cname, request, id)
    def patch(self, request, id):
        cname = self.__class__.__name__
        return PatchDetailAPIView(cname, request, id)
    def delete(self, request, id):
        cname = self.__class__.__name__
        return DeleteDetailAPIView(cname, request, id)

class PartDetailCount(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Part')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        orgs = helpers.get_user_organizations(request.user)
        if not part.tenant.organization in orgs:
            logger.info('User not authorized to view Part')
            logger2.warning(request.user, 'User not authorized to view Part id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Accessed {} id {}'.format(cname.replace('DetailCount',''), id))
        data = {}
        data['count'] = part.get_total()
        return Response(data, status.HTTP_200_OK)

class PartDetailComponent(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Part')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Assembly')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Part Component List'
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        orgs = helpers.get_user_organizations(request.user)
        if not part.tenant.organization in orgs:
            logger.info('User not authorized to view Part')
            logger2.warning(request.user, 'User not authorized to view Part id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Generating list of Part')
        assemblies = part.get_components()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Part with query')
            sf = Search().filter(['part__name', 'part__sku', 'part__description'], query)
            assemblies = assemblies.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Part with query on field "{}"'.format(field))
                sf = Search().filter(['part__{}'.format(field)], value)
                assemblies = assemblies.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-part__{}'.format(sort[1:])
                else:
                    order = 'part__{}'.format(sort)
                assemblies = assemblies.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Part based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Part based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        assemblies = assemblies[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(AssemblyPartCountSerializer(assemblies, many=True).data, status.HTTP_200_OK)

class PartDetailAssembly(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Part')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Assembly')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Part Assembly List'
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        orgs = helpers.get_user_organizations(request.user)
        if not part.tenant.organization in orgs:
            logger.info('User not authorized to view Part')
            logger2.warning(request.user, 'User not authorized to view Part id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Generating list of Part')
        assemblies = part.get_parent_assemblies()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Part with query')
            sf = Search().filter(['parent__name', 'parent__sku', 'parent__description'], query)
            assemblies = assemblies.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Part with query on field "{}"'.format(field))
                sf = Search().filter(['parent__{}'.format(field)], value)
                assemblies = assemblies.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    sort = sort[1:]
                    order = '-parent__{}'.format(sort)
                else:
                    order = 'parent__{}'.format(sort)
                assemblies = assemblies.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Part based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Part based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        assemblies = assemblies[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(AssemblyParentCountSerializer(assemblies, many=True).data, status.HTTP_200_OK)

class PartDetailStorage(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Part')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Storage')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Part Storage List'
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        orgs = helpers.get_user_organizations(request.user)
        if not part.tenant.organization in orgs:
            logger.info('User not authorized to view Part')
            logger2.warning(request.user, 'User not authorized to view Part id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Generating list of Storage')
        partstorages = part.get_storage()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Storage with query')
            sf = Search().filter(['storage__name', 'storage__description'], query)
            partstorages = partstorages.filter(sf)
        for field in ['name','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Storage with query on field "{}"'.format(field))
                sf = Search().filter(['storage__{}'.format(field)], value)
                partstorages = partstorages.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-storage__{}'.format(sort[1:])
                else:
                    order = 'storage__{}'.format(sort)
                partstorages = partstorages.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Storage based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Storage based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        partstorages = partstorages[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(PartStorageStorageCountSerializer(partstorages, many=True).data, status.HTTP_200_OK)

class PartDetailKit(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Part')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Kit')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Part Storage List'
        logger.debug('Getting requested Part')
        part = get_object_or_404(Part, id=id)
        orgs = helpers.get_user_organizations(request.user)
        if not part.tenant.organization in orgs:
            logger.info('User not authorized to view Part')
            logger2.warning(request.user, 'User not authorized to view Part id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Generating list of Kit')
        kits = part.get_parent_kits()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Kit with query')
            sf = Search().filter(['kit__name', 'kit__sku', 'kit__description'], query)
            kits = kits.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Kit with query on field "{}"'.format(field))
                sf = Search().filter(['kit__{}'.format(field)], value)
                kits = kits.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-kit__{}'.format(sort[1:])
                else:
                    order = 'kit__{}'.format(sort)
                kits = kits.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Kit based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Kit based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        kits = kits[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(KitPartStorageKitSerializer(kits, many=True).data, status.HTTP_200_OK)

class PartDetailKitAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, partid, kitid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, kitid))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Part')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Checking and converting "inc" to int')
        try:
            inc = int(inc)
        except Exception as err:
            logger2.error(request.user, str(err))
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(inc),
                },
                status.HTTP_400_BAD_REQUEST
            )
        if not request.user.has_perm(helpers.get_admin_permission('inventory','increment','Kit')):
            logger.debug('User does not have Increment permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested Kit object for adding')
        kit = get_object_or_404(Kit, id=kitid)
        orgs = helpers.get_user_organizations(request.user)
        if not kit.tenant.organization in orgs:
            logger.info('User not authorized to view Kit')
            logger2.warning(request.user, 'User not authorized to view Kit id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Adding {} to Kit id {}'.format(inc, kit.id))
        kit.add(inc)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Added {} to Kit id {}; current count: {}'.format(inc, kitid, kit.get_available_count()))
        return Response(
            {
                'message': 'Part counts have been incremented for {} number of Kit id {}'.format(inc, kit.id),
                'kit': KitSerializer(kit).data,
            },
            status.HTTP_200_OK
        )

class PartDetailKitSub(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, partid, kitid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, kitid))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Part')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Checking and converting "inc" to int')
        try:
            inc = int(inc)
        except Exception as err:
            logger2.error(request.user, str(err))
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(inc),
                },
                status.HTTP_400_BAD_REQUEST
            )
        if not request.user.has_perm(helpers.get_admin_permission('inventory','decrement','Kit')):
            logger.debug('User does not have Decrement permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested Kit object for subtracting')
        kit = get_object_or_404(Kit, id=kitid)
        orgs = helpers.get_user_organizations(request.user)
        if not kit.tenant.organization in orgs:
            logger.info('User not authorized to view Kit')
            logger2.warning(request.user, 'User not authorized to view Kit id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        try:
            logger.debug('Subtracting {} from Kit id {}'.format(inc, kit.id))
            kit.subtract(inc)
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger2.info((request.user or 0), 'Subtracted {} from Kit id {}; current count: {}'.format(inc, kitid, kit.get_available_count()))
            return Response(
                {
                    'message': 'Part counts have been decremented for {} number of Kit id {}'.format(inc, kit.id),
                    'kit': KitSerializer(kit).data,
                },
                status.HTTP_200_OK
            )
        except ValueError:
            logger.info('Cannot subtract {} from Kit id {}. Insufficient amount available'.format(inc, kit.id))
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning((request.user or 0), 'Insufficient parts to subtract {} from Kit id {}'.format(inc, kitid))
            return Response(
                {
                    'message': 'Kit cannot be decremented by {}, there is not enough parts available.'.format(inc),
                    'kit': KitSerializer(kit).data
                },
                status.HTTP_409_CONFLICT
            )

# Storage API Views
class StorageList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        return GetListAPIView(cname, request)
    def post(self, request):
        cname = self.__class__.__name__
        return PostListAPIView(cname, request)

class StorageDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        return GetDetailAPIView(cname, request, id)
    def put(self, request, id):
        cname = self.__class__.__name__
        return PutDetailAPIView(cname, request, id)
    def patch(self, request, id):
        cname = self.__class__.__name__
        return PatchDetailAPIView(cname, request, id)
    def delete(self, request, id):
        cname = self.__class__.__name__
        return DeleteDetailAPIView(cname, request, id)

class StorageDetailParts(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API'.format(cname))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Storage')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Part')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Storage Detail Parts'
        logger.debug('Getting requested Storage')
        storage = get_object_or_404(Storage, id=id)
        orgs = helpers.get_user_organizations(request.user)
        if not storage.tenant.organization in orgs:
            logger.info('User not authorized to view Storage')
            logger2.warning(request.user, 'User not authorized to view Storage id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        partstorages = storage.get_parts_storage()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Part with query')
            sf = Search().filter(['part__name','part__sku','part__description'], query)
            partstorages = partstorages.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Part with query on field "{}"'.format(field))
                sf = Search().filter(['part__{}'.format(field)], value)
                partstorages = partstorages.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-part__{}'.format(sort[1:])
                else:
                    order = 'part__{}'.format(sort)
                partstorages = partstorages.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Part based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Part based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        partstorages = partstorages[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(PartStoragePartCountSerializer(partstorages, many=True).data, status.HTTP_200_OK)

# AlternateSKU API Views
class AlternateSKUList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        return GetListAPIView(cname, request)
    def post(self, request):
        cname = self.__class__.__name__
        return PostListAPIView(cname, request)

class AlternateSKUDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        return GetDetailAPIView(cname, request, id)
    def put(self, request, id):
        cname = self.__class__.__name__
        return PutDetailAPIView(cname, request, id)
    def patch(self, request, id):
        cname = self.__class__.__name__
        return PatchDetailAPIView(cname, request, id)
    def delete(self, request, id):
        cname = self.__class__.__name__
        return DeleteDetailAPIView(cname, request, id)

# PartAlternateSKU API Views
class PartAlternateSKUList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        return GetListAPIView(cname, request)
    def post(self, request):
        cname = self.__class__.__name__
        return PostListAPIView(cname, request)

class PartAlternateSKUDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        return GetDetailAPIView(cname, request, id)
    def put(self, request, id):
        cname = self.__class__.__name__
        return PutDetailAPIView(cname, request, id)
    def patch(self, request, id):
        cname = self.__class__.__name__
        return PatchDetailAPIView(cname, request, id)
    def delete(self, request, id):
        cname = self.__class__.__name__
        return DeleteDetailAPIView(cname, request, id)

# PartStorage API Views
class PartStorageList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        return GetListAPIView(cname, request)
    def post(self, request):
        cname = self.__class__.__name__
        return PostListAPIView(cname, request)

class PartStorageDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        return GetDetailAPIView(cname, request, id)
    def put(self, request, id):
        cname = self.__class__.__name__
        return PutDetailAPIView(cname, request, id)
    def patch(self, request, id):
        cname = self.__class__.__name__
        return PatchDetailAPIView(cname, request, id)
    def delete(self, request, id):
        cname = self.__class__.__name__
        return DeleteDetailAPIView(cname, request, id)
    
class PartStorageAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id, count):
        cname = self.__class__.__name__
        model = 'PartStorage'
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug(request.user, 'GET Request to {} API for id {}'.format(cname, id))
        if not helpers.is_int(count):
            logger.debug('Provided value "{}" is not valid'.format(count))
            logger2.warning(request.user, 'Invalid value for incrementing part count')
            return Response({}, status.HTTP_400_BAD_REQUEST)
        if not request.user.has_perm(helpers.get_admin_permission('inventory','increment',model)):
            logger.debug('User does not have Increment permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested {}'.format(model))
        orgs = helpers.get_user_organizations(request.user)
        try:
            obj = helpers.get_object(model, id)
        except Exception as err:
            logger2.warning(request.user, str(err))
            return Response({}, status.HTTP_404_NOT_FOUND)
        if not obj.tenant.organization in orgs:
            logger.info('User not authorized to view {}.'.format(model))
            logger2.warning(request.user, 'User not authorized to view {} id {}'.format(model, id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Adding {} to PartStorage {}'.format(count, obj))
        obj.add(count)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Added {} to PartStorage id {}'.format(count, obj.id))
        return Response(
            {
                'message': 'Part count has been incremented by {} for PartStorage id {}.'.format(count, obj.id),
                'current_count': obj.count
            }, 
            status.HTTP_200_OK
        )

class PartStorageSub(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id, count):
        cname = self.__class__.__name__
        model = 'PartStorage'
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug(request.user, 'GET Request to {} API for id {}'.format(cname, id))
        if not helpers.is_int(count):
            logger.debug('Provided value "{}" is not valid'.format(count))
            logger2.warning(request.user, 'Invalid value for decrementing part count')
            return Response({}, status.HTTP_400_BAD_REQUEST)
        if not request.user.has_perm(helpers.get_admin_permission('inventory','decrement',model)):
            logger.debug('User does not have Decrement permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested {}'.format(model))
        orgs = helpers.get_user_organizations(request.user)
        try:
            obj = helpers.get_object(model, id)
        except Exception as err:
            logger2.warning(request.user, str(err))
            return Response({}, status.HTTP_404_NOT_FOUND)
        if not obj.tenant.organization in orgs:
            logger.info('User not authorized to view {}.'.format(model))
            logger2.warning(request.user, 'User not authorized to view {} id {}'.format(model, id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Subtracting {} from PartStorage {}'.format(count, obj))
        try:
            obj.subtract(count)
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger2.info(request.user, 'Subtracted {} from PartStorage id {}'.format(count, obj.id))
            return Response(
                {
                    'message': 'Part count has been decremented by {} for PartStorage id {}.'.format(count, obj.id),
                    'current_count': obj.count
                }, 
                status.HTTP_200_OK
            )
        except ValueError:
            logger.info('Cannot subtract {} from PartStorage id {}. Insufficient amount available'.format(count, obj.id))
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning(request.user, 'Insufficient parts to subtract {} from PartStorage id {}'.format(count, obj.id))
            return Response(
                {
                    'message': 'Part count cannot be decremented by {}, there is not enough in PartStorage id {}.'.format(count, obj.id),
                    'current_count': obj.count
                }, 
                status.HTTP_409_CONFLICT
            )

# Assembly API Views
class AssemblyList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        return GetListAPIView(cname, request)
    def post(self, request):
        cname = self.__class__.__name__
        return PostListAPIView(cname, request)

class AssemblyDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        return GetDetailAPIView(cname, request, id)
    def put(self, request, id):
        cname = self.__class__.__name__
        return PutDetailAPIView(cname, request, id)
    def patch(self, request, id):
        cname = self.__class__.__name__
        return PatchDetailAPIView(cname, request, id)
    def delete(self, request, id):
        cname = self.__class__.__name__
        return DeleteDetailAPIView(cname, request, id)

class AssemblyDetailParts(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API'.format(cname))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Assembly')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Part')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Assembly Detail Parts'
        logger.debug('Getting requested Assembly')
        assembly = get_object_or_404(Assembly, id=id)
        orgs = helpers.get_user_organizations(request.user)
        if not assembly.tenant.organization in orgs:
            logger.info('User not authorized to view Assembly')
            logger2.warning(request.user, 'User not authorized to view Assembly id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        assemblies = assembly.get_related_parts()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of Part with query')
            sf = Search().filter(['part__name','part__sku','part__description'], query)
            assemblies = assemblies.filter(sf)
        for field in ['name','sku','description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of Part with query on field "{}"'.format(field))
                sf = Search().filter(['part__{}'.format(field)], value)
                assemblies = assemblies.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-part__{}'.format(sort[1:])
                else:
                    order = 'part__{}'.format(sort)
                assemblies = assemblies.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort Part based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort Part based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        assemblies = assemblies[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(AssemblyPartCountSerializer(assemblies, many=True).data, status.HTTP_200_OK)

# Kit API Views
class KitList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        return GetListAPIView(cname, request)        
    def post(self, request):
        cname = self.__class__.__name__
        return PostListAPIView(cname, request)

class KitDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        return GetDetailAPIView(cname, request, id)
    def put(self, request, id):
        cname = self.__class__.__name__
        return PutDetailAPIView(cname, request, id)
    def patch(self, request, id):
        cname = self.__class__.__name__
        return PatchDetailAPIView(cname, request, id)
    def delete(self, request, id):
        cname = self.__class__.__name__
        return DeleteDetailAPIView(cname, request, id)
    
class KitDetailCount(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Kit')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested Kit')
        kit = get_object_or_404(Kit, id=id)
        orgs = helpers.get_user_organizations(request.user)
        if not kit.tenant.organization in orgs:
            logger.info('User not authorized to view Kit')
            logger2.warning(request.user, 'User not authorized to view Kit id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Accessed {} id {}'.format(cname.replace('DetailCount',''), id))
        data = {}
        data['count'] = kit.get_available_count()
        return Response(data, status.HTTP_200_OK)

class KitDetailPartStorage(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','Kit')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if not request.user.has_perm(helpers.get_admin_permission('inventory','view','KitPartStorage')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed Kit Detail PartStorage'
        logger.debug('Getting requested Kit')
        kit = get_object_or_404(Kit, id=id)
        orgs = helpers.get_user_organizations(request.user)
        if not kit.tenant.organization in orgs:
            logger.info('User not authorized to view Kit')
            logger2.warning(request.user, 'User not authorized to view Kit id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        kitpartstorages = kit.get_components()
        query = request.GET.get('search')
        if query != None:
            logmsg += ' query={},'.format(query)
            logger.debug('Filtering list of PartStorage with query')
            sf = Search().filter(['partstorage__part__name', 'partstorage__part__sku', 'partstorage__part__description', 'partstorage__storage__name', 'partstorage__storage__description'], query)
            kitpartstorages = kitpartstorages.filter(sf)
        for field in ['part__name', 'part__sku', 'part__description', 'storage__name', 'storage__description']:
            value = request.GET.get(field)
            if value != None:
                logmsg += ' {}={},'.format(field, value)
                logger.debug('Filtering list of PartStorage with query on field "{}"'.format(field))
                sf = Search().filter(['partstorage__{}'.format(field)], value)
                kitpartstorages = kitpartstorages.filter(sf)
        sort = request.GET.get('sort')
        if sort != None:
            try:
                if sort[0] == '-':
                    order = '-partstorage__{}'.format(sort[1:])
                else:
                    order = 'partstorage__{}'.format(sort)
                kitpartstorages = kitpartstorages.order_by(order)
                logmsg += ' sort={},'.format(sort)
            except FieldError:
                logger.warning('Unable to sort PartStorage based on field "{}"'.format(sort))
                logger2.warning(request.user, 'Unable to sort PartStorage based on field "{}"'.format(sort))
        limit = request.GET.get('limit')
        if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
        else: limit = int(limit)
        offset = request.GET.get('offset')
        if not helpers.is_int(offset): offset = 0
        else: offset = int(offset)
        limit += offset
        logmsg += ' limit={}, offset={}'.format(limit, offset)
        kitpartstorages = kitpartstorages[offset:limit]
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return Response(KitPartStoragePartStorageSerializer(kitpartstorages, many=True).data, status.HTTP_200_OK)

class KitAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, kitid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug((request.user or 0), 'GET Request to {} API for id {}'.format(cname, kitid))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','increment','Kit')):
            logger.debug('User does not have Increment permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Checking and converting "inc" to int')
        try:
            inc = int(inc)
        except Exception as err:
            logger2.error(request.user, str(err))
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(inc),
                },
                status.HTTP_400_BAD_REQUEST
            )
        logger.debug('Getting requested Kit object for adding')
        kit = get_object_or_404(Kit, id=kitid)
        orgs = helpers.get_user_organizations(request.user)
        if not kit.tenant.organization in orgs:
            logger.info('User not authorized to work with Kit.')
            logger2.warning(request.user, 'User not authorized to work with Kit id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Adding {} to Kit id {}'.format(inc, kit.id))
        kit.add(inc)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info((request.user or 0), 'Added {} to Kit id {}; current count: {}'.format(inc, kitid, kit.get_available_count()))
        return Response(
            {
                'message': 'Part counts have been incremented for {} number of Kit id {}'.format(inc, kit.id),
                'kit': KitSerializer(kit).data,
            },
            status.HTTP_200_OK
        )
    
class KitSub(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, kitid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug(request.user, 'GET Request to {} API for id {}'.format(cname, kitid))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','decrement','Kit')):
            logger.debug('User does not have Decrement permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Checking and converting "inc" to int')
        try:
            inc = int(inc)
        except Exception as err:
            logger2.error(request.user, str(err))
            return Response(
                {
                    'message': 'Unable to process the value "{}"'.format(inc),
                },
                status.HTTP_400_BAD_REQUEST
            )
        logger.debug('Getting requested Kit object for subtracting')
        orgs = helpers.get_user_organizations(request.user)
        kit = get_object_or_404(Kit, id=kitid)
        if not kit.tenant.organization in orgs:
            logger.info('User not authorized to work with Kit.')
            logger2.warning(request.user, 'User not authorized to work with Kit id {}'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        try:
            logger.debug('Subtracting {} from Kit id {}'.format(inc, kit.id))
            kit.subtract(inc)
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger2.info(request.user, 'Subtracted {} from Kit id {}'.format(inc, kitid))
            return Response(
                {
                    'message': 'Part counts have been decremented for {} number of Kit id {}'.format(inc, kit.id),
                    'kit': KitSerializer(kit).data,
                },
                status.HTTP_200_OK
            )
        except ValueError:
            logger.info('Cannot subtract {} from Kit id {}. Insufficient amount available'.format(inc, kit.id))
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning(request.user, 'Insufficient parts to subtract {} from Kit id {}'.format(inc, kitid))
            return Response(
                {
                    'message': 'Kit cannot be decremented by {}, there is not enough parts available.'.format(inc),
                    'kit': KitSerializer(kit).data
                },
                status.HTTP_409_CONFLICT
            )

# KitPartStorage API Views
class KitPartStorageList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        return GetListAPIView(cname, request)
    def post(self, request):
        cname = self.__class__.__name__
        return PostListAPIView(cname, request)

class KitPartStorageDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        return GetDetailAPIView(cname, request, id)
    def put(self, request, id):
        cname = self.__class__.__name__
        return PutDetailAPIView(cname, request, id)
    def patch(self, request, id):
        cname = self.__class__.__name__
        return PatchDetailAPIView(cname, request, id)
    def delete(self, request, id):
        cname = self.__class__.__name__
        return DeleteDetailAPIView(cname, request, id)

# Action API Views
class PartAdd(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, stoid, parid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug(request.user, 'GET Request to {} API for Part id {} and Storage id {}'.format(cname, parid, stoid))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','increment','PartStorage')):
            logger.debug('User does not have Increment permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested PartStorage object for adding')
        orgs = helpers.get_user_organizations(request.user)
        part = get_object_or_404(Part, id=parid)
        if not part.tenant.organization in orgs:
            logger.info('User not authorized to work with Part.')
            logger2.warning(request.user, 'User not authorized to work with Part id {}'.format(part.id))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        storage = get_object_or_404(Storage, id=stoid)
        if not storage.tenant.organization in orgs:
            logger.info('User not authorized to work with Storage.')
            logger2.warning(request.user, 'User not authorized to work with Storage id {}'.format(storage.id))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        if part.tenant.organization != storage.tenant.organization:
            logger.info('Organization between Part and Storage does not match.')
            logger2.warning(request.user, 'Storage id {} and Part id {} Organizations do not match'.format(storage.id,part.id))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        container = PartStorage.objects.filter(part=part, storage=storage, tenant__organization=part.tenant.organization).first()
        if container == None:
            logger.info('HTTP_404_NOT_FOUND response for {} APIView'.format(cname))
            logger2.warning(request.user, 'PartStorage for Part id {} and Storage id {} Not Found'.format(parid,stoid))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        logger.debug('Adding {} to Part {} in Storage {}'.format(inc, part.name, storage.name))
        container.add(inc)
        logger.debug('Saving changes to PartStorage id {}'.format(container.id))
        container.save()
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Added {} to PartStorage id {}'.format(inc, container.id))
        return Response(
            {
                'message': 'Part count has been incremented by {} for Part {} in Storage {}.'.format(inc, part.name, storage.name),
                'part': PartSerializer(part).data,
                'storage': StorageSerializer(storage).data,
                'current_count': str(container.count)
            }, 
            status.HTTP_200_OK
        )

class PartSub(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, stoid, parid, inc):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger2.debug(request.user, 'GET Request to {} API for Part id {} and Storage id {}'.format(cname, parid, stoid))
        if not request.user.has_perm(helpers.get_admin_permission('inventory','decrement','PartStorage')):
            logger.debug('User does not have Decrement permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested PartStorage object for subtracting')
        orgs = helpers.get_user_organizations(request.user)
        part = get_object_or_404(Part, id=parid)
        if not part.tenant.organization in orgs:
            logger.info('User not authorized to work with Part.')
            logger2.warning(request.user, 'User not authorized to work with Part id {}'.format(part.id))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        storage = get_object_or_404(Storage, id=stoid)
        if not storage.tenant.organization in orgs:
            logger.info('User not authorized to work with Storage.')
            logger2.warning(request.user, 'User not authorized to work with Storage id {}'.format(storage.id))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        if part.tenant.organization != storage.tenant.organization:
            logger.info('Organization between Part and Storage does not match.')
            logger2.warning(request.user, 'Storage id {} and Part id {} Organizations do not match'.format(storage.id,part.id))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        container = PartStorage.objects.filter(part=part, storage=storage, tenant__organization=part.tenant.organization).first()
        if container == None:
            logger.info('HTTP_404_NOT_FOUND response for {} APIView'.format(cname))
            logger2.warning(request.user, 'PartStorage for Part id {} and Storage id {} Not Found'.format(parid,stoid))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
        try:
            logger.debug('Subtracting {} from Part {} in Storage {}'.format(inc, part.name, storage.name))
            container.subtract(inc)
            logger.debug('Saving changes to PartStorage id {}'.format(container.id))
            container.save()
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger2.info(request.user, 'Subtracted {} to PartStorage id {}'.format(inc, container.id))
            return Response(
                {
                    'message': 'Part count has been decremented by {} for Part {} in Storage {}.'.format(inc, part.name, storage.name),
                    'part': PartSerializer(part).data,
                    'storage': StorageSerializer(storage).data,
                    'current_count': str(container.count)
                }, 
                status.HTTP_200_OK
            )
        except ValueError:
            logger.info('Cannot subtract {} from Part {} in Storage {}. Insufficient amount available'.format(inc, part.name, storage.name))
            logger.info('HTTP_409_CONFLICT response for {} APIView'.format(cname))
            logger.debug('Responding (409) to request for {} APIView'.format(cname))
            logger2.warning(request.user, 'Insufficient parts to subtract {} from PartStorage id {}'.format(inc, container.id))
            return Response(
                {
                    'message': 'Part count cannot be decremented by {}, there is not enough of Part {} in Storage {}.'.format(inc, part.name, storage.name),
                    'part': PartSerializer(part).data,
                    'storage': StorageSerializer(storage).data,
                    'current_count': str(container.count)
                }, 
                status.HTTP_409_CONFLICT
            )

class OrganizationList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'GET Request to {} API'.format(cname))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','view','Organization')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed {} API'.format(cname)
        logger.debug('Generating list of Organization')
        if request.user.is_staff:
            orgs = Organization.objects.all()
        else:
            objs = helpers.get_user_organizations(request.user)
            orgs = Organization.objects.filter(id__in=objs)
        query = request.GET.get('search')
        if query != None:
            logmsg = '{} with query {}'.format(logmsg, query)
            logger.debug('Filtering list of Kit with query')
            sf = Search().filter(['name', 'description'], query)
            orgs = orgs.filter(sf)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger3.info(request.user, logmsg)
        return Response(OrganizationSerializer(orgs, many=True).data, status.HTTP_200_OK)
    def post(self, request):
        cname = self.__class__.__name__
        logger.debug('POST request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'GET Request to {} API'.format(cname))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','add','Organization')):
            logger.debug('User does not have Add permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if request.user.is_staff:
            logger.debug('Serializing data in request')
            serializer = OrganizationSerializer(data=request.data)
            if serializer.is_valid():
                logger.debug('Serializer is valid')
                logger.info('Creating new Organization "{}"'.format(serializer.validated_data.get('name')))
                org = serializer.save()
                logger.debug('Responding (201) to request for {} APIView'.format(cname))
                logger3.info(request.user, 'Created Organization id {}'.format(org.id))
                return Response(serializer.data, status.HTTP_201_CREATED)
            logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            logger3.warning(request.user, 'Bad POST Request to {} API'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        logger.info('HTTP_403_FORBIDDEN response for {} APIView'.format(cname))
        logger.debug('Responding (403) to request for {} APIView'.format(cname))
        logger3.warning(request.user, 'User is not Staff and attempt to POST to {} API'.format(cname))
        return Response({}, status.HTTP_403_FORBIDDEN)

class OrganizationDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','view','Organization')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        orgs = helpers.get_user_organizations(request.user)
        try:
            if request.user.is_staff or orgs.get(id=id):
                logger.debug('Getting requested Organization')
                org = get_object_or_404(Organization, id=id)
                logger.debug('Responding (200) to request for {} APIView'.format(cname))
                logger3.info(request.user, 'Accessed Organization id {}'.format(id))
                return Response(OrganizationSerializer(org).data, status.HTTP_200_OK)
            else:
                logger.info('User not authorized to view Organization.')
                logger3.warning(request.user, 'User not authorized to view Organization id {}'.format(id))
                logger.debug('Responding (403) to request for {} APIView'.format(cname))
                return Response({}, status.HTTP_403_FORBIDDEN)
        except Organization.DoesNotExist:
            logger.info('Requested Organization does not exist')
            logger3.warning(request.user, 'Requested Organization id {} does not exist'.format(id))
            logger.debug('Responding (404) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_404_NOT_FOUND)
    def put(self, request, id):
        cname = self.__class__.__name__
        logger.debug('PUT request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'PUT Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','change','Organization')):
            logger.debug('User does not have Change permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if request.user.is_staff:
            org = get_object_or_404(Organization, id=id)
            logger.debug('Serializing data in request')
            serializer = OrganizationSerializer(org, data=request.data)
            if serializer.is_valid():
                logger.debug('Serializer is valid')
                serializer.save()
                logger.info('PUT request successfully updated Organization id {}'.format(id))
                logger.debug('Responding (200) to request for {} APIView'.format(cname))
                logger3.info(request.user, 'Updated {} for id {}'.format(cname, id))
                return Response(serializer.data, status.HTTP_200_OK)
            logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            logger3.warning(request.user, 'Bad PUT Request to {} API for id {}'.format(cname, id))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        else:
            logger.info('User not authorized to modify Organization.')
            logger3.warning(request.user, 'User not authorized to modify Organization id {}'.format(id))
            logger.debug('Responding (403) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
    def patch(self, request, id):
        cname = self.__class__.__name__
        logger.debug('PATCH request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'PATCH Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','change','Organization')):
            logger.debug('User does not have Change permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if request.user.is_staff:
            org = get_object_or_404(Organization, id=id)
            logger.debug('Serializing data in request')
            serializer = OrganizationSerializer(org, data=request.data, partial=True)
            if serializer.is_valid():
                logger.debug('Serializer is valid')
                serializer.save()
                logger.info('PATCH request successfully updated Organization id {}'.format(id))
                logger.debug('Responding (200) to request for {} APIView'.format(cname))
                logger3.info(request.user, 'Updated {} for id {}'.format(cname, id))
                return Response(serializer.data, status.HTTP_200_OK)
            logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            logger3.warning(request.user, 'Bad PATCH Request to {} API for id {}'.format(cname, id))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        else:
            logger.info('User not authorized to modify Organization.')
            logger.debug('Responding (403) to request for {} APIView'.format(cname))
            logger3.warning(request.user, 'User not authorized to modify Organization id {}'.format(id))
            return Response({}, status.HTTP_403_FORBIDDEN)
    def delete(self, request, id):
        cname = self.__class__.__name__
        logger.debug('DELETE request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'DELETE Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','delete','Organization')):
            logger.debug('User does not have Delete permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if request.user.is_staff:
            org = get_object_or_404(Organization, id=id)
            logger.debug('DELETE request for Organization id {}'.format(id))
            org.delete()
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger3.info(request.user, 'Deleted Organization with id {}'.format(id))
            return Response(
                {
                    'message': 'Organization has successfully been deleted.',
                },
                status.HTTP_200_OK
            )
        else:
            logger.info('User not authorized to delete Organization.')
            logger3.warning(request.user, 'User not authorized to delete Organization id {}'.format(id))
            logger.debug('Responding (403) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)

class UserOrganizationList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'GET Request to {} API'.format(cname))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','view','UserOrganization')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logmsg = 'Accessed {} API'.format(cname)
        logger.debug('Generating list of UserOrganization')
        if request.user.is_staff:
            userorgs = UserOrganization.objects.all()
        else:
            objs = helpers.get_user_organizations(request.user)
            userorgs = UserOrganization.objects.filter(organization__in=objs)
        query = request.GET.get('search')
        if query != None:
            logmsg = '{} with query {}'.format(logmsg, query)
            logger.debug('Filtering list of UserOrganization with query')
            sf = Search().filter(['organization__name', 'organization__description', 'user__username', 'user__first_name', 'user__last_name', 'user__email'], query)
            userorgs = userorgs.filter(sf)
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger3.info(request.user, logmsg)
        return Response(UserOrganizationSerializer(userorgs, many=True).data, status.HTTP_200_OK)
    def post(self, request):
        cname = self.__class__.__name__
        logger.debug('POST request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'POST Request to {} API'.format(cname))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','add','UserOrganization')):
            logger.debug('User does not have Add permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if request.user.is_staff:
            logger.debug('Serializing data in request')
            serializer = UserOrganizationSerializer(data=request.data)
            if serializer.is_valid():
                logger.debug('Serializer is valid')
                userorg = UserOrganization.objects.filter(user=serializer.validated_data.get('user'),organization=serializer.validated_data.get('organization')).first()
                if userorg is None:
                    logger.info('Creating new UserOrganization "{}-{}"'.format(serializer.validated_data.get('user'), serializer.validated_data.get('organization')))
                    userorg = serializer.save()
                    logger.debug('Responding (201) to request for {} APIView'.format(cname))
                    logger3.info(request.user, 'Created UserOrganization id {}'.format(userorg.id))
                    return Response(serializer.data, status.HTTP_201_CREATED)
                logger.debug('Responding (200) to request for {} APIView'.format(cname))
                logger3.info(request.user, 'UserOrganization already exists with id {}'.format(userorg.id))
                return Response(serializer.data, status.HTTP_200_OK)
            logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
            logger.debug('Responding (400) to request for {} APIView'.format(cname))
            logger3.warning(request.user, 'Bad POST Request to {} API'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        logger.info('HTTP_403_FORBIDDEN response for {} APIView'.format(cname))
        logger.debug('Responding (403) to request for {} APIView'.format(cname))
        logger3.warning(request.user, 'User is not Staff and attempt to POST to {} API'.format(cname))
        return Response({}, status.HTTP_403_FORBIDDEN)

class UserOrganizationDetail(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, id):
        cname = self.__class__.__name__
        logger.debug('GET request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'GET Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','view','UserOrganization')):
            logger.debug('User does not have View permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        logger.debug('Getting requested UserOrganization')
        orgs = helpers.get_user_organizations(request.user)
        userorg = get_object_or_404(UserOrganization, id=id)
        if request.user.is_staff or userorg.organization in orgs:
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger3.info(request.user, 'Accessed {} API with id {}'.format(cname, id))
            return Response(UserOrganizationSerializer(userorg).data, status.HTTP_200_OK)
        else:
            logger.info('User not authorized to view UserOrganization.')
            logger3.warning(request.user, 'User not authorized to view UserOrganization id {}'.format(id))
            logger.debug('Responding (403) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
    def delete(self, request, id):
        cname = self.__class__.__name__
        logger.debug('DELETE request made to {} APIView'.format(cname))
        logger3.debug(request.user, 'DELETE Request to {} API for id {}'.format(cname, id))
        if not request.user.has_perm(helpers.get_admin_permission('tenant','delete','UserOrganization')):
            logger.debug('User does not have Delete permissions to {} APIView'.format(cname))
            logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        if request.user.is_staff:
            userorg = get_object_or_404(UserOrganization, id=id)
            logger.debug('DELETE request for UserOrganization id {}'.format(id))
            userorg.delete()
            logger.debug('Responding (200) to request for {} APIView'.format(cname))
            logger3.info(request.user, 'Deleted UserOrganization with id {}'.format(id))
            return Response(
                {
                    'message': 'UserOrganization has successfully been deleted.',
                },
                status.HTTP_200_OK
            )
        else:
            logger.info('User not authorized to delete UserOrganization.')
            logger3.warning(request.user, 'User not authorized to delete UserOrganization id {}'.format(id))
            logger.debug('Responding (403) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_403_FORBIDDEN)
        
def GetListAPIView(cname, request):
    model = cname.replace('List','').replace('Detail','')
    logger.debug('GET request made to {} APIView'.format(cname))
    logger2.debug((request.user or 0), 'GET Request to {} API'.format(cname))
    if not request.user.has_perm(helpers.get_admin_permission('inventory','view',model)):
        logger.debug('User does not have View permissions to {} APIView'.format(cname))
        logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logmsg = 'Accessed {} List'.format(model)
    logger.debug('Generating list of {}'.format(model))
    orgs = helpers.get_user_organizations(request.user)
    objects = helpers.get_object_list(model, orgs)
    fields = helpers.get_search_fields(model)
    query = request.GET.get('search')
    if query != None:
        logmsg += ' search={},'.format(query)
        logger.debug('Filtering list of {} with query'.format(model))
        sf = Search().filter(fields, query)
        objects = objects.filter(sf)
    for field in fields:
        value = request.GET.get(field)
        if value != None:
            logmsg += ' {}={},'.format(field, value)
            logger.debug('Filtering list of {} with query on field "{}"'.format(model, field))
            sf = Search().filter([field], value)
            objects = objects.filter(sf)
    sort = request.GET.get('sort')
    if sort != None:
        try:
            objects = objects.order_by(sort)
            logmsg += ' sort={},'.format(sort)
        except FieldError:
            logger.warning('Unable to sort {} based on field "{}"'.format(model, sort))
            logger2.warning(request.user, 'Unable to sort {} based on field "{}"'.format(model, sort))
    limit = request.GET.get('limit')
    if not helpers.is_int(limit): limit = settings.DEFAULT_API_RESULTS
    else: limit = int(limit)
    offset = request.GET.get('offset')
    if not helpers.is_int(offset): offset = 0
    else: offset = int(offset)
    limit += offset
    logmsg += ' limit={}, offset={}'.format(limit, offset)
    objects = objects[offset:limit]
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info((request.user or 0), logmsg)
    return Response(helpers.get_readserializer_data(model, objects, True), status.HTTP_200_OK)

def PostListAPIView(cname, request):
    model = cname.replace('List','').replace('Detail','')
    logger.debug('POST request made to {} APIView'.format(cname))
    logger2.debug(request.user, 'POST Request to {} API'.format(cname))
    if not request.user.has_perm(helpers.get_admin_permission('inventory','add',model)):
        logger.debug('User does not have Add permissions to {} APIView'.format(cname))
        logger2.warning(request.user, 'Access Denied for POST Request to {} API'.format(cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Serializing data in request')
    serializer = helpers.get_serializer(model, request.data)
    if serializer.is_valid():
        logger.debug('Serializer is valid')
        org = ''
        orgs = helpers.get_user_organizations(request.user)
        try:
            logger.debug('Obtaining Organization info')
            org = _get_organization(serializer.initial_data['organization'])
        except KeyError:
            logger.info('KeyError while trying to get Organization; organization not included in request')
            logger2.warning(request.user, 'Bad POST Request to {} API. \'Organization not included in request\''.format(cname))
            logger.debug('Responsing (400) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        if not org in orgs:
            logger.info('User is not authorized to work with Organization')
            logger2.warning(request.user, 'User is not authorized to work with Organization id {}'.format(org.id))
            logger.debug('Responsing (400) to request for {} APIView'.format(cname))
            return Response({}, status.HTTP_400_BAD_REQUEST)
        name = ''
        if model == 'Storage' or model == 'Assembly':
            parent = serializer.validated_data.get('parent')
            if parent is not None:
                if not parent.tenant.organization in orgs:
                    logger.info('User not authorized to work with parent {}.'.format(parent.__class__.__name__))
                    logger2.warning(request.user, 'User not authorized to work with parent {} id {}'.format(parent.__class__.__name__, parent.id))
                    logger.debug('Responding (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
        if model == 'PartAlternateSKU' or model == 'PartStorage' or model == 'Assembly':
            part = serializer.validated_data.get('part')
            if part.tenant.organization != org:
                logger.info('User is not authorized to work with Part')
                logger2.warning(request.user, 'User is not authorized to work with Part id {}'.format(part.id))
                logger.debug('Responsing (400) to request for {} APIView'.format(cname))
                return Response({}, status.HTTP_400_BAD_REQUEST)
        if model == 'PartAlternateSKU':
            pa = PartAlternateSKU.objects.filter(part=serializer.validated_data.get('part'), alt_sku=serializer.validated_data.get('alt_sku'), tenant__organization=org).first()
            if pa == None:
                name = '{}-{}'.format(serializer.validated_data.get('part'), serializer.validated_data.get('alt_sku'))
                altsku = serializer.validated_data.get('alt_sku')
                if altsku.tenant.organization != org:
                    logger.info('User is not authorized to work with AlternateSKU')
                    logger2.warning(request.user, 'User is not authorized to work with AlternateSKU id {}'.format(altsku.id))
                    logger.debug('Responsing (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
            else:
                logger.debug('Responding (200) to request for {} APIView'.format(cname))
                logger2.info(request.user, 'PartAlternateSKU Already Exists with id {}'.format(pa.id))
                return Response(serializer.data, status.HTTP_200_OK)
        if model == 'PartStorage':
            ps = PartStorage.objects.filter(part=serializer.validated_data.get('part'), storage=serializer.validated_data.get('storage'), tenant__organization=org).first()
            if ps == None:
                name = '{}-{}'.format(serializer.validated_data.get('storage'), serializer.validated_data.get('part'))
                storage = serializer.validated_data.get('storage')
                if storage.tenant.organization != org:
                    logger.info('User is not authorized to work with Storage')
                    logger2.warning(request.user, 'User is not authorized to work with Storage id {}'.format(storage.id))
                    logger.debug('Responsing (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
                part = serializer.validated_data.get('part')
                if part.tenant.organization != org:
                    logger.info('User is not authorized to work with Part')
                    logger2.warning(request.user, 'User is not authorized to work with Part id {}'.format(part.id))
                    logger.debug('Responsing (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
            else:
                logger.debug('Responding (200) to request for {} APIView'.format(cname))
                logger2.info(request.user, 'PartStorage Already Exists with id {}'.format(ps.id))
                return Response(serializer.data, status.HTTP_200_OK)
        if model == 'Assembly':
            assembly = Assembly.objects.filter(parent=serializer.validated_data.get('parent'), part=serializer.validated_data.get('part'), tenant__organization=org).first()
            if assembly == None:
                name = '{}-{}'.format(serializer.validated_data.get('parent'), serializer.validated_data.get('part'))
                parent = serializer.validated_data.get('parent')
                if parent.tenant.organization != org:
                    logger.debug('User is not authorized to work with parent Part')
                    logger2.warning(request.user, 'User is not authorized to work with parent Part id {}'.format(parent.id))
                    logger.debug('Responsing (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
            else:
                logger.debug('Responding (200) to request for {} APIView'.format(cname))
                logger2.info(request.user, 'Assembly Already Exists with id {}'.format(assembly.id))
                return Response(serializer.data, status.HTTP_200_OK)
        if model == 'KitPartStorage':
            kps = KitPartStorage.objects.filter(kit=serializer.validated_data.get('kit'), partstorage=serializer.validated_data.get('partstorage'), tenant__organization=org).first()
            if kps == None:
                name = '{}-{}'.format(serializer.validated_data.get('kit'), serializer.validated_data.get('partstorage'))
                kit = serializer.validated_data.get('kit')
                if kit.tenant.organization != org:
                    logger.info('User is not authorized to work with Kit')
                    logger2.warning(request.user, 'User is not authorized to work with Kit id {}'.format(kit.id))
                    logger.debug('Responsing (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
                ps = serializer.validated_data.get('partstorage')
                if ps.tenant.organization != org:
                    logger.info('User is not authorized to work with PartStorage')
                    logger2.warning(request.user, 'User is not authorized to work with PartStorage id {}'.format(ps.id))
                    logger.debug('Responsing (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
            else:
                logger.debug('Responding (200) to request for {} APIView'.format(cname))
                logger2.info(request.user, 'KitPartStorage Already Exists with id {}'.format(kps.id))
                return Response(serializer.data, status.HTTP_200_OK)
        if name == '':
            name = serializer.validated_data.get('name')
        logger.info('Creating new {} "{}" for Organization id {}'.format(model, name, org.id))
        obj = serializer.save()
        helpers.create_tenant_object(obj, org)
        logger.debug('Responding (201) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Created {} id {}'.format(model, obj.id))
        return Response(serializer.data, status.HTTP_201_CREATED)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad POST Request to {} API. \'Form invalid\''.format(cname))
    return Response({}, status.HTTP_400_BAD_REQUEST)

def GetDetailAPIView(cname, request, id):
    model = cname.replace('List','').replace('Detail','')
    logger.debug('GET request made to {} APIView'.format(cname))
    logger2.debug(request.user, 'GET Request to {} API for id {}'.format(cname, id))
    if not request.user.has_perm(helpers.get_admin_permission('inventory','view',model)):
        logger.debug('User does not have View permissions to {} APIView'.format(cname))
        logger2.warning(request.user, 'Access Denied for GET Request to {} API'.format(cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model))
    orgs = helpers.get_user_organizations(request.user)
    try:
        obj = helpers.get_object(model, id)
    except Exception as err:
        logger2.warning(request.user, str(err))
        return Response({}, status.HTTP_404_NOT_FOUND)
    if not obj.tenant.organization in orgs:
        logger.info('User not authorized to view {}.'.format(model))
        logger2.warning(request.user, 'User not authorized to view {} id {}'.format(model, id))
        logger.debug('Responding (404) to request for {} APIView'.format(cname))
        return Response({}, status.HTTP_404_NOT_FOUND)
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info(request.user, 'Accessed {} API for id {}'.format(cname, id))
    return Response(helpers.get_readserializer_data(model, obj), status.HTTP_200_OK)

def PutDetailAPIView(cname, request, id):
    model = cname.replace('List','').replace('Detail','')
    logger.debug('PUT request made to {} APIView'.format(cname))
    logger2.debug(request.user, 'PUT Request to {} API for id {}'.format(cname, id))
    if not request.user.has_perm(helpers.get_admin_permission('inventory','change',model)):
        logger.debug('User does not have Change permissions to {} APIView'.format(cname))
        logger2.warning(request.user, 'Access Denied for PUT Request to {} API'.format(cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {} to PUT'.format(model))
    orgs = helpers.get_user_organizations(request.user)
    try:
        obj = helpers.get_object(model, id)
    except Exception as err:
        logger2.warning(request.user, str(err))
        return Response({}, status.HTTP_404_NOT_FOUND)
    if not obj.tenant.organization in orgs:
        logger.info('User not authorized to modify {}.'.format(model))
        logger2.warning(request.user, 'User not authorized to modify {} id {}'.format(model, id))
        logger.debug('Responding (404) to request for {} APIView'.format(cname))
        return Response({}, status.HTTP_404_NOT_FOUND)
    serializer = helpers.get_serializer(obj, request.data)
    if serializer.is_valid():
        if model == 'Storage' or model == 'Assembly':
            parent = serializer.validated_data.get('parent')
            if parent is not None:
                if not parent.tenant.organization in orgs:
                    logger.info('User not authorized to work with parent {}.'.format(parent.__class__.__name__))
                    logger2.warning(request.user, 'User not authorized to work with parent {} id {}'.format(parent.__class__.__name__, parent.id))
                    logger.debug('Responding (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
        if model in ['PartAlternateSKU', 'PartStorage', 'Assembly']:
            part = serializer.validated_data.get('part')
            if not part.tenant.organization in orgs:
                logger.info('User not authorized to work with Part.')
                logger2.warning(request.user, 'User not authorized to work with Part id {}'.format(part.id))
                logger.debug('Responding (400) to request for {} APIView'.format(cname))
                return Response({}, status.HTTP_400_BAD_REQUEST)
        if model == 'PartStorage':
            storage = serializer.validated_data.get('storage')
            if not storage.tenant.organization in orgs:
                logger.info('User not authorized to work with Storage.')
                logger2.warning(request.user, 'User not authorized to work with Storage id {}'.format(storage.id))
                logger.debug('Responding (400) to request for {} APIView'.format(cname))
                return Response({}, status.HTTP_400_BAD_REQUEST)
        if model == 'PartAlternateSKU':
            altsku = serializer.validated_data.get('alt_sku')
            if not altsku.tenant.organization in orgs:
                logger.info('User not authorized to work with AlternateSKU.')
                logger2.warning(request.user, 'User not authorized to work with AlternateSKU id {}'.format(altsku.id))
                logger.debug('Responding (400) to request for {} APIView'.format(cname))
                return Response({}, status.HTTP_400_BAD_REQUEST)
        if model == 'KitPartStorage':
            partstorage = serializer.validated_data.get('partstorage')
            if not partstorage.tenant.organization in orgs:
                logger.info('User not authorized to work with PartStorage.')
                logger2.warning(request.user, 'User not authorized to work with PartStorage id {}'.format(partstorage.id))
                logger.debug('Responding (400) to request for {} APIView'.format(cname))
                return Response({}, status.HTTP_400_BAD_REQUEST)
        logger.debug('Serializing data in request')
        logger.debug('Serializer is valid')
        serializer.save()
        logger.info('PUT request successfully updated {} id {}'.format(model, id))
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Updated {} for id {}'.format(cname, id))
        return Response(serializer.data, status.HTTP_200_OK)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad PUT Request to {} API for id {}'.format(cname,id))
    return Response({}, status.HTTP_400_BAD_REQUEST)

def PatchDetailAPIView(cname, request, id):
    model = cname.replace('List','').replace('Detail','')
    logger.debug('PATCH request made to {} APIView'.format(cname))
    logger2.debug(request.user, 'PATCH Request to {} API for id {}'.format(cname, id))
    if not request.user.has_perm(helpers.get_admin_permission('inventory','change',model)):
        logger.debug('User does not have Change permissions to {} APIView'.format(cname))
        logger2.warning(request.user, 'Access Denied for PUT Request to {} API'.format(cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {} to PATCH'.format(model))
    orgs = helpers.get_user_organizations(request.user)
    try:
        obj = helpers.get_object(model, id)
    except Exception as err:
        logger2.warning(request.user, str(err))
        return Response({}, status.HTTP_404_NOT_FOUND)
    if not obj.tenant.organization in orgs:
        logger.info('User not authorized to modify {}.'.format(model))
        logger2.warning(request.user, 'User not authorized to modify {} id {}'.format(model, id))
        logger.debug('Responding (404) to request for {} APIView'.format(cname))
        return Response({}, status.HTTP_404_NOT_FOUND)
    serializer = helpers.get_serializer(obj, request.data, partial=True)
    if serializer.is_valid():
        if model == 'Storage' or model == 'Assembly':
            if 'parent' in serializer.validated_data.keys():
                parent = serializer.validated_data.get('parent')
                if parent is not None:
                    if not parent.tenant.organization in orgs:
                        logger.info('User not authorized to work with parent {}.'.format(parent.__class__.__name__))
                        logger2.warning(request.user, 'User not authorized to work with parent {} id {}'.format(parent.__class__.__name__, parent.id))
                        logger.debug('Responding (400) to request for {} APIView'.format(cname))
                        return Response({}, status.HTTP_400_BAD_REQUEST)
        if model in ['PartAlternateSKU', 'PartStorage', 'Assembly']:
            if 'part' in serializer.validated_data.keys():
                part = serializer.validated_data.get('part')
                if not part.tenant.organization in orgs:
                    logger.info('User not authorized to work with Part.')
                    logger2.warning(request.user, 'User not authorized to work with Part id {}'.format(part.id))
                    logger.debug('Responding (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
        if model == 'PartStorage':
            if 'storage' in serializer.validated_data.keys():
                storage = serializer.validated_data.get('storage')
                if not storage.tenant.organization in orgs:
                    logger.info('User not authorized to work with Storage.')
                    logger2.warning(request.user, 'User not authorized to work with Storage id {}'.format(storage.id))
                    logger.debug('Responding (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
        if model == 'PartAlternateSKU':
            if 'alt_sku' in serializer.validated_data.keys():
                altsku = serializer.validated_data.get('alt_sku')
                if not altsku.tenant.organization in orgs:
                    logger.info('User not authorized to work with AlternateSKU.')
                    logger2.warning(request.user, 'User not authorized to work with AlternateSKU id {}'.format(altsku.id))
                    logger.debug('Responding (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
        if model == 'KitPartStorage':
            if 'partstorage' in serializer.validated_data.keys():
                partstorage = serializer.validated_data.get('partstorage')
                if not partstorage.tenant.organization in orgs:
                    logger.info('User not authorized to work with PartStorage.')
                    logger2.warning(request.user, 'User not authorized to work with PartStorage id {}'.format(partstorage.id))
                    logger.debug('Responding (400) to request for {} APIView'.format(cname))
                    return Response({}, status.HTTP_400_BAD_REQUEST)
        logger.debug('Serializer is valid')
        serializer.save()
        logger.info('PATCH request successfully updated {} id {}'.format(model, id))
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Updated {} for id {}'.format(cname, id))
        return Response(serializer.data, status.HTTP_200_OK)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad PATCH Request to {} API for id {}'.format(cname, id))
    return Response({}, status.HTTP_400_BAD_REQUEST)

def DeleteDetailAPIView(cname, request, id):
    model = cname.replace('List','').replace('Detail','')
    logger.debug('DELETE request made to {} APIView'.format(cname))
    logger2.debug(request.user, 'DELETE Request to {} API for id {}'.format(cname, id))
    if not request.user.has_perm(helpers.get_admin_permission('inventory','delete',model)):
        logger.debug('User does not have Change permissions to {} APIView'.format(cname))
        logger2.warning(request.user, 'Access Denied for PUT Request to {} API'.format(cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {} to DELETE'.format(model))
    orgs = helpers.get_user_organizations(request.user)
    try:
        obj = helpers.get_object(model, id)
    except Exception as err:
        logger2.warning(request.user, str(err))
        return Response({}, status.HTTP_404_NOT_FOUND)
    if not obj.tenant.organization in orgs:
        logger.info('User not authorized to delete {}.'.format(model))
        logger2.warning(request.user, 'User not authorized to delete {} id {}'.format(model, id))
        logger.debug('Responding (404) to request for {} APIView'.format(cname))
        return Response({}, status.HTTP_404_NOT_FOUND)
    message = {'message': '{} has successfully been deleted. It was connected to the following objects:'.format(model)}
    if model == 'Part':
        logger.debug('Getting related Parent Part objects for {} id {}'.format(model, id))
        parents = Assembly.objects.filter(part=obj)
        message['assembly(parent)'] = PartSerializer(([a.parent for a in parents]), many=True).data
        logger.debug('Getting related Children Part objects for {} id {}'.format(model, id))
        children = Assembly.objects.filter(parent=obj)
        message['assembly(child)'] = PartSerializer(([a.part for a in children]), many=True).data
        logger.debug('Getting related Storage objects for {} id {}'.format(model, id))
        storages = PartStorage.objects.filter(part=obj)
        message['storage'] = StorageSerializer(([ps.storage for ps in storages]), many=True).data
        logger.debug('Getting related AlternateSKU objects for {} id {}'.format(model, id))
        altskus = PartAlternateSKU.objects.filter(part=obj)
        message['alternatesku'] = AlternateSKUSerializer(([pa.alt_sku for pa in altskus]), many=True).data
    if model == 'Storage':
        logger.debug('Getting related Part objects for {} id {}'.format(model, id))
        partstorages = PartStorage.objects.filter(storage=obj)
        message['part'] = PartSerializer(([ps.part for ps in partstorages]), many=True).data
        logger.debug('Getting related Children Storage objects for {} id {}'.format(model, id))
        storages = Storage.objects.filter(parent=obj)
        message['storage(child)'] = StorageSerializer(storages, many=True).data
        message['storage(parent)'] = StorageSerializer(obj.parent).data
    if model == 'AlternateSKU':
        logger.debug('Getting related Part objects for {} id {}'.format(model, id))
        partaltskus = PartAlternateSKU.objects.filter(alt_sku=obj)
        message['part'] = PartSerializer(([pa.part for pa in partaltskus]), many=True).data
    if model == 'PartAlternateSKU':
        message['part'] = PartSerializer(obj.part).data
        message['alternatesku'] = AlternateSKUSerializer(obj.alt_sku).data
    if model == 'PartStorage':
        message['part'] = PartSerializer(obj.part).data
        message['storage'] = StorageSerializer(obj.storage).data
        logger.debug('Getting related Kit objects for {} id {}'.format(model, id))
        kitpartstorages = KitPartStorage.objects.filter(partstorage=obj)
        message['kit'] = KitSerializer(([kps.kit for kps in kitpartstorages]), many=True).data
    if model == 'Assembly':
        message['part'] = PartSerializer(obj.part).data
        message['parent'] = PartSerializer(obj.parent).data
    if model == 'Kit':
        logger.debug('Getting related PartStorage objects for {} id {}'.format(model, id))
        kitpartstorages = KitPartStorage.objects.filter(kit=obj)
        message['partstorage'] = PartStorageSerializer(([kps.partstorage for kps in kitpartstorages]), many=True).data
    if model == 'KitPartStorage':
        message['kit'] = KitSerializer(obj.kit).data
        message['partstorage'] = PartStorageSerializer(obj.partstorage).data
    logger.info('DELETE request for {} id {}'.format(model, id))
    obj.delete()
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info(request.user, 'Deleted {} with id {}'.format(model, id))
    return Response(message, status.HTTP_200_OK)

def _get_organization(org_data):
    if IsInt(org_data):
        return get_object_or_404(Organization, id=org_data)
    else:
        return get_object_or_404(Organization, name=org_data)
