import django
django.setup()
from django.contrib.auth.models import User, Permission
from django.core.exceptions import FieldError
from django.test import TestCase
from random import randint
from rest_framework import status
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from inventory.api.serializers import PartSerializer, StorageSerializer, AlternateSKUSerializer, PartAlternateSKUSerializer, PartStorageSerializer, AssemblySerializer, KitSerializer, KitPartStorageSerializer
from ims.addons.tenant.models import Organization, UserOrganization, TenantPart, TenantStorage, TenantAlternateSKU, TenantPartAlternateSKU, TenantPartStorage, TenantAssembly, TenantKit, TenantKitPartStorage
from ims.addons.tenant.api.serializers import OrganizationSerializer, AlternateSKUReadSerializer, AssemblyReadSerializer, KitReadSerializer, KitPartStorageReadSerializer, PartReadSerializer, PartAlternateSKUReadSerializer, PartStorageReadSerializer, StorageReadSerializer
import ims.addons.tenant.helpers as helpers
import json

api_version = 'v1'
api_module = 'inventory'
api_tenant_module ='tenant'
api_tenant_version = 'v1'

api_baseurl = '/api/{}/{}/'.format(api_version, api_module)
api_tenant_baseurl = '/api/{}/{}/'.format(api_tenant_version, api_tenant_module)

# Test Inventory API Part Views
class InventoryApiPartViewTests(TestCase):
    def setUp(self):
        for i in range(30,35):
            if i != 30:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,randint(50,100)):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i).zfill(4), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part, organization=org)
        self.baseurl = '{}part/'.format(api_baseurl)
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part','Can add part','Can change part','Can delete part']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=self.user,organization=Organization.objects.all()[i])
        self.client.force_login(self.user)

    def test_apiview_part_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().get(self, self.baseurl, part)
        TestGenerics().get(self, '{}?search={}'.format(self.baseurl, part.name[0:4]), part)
        TestGenerics().get(self, '{}?name={}'.format(self.baseurl, part.name), part)
        TestGenerics().get(self, '{}?description={}'.format(self.baseurl, part.description[0:4]), part)
        TestGenerics().get(self, '{}?sku={}'.format(self.baseurl, part.sku[0:3]), part)
        # Run Specific Tests
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, part.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, part.name)
        response = self.client.get('{}?limit=20&sort=name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, part.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, part.name)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, part.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, part.name)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertGreaterEqual(len(json.loads(response.content)),50)

    def test_apiview_part_post(self):
        # Make Data
        orgs = helpers.get_user_organizations(self.user)
        data = {'name': 'Test_View_Part_New', 'description': 'This is a new api view TestCase test part', 'sku': 'new', 'price': '10.00', 'cost': '5.00', 'organization': orgs[0].id}
        serializer = PartSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        # Run Generic Tests
        TestGenerics().post(self, self.baseurl, data)
        # Run Specific Tests
        self.assertIsNotNone(Part.objects.filter(name='Test_View_Part_New').first())
        org = Organization.objects.exclude(id__in=orgs).first()
        data = {'name': 'Test_View_Part_New', 'description': 'This is a new api view TestCase test part', 'sku': 'new', 'price': '10.00', 'cost': '5.00', 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {'name': 'Test_View_Part_New', 'description': 'This is a new api view TestCase test part', 'sku': 'new', 'price': '10.00', 'cost': '5.00'}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_apiview_part_pk_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().pk_get(self, self.baseurl, part)
        # Run Specific Tests
        part = Part.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/'.format(self.baseurl, part.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_apiview_part_pk_put(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        part.name = 'updated'
        # Run Generic Tests
        TestGenerics().pk_put(self, self.baseurl, part)
        # Run Specific Tests
        part.refresh_from_db()
        self.assertEqual(part.name, 'updated')
        part = Part.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.put('{}{}/'.format(self.baseurl, part.id), data=PartSerializer(part).data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_apiview_part_pk_patch(self):
        # Get Object and Make Data
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        data = {'id': part.id, 'sku': part.sku, 'name': 'updated'}
        # Run Generic Tests
        TestGenerics().pk_patch(self, self.baseurl, data)
        # Run Specific Tests
        part = Part.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.patch('{}{}/'.format(self.baseurl, part.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_apiview_part_pk_delete(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        part2 = Part.objects.filter(tenant__organization__in=orgs).last()
        # Build Test Specific Objects
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=part.tenant.organization)
        storage2 = Storage.objects.create(name='Test_View_Storage_2', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage2, organization=part2.tenant.organization)
        altsku = AlternateSKU.objects.create(manufacturer='Zephryus Computing', sku='00001')
        TenantAlternateSKU.objects.create(alternatesku=altsku, organization=part.tenant.organization)
        altsku2 = AlternateSKU.objects.create(manufacturer='Zephryus Computing', sku='00002')
        TenantAlternateSKU.objects.create(alternatesku=altsku2, organization=part.tenant.organization)
        ps = PartStorage.objects.create(part=part, storage=storage, count=1)
        TenantPartStorage.objects.create(partstorage=ps, organization=part.tenant.organization)
        ps2 = PartStorage.objects.create(part=part2, storage=storage2, count=1)
        TenantPartStorage.objects.create(partstorage=ps2, organization=part2.tenant.organization)
        pa = PartAlternateSKU.objects.create(part=part, alt_sku=altsku)
        TenantPartAlternateSKU.objects.create(partalternatesku=pa, organization=part.tenant.organization)
        pa2 = PartAlternateSKU.objects.create(part=part2, alt_sku=altsku2)
        TenantPartAlternateSKU.objects.create(partalternatesku=pa2, organization=part2.tenant.organization)
        # Run Generic Tests
        #TestGenerics().pk_delete(self, self.baseurl, part)
        # Run Specific Tests
        #self.assertIsNone(Part.objects.filter(name=part.name,tenant__organization=part.tenant.organization).first())
        parent = Part.objects.filter(tenant__organization__in=orgs)[3]
        child = Part.objects.filter(tenant__organization__in=orgs)[5]
        Assembly.objects.create(parent=parent, part=part2, count=1)
        Assembly.objects.create(parent=part2, part=child, count=1)
        response = self.client.delete('{}{}/'.format(self.baseurl, part2.id))
        self.assertContains(response, "\"storage\":[{}\"id\":{},".format('{', storage2.id))
        self.assertContains(response, "\"assembly(parent)\":[{}\"id\":{},".format('{', parent.id))
        self.assertContains(response, "\"assembly(child)\":[{}\"id\":{},".format('{', child.id))
        self.assertContains(response, "\"alternatesku\":[{}\"id\":{},".format('{', altsku2.id))
        part = Part.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.delete('{}{}/'.format(self.baseurl, part.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_apiview_part_pk_count(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=part.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=part.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage_2', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=part.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=part.tenant.organization)
        part2 = Part.objects.exclude(tenant__organization__in=orgs).first()
        storage2 = Storage.objects.create(name='Test_View_Storage_3', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage2, organization=part2.tenant.organization)
        ps2 = PartStorage.objects.create(part=part2,storage=storage2,count=5)
        TenantPartStorage.objects.create(partstorage=ps2, organization=part2.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/count/'.format(self.baseurl,part.id))
        self.assertEqual(response.content, b'{"count":10}')
        ps.subtract(3)
        response = self.client.get('{}{}/count/'.format(self.baseurl,part.id))
        self.assertEqual(response.content, b'{"count":7}')
        ps.delete()
        response = self.client.get('{}{}/count/'.format(self.baseurl,part.id))
        self.assertEqual(response.content, b'{"count":5}')
        response = self.client.get('{}{}/count/'.format(self.baseurl,part2.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/count/'.format(self.baseurl,part2.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_assembly(self):
        # Get Object
        self.user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        parent = Part.objects.create(name='Test_View_Part_Assembly',description='This is a short-lived parent part',sku='Assembly',price=10,cost=5)
        TenantPart.objects.create(part=parent, organization=part.tenant.organization)
        child = Part.objects.create(name='Test_View_Part_Child',description='This is a short-lived child part',sku='Child',price=1,cost=0.5)
        TenantPart.objects.create(part=child, organization=part.tenant.organization)
        a = Assembly.objects.create(parent=parent,part=part,count=1)
        TenantAssembly.objects.create(assembly=a, organization=part.tenant.organization)
        a = Assembly.objects.create(parent=parent,part=child,count=1)
        TenantAssembly.objects.create(assembly=a, organization=part.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/assembly/'.format(self.baseurl,part.id))
        self.assertContains(response, parent.name)
        self.assertNotContains(response, part.name)
        self.assertNotContains(response, child.name)
        response = self.client.get('{}{}/assembly/?search=Test_View_Part_Ass&limit=1&sort=sku'.format(self.baseurl,part.id))
        self.assertContains(response, parent.name)
        response = self.client.get('{}{}/assembly/?name=Test_View_Part_Ass&offset=1&sort=-name'.format(self.baseurl,part.id))
        self.assertNotContains(response, parent.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/assembly/?sku=Assem&sort=banana'.format(self.baseurl,part.id))
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/assembly/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view part'))
        response = self.client.get('{}{}/assembly/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
        response = self.client.get('{}{}/assembly/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_apiview_part_pk_component(self):
        # Get Object
        self.user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        parent = Part.objects.create(name='Test_View_Part_Assembly',description='This is a short-lived parent part',sku='Assembly',price=10,cost=5)
        TenantPart.objects.create(part=parent, organization=part.tenant.organization)
        child = Part.objects.create(name='Test_View_Part_Child',description='This is a short-lived child part',sku='Child',price=1,cost=0.5)
        TenantPart.objects.create(part=child, organization=part.tenant.organization)
        a = Assembly.objects.create(parent=parent,part=part,count=1)
        TenantAssembly.objects.create(assembly=a, organization=part.tenant.organization)
        a = Assembly.objects.create(parent=part,part=child,count=1)
        TenantAssembly.objects.create(assembly=a, organization=part.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/component/'.format(self.baseurl,part.id))
        self.assertContains(response, child.name)
        self.assertNotContains(response, part.name)
        self.assertNotContains(response, parent.name)
        response = self.client.get('{}{}/component/?search=Test_View_Part_Chi&limit=1&sort=sku'.format(self.baseurl,part.id))
        self.assertContains(response, child.name)
        response = self.client.get('{}{}/component/?name=Test_View_Part_Chi&offset=1&sort=-name'.format(self.baseurl,part.id))
        self.assertNotContains(response, child.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/component/?sku=Chil&sort=banana'.format(self.baseurl,part.id))
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/component/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view part'))
        response = self.client.get('{}{}/component/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
        response = self.client.get('{}{}/component/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_apiview_part_pk_storage(self):
        # Get Object
        self.user.user_permissions.add(Permission.objects.get(name='Can view storage'))
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=part.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=part.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage_2', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=part.tenant.organization)
        ex = PartStorage.objects.create(part=Part.objects.filter(tenant__organization__in=orgs).last(),storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ex, organization=part.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/storage/'.format(self.baseurl,part.id))
        self.assertContains(response, ps.storage.name)
        self.assertNotContains(response, ps.part.name)
        self.assertNotContains(response, ex.storage.name)
        response = self.client.get('{}{}/storage/?search=Test_View_Storage&limit=1&sort=description'.format(self.baseurl,part.id))
        self.assertContains(response, ps.storage.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/storage/?name=Test_View_Storage&offset=1&sort=banana'.format(self.baseurl,part.id))
        response = self.client.get('{}{}/storage/?description={}&sort=-name'.format(self.baseurl,part.id,'This is a short'))
        self.assertContains(response, ps.storage.name)
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/storage/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view part'))
        response = self.client.get('{}{}/storage/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view storage'))
        response = self.client.get('{}{}/storage/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_apiview_part_pk_kit(self):
        # Get Object
        self.user.user_permissions.add(Permission.objects.get(name='Can view kit'))
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        kit = Kit.objects.create(name='Test_View_Kit_1', description='This is a short-lived kit', sku='Kit1', price=1)
        TenantKit.objects.create(kit=kit, organization=part.tenant.organization)
        ex = Kit.objects.create(name='Test_View_Kit_2', description='This is a short-lived kit', sku='Kit2', price=1)
        TenantKit.objects.create(kit=ex, organization=part.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=part.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=part.tenant.organization)
        kps = KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=part.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage_2', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=part.tenant.organization)
        ps = PartStorage.objects.create(part=Part.objects.filter(tenant__organization__in=orgs).last(),storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=part.tenant.organization)
        kps = KitPartStorage.objects.create(kit=ex,partstorage=ps,count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=part.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/kit/'.format(self.baseurl,part.id))
        self.assertContains(response, kit.name)
        self.assertNotContains(response, ex.name)
        response = self.client.get('{}{}/kit/?search=Test_View_Kit&limit=1&sort=description'.format(self.baseurl,part.id))
        self.assertContains(response, kit.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/kit/?name=Test_View_Kit&offset=1&sort=banana'.format(self.baseurl,part.id))
        response = self.client.get('{}{}/kit/?description={}&sort=-name'.format(self.baseurl,part.id,'This is a short'))
        self.assertContains(response, kit.name)
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/kit/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view part'))
        response = self.client.get('{}{}/kit/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view storage'))
        response = self.client.get('{}{}/kit/'.format(self.baseurl,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_part_pk_kit_sub(self):
        # Get Object
        self.user.user_permissions.add(Permission.objects.get(name='Can decrement count kit'))
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        kit = Kit.objects.create(name='Test_View_Kit_1', description='This is a short-lived kit', sku='Kit1', price=1)
        TenantKit.objects.create(kit=kit, organization=part.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=part.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=part.tenant.organization)
        kps = KitPartStorage.objects.create(kit=kit,partstorage=ps,count=2)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=part.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/kit/{}/subtract/2/'.format(self.baseurl,part.id,kit.id))
        self.assertContains(response, 'Part counts have been decremented for 2 number of Kit id {}'.format(kit.id))
        self.assertNotContains(response, 'Kit cannot be decremented by 2, there are not enough parts available.')
        response = self.client.get('{}{}/kit/{}/subtract/1/'.format(self.baseurl,part.id,kit.id))
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/kit/{}/subtract/1/'.format(self.baseurl,part.id,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view part'))
        response = self.client.get('{}{}/kit/{}/subtract/1/'.format(self.baseurl,part.id,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can decrement count kit'))
        response = self.client.get('{}{}/kit/{}/subtract/1/'.format(self.baseurl,part.id,kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_apiview_part_pk_kit_add(self):
        # Get Object
        self.user.user_permissions.add(Permission.objects.get(name='Can increment count kit'))
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        kit = Kit.objects.create(name='Test_View_Kit_1', description='This is a short-lived kit', sku='Kit1', price=1)
        TenantKit.objects.create(kit=kit, organization=part.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=part.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=part.tenant.organization)
        kps = KitPartStorage.objects.create(kit=kit,partstorage=ps,count=2)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=part.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/kit/{}/add/2/'.format(self.baseurl,part.id,kit.id))
        self.assertContains(response, 'Part counts have been incremented for 2 number of Kit id {}'.format(kit.id))
        response = self.client.get('{}{}/kit/{}/add/1/'.format(self.baseurl,part.id,kit.id))
        self.assertContains(response, 'Part counts have been incremented for 1 number of Kit id {}'.format(kit.id))
        self.assertNotContains(response, 'Kit cannot be decremented by 1, there are not enough parts available.')
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/kit/{}/add/1/'.format(self.baseurl,part.id,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view part'))
        response = self.client.get('{}{}/kit/{}/add/1/'.format(self.baseurl,part.id,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can increment count kit'))
        response = self.client.get('{}{}/kit/{}/add/1/'.format(self.baseurl,part.id,kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

# Test Inventory API Storage Views
class InventoryApiStorageViewTests(TestCase):
    def setUp(self):
        for i in range(35,40):
            if i != 35:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,21):
                storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
                TenantStorage.objects.create(storage=storage, organization=org)
        self.baseurl = '{}storage/'.format(api_baseurl)
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view storage','Can add storage','Can change storage','Can delete storage']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=self.user,organization=Organization.objects.all()[i])
        self.client.force_login(self.user)

    def test_apiview_storage_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        storage = Storage.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().get(self, self.baseurl, storage)
        TestGenerics().get(self, '{}?search={}'.format(self.baseurl, storage.name[0:4]), storage)
        TestGenerics().get(self, '{}?name={}'.format(self.baseurl, storage.name), storage)
        TestGenerics().get(self, '{}?description={}'.format(self.baseurl, storage.description[0:4]), storage)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, storage.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertGreaterEqual(len(json.loads(response.content)),1)
        self.assertNotContains(response, storage.name)
        response = self.client.get('{}?limit=20&sort=name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, storage.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, storage.name)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, storage.name)
        response = self.client.get('{}?limit=20&sort=-name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, storage.name)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertGreaterEqual(len(json.loads(response.content)),20)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_post(self):
        # Make Data
        orgs = helpers.get_user_organizations(self.user)
        data = {'name': 'Test_View_Storage_New', 'description': 'This is a new api view TestCase test storage', 'organization': orgs[0].id}
        serializer = StorageSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        # Run Generic Tests
        TestGenerics().post(self, self.baseurl, data)
        # Run Specific Tests
        self.assertIsNotNone(Storage.objects.filter(name='Test_View_Storage_New').first())
        org = Organization.objects.exclude(id__in=orgs).first()
        data = {'name': 'Test_View_Storage_New', 'description': 'This is a new api view TestCase test storage', 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {'name': 'Test_View_Storage_New', 'description': 'This is a new api view TestCase test storage'}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.post(self.baseurl, data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        storage = Storage.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().pk_get(self, self.baseurl, storage)
        # Run Specific Tests
        storage = Storage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/'.format(self.baseurl, storage.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/'.format(self.baseurl,storage.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_put(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        storage = Storage.objects.filter(tenant__organization__in=orgs).first()
        storage.description = 'This is an updated description'
        # Run Generic Tests
        TestGenerics().pk_put(self, self.baseurl, storage)
        # Run Specific Tests
        storage.refresh_from_db()
        self.assertEqual(storage.description, 'This is an updated description')
        storage = Storage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.put('{}{}/'.format(self.baseurl, storage.id), data=StorageSerializer(storage).data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.put('{}{}/'.format(self.baseurl,storage.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_patch(self):
        # Get Object and Make Data
        orgs = helpers.get_user_organizations(self.user)
        storage = Storage.objects.filter(tenant__organization__in=orgs).first()
        data = {'id': storage.id, 'description': 'This is an updated description'}
        # Run Generic Tests
        TestGenerics().pk_patch(self, self.baseurl, data)
        # Run Specific Tests
        storage = Storage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.patch('{}{}/'.format(self.baseurl, storage.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.patch('{}{}/'.format(self.baseurl,storage.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_delete(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        storage = Storage.objects.filter(tenant__organization__in=orgs).first()
        storage2 = Storage.objects.filter(tenant__organization__in=orgs)[1]
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1', description='This is a short-lived part', sku='00003', price=10.00, cost=5.00)
        TenantPart.objects.create(part=part, organization=storage.tenant.organization)
        part2 = Part.objects.create(name='Test_View_Part_2', description='This is a short-lived part', sku='00004', price=10.00, cost=5.00)
        TenantPart.objects.create(part=part2, organization=storage.tenant.organization)
        ps = PartStorage.objects.create(part=part, storage=storage, count=1)
        TenantPartStorage.objects.create(partstorage=ps, organization=part.tenant.organization)
        ps2 = PartStorage.objects.create(part=part2, storage=storage2, count=1)
        TenantPartStorage.objects.create(partstorage=ps2, organization=part2.tenant.organization)
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, storage)
        # Run Specific Tests
        self.assertIsNone(Storage.objects.filter(name=storage.name,tenant__organization=storage.tenant.organization).first())
        parent = Storage.objects.filter(tenant__organization__in=orgs)[3]
        child = Storage.objects.filter(tenant__organization__in=orgs)[5]
        storage2.parent = parent
        storage2.save()
        child.parent = storage2
        child.save()
        response = self.client.delete('{}{}/'.format(self.baseurl, storage2.id))
        self.assertContains(response, "\"part\":[{}\"id\":{},".format('{', part2.id))
        self.assertContains(response, "\"storage(parent)\":{}\"id\":{},".format('{', parent.id))
        self.assertContains(response, "\"storage(child)\":[{}\"id\":{},".format('{', child.id))
        storage = Storage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.delete('{}{}/'.format(self.baseurl, storage.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.delete('{}{}/'.format(self.baseurl,storage.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_storage_pk_part(self):
        # Get Object
        self.user.user_permissions.add(Permission.objects.get(name='Can view part'))
        orgs = helpers.get_user_organizations(self.user)
        storage = Storage.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1',description='This is a short-lived part',sku='Part1',price=2,cost=1)
        TenantPart.objects.create(part=part, organization=storage.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=storage.tenant.organization)
        part2 = Part.objects.create(name='Test_View_Part_2',description='This is a short-lived part',sku='Part2',price=2,cost=1)
        TenantPart.objects.create(part=part2, organization=storage.tenant.organization)
        ps2 = PartStorage.objects.create(part=part2,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps2, organization=storage.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/part/'.format(self.baseurl,storage.id))
        self.assertContains(response, part2.name)
        response = self.client.get('{}{}/part/?search=Test_View_Part&limit=1&sort=description'.format(self.baseurl,storage.id))
        self.assertContains(response, part.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/part/?name=Test_View_Part&offset=1&sort=banana'.format(self.baseurl,storage.id))
        response = self.client.get('{}{}/part/?description={}&sort=-name'.format(self.baseurl,storage.id,'This is a short'))
        self.assertContains(response, part2.name)
        storage = Storage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/part/'.format(self.baseurl, storage.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/part/'.format(self.baseurl,storage.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view storage'))
        response = self.client.get('{}{}/part/'.format(self.baseurl,storage.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view part'))
        response = self.client.get('{}{}/part/'.format(self.baseurl,storage.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

# Test Inventory API AlternateSKU Views
class InventoryApiAlternateSkuViewTests(TestCase):
    def setUp(self):
        for i in range(40,45):
            if i != 40:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,30):
                altsku = AlternateSKU.objects.create(manufacturer='Zephyrus Computing', sku=str(i).zfill(3))
                TenantAlternateSKU.objects.create(alternatesku=altsku, organization=org)
        self.baseurl = api_baseurl + 'altsku/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view alternate sku','Can add alternate sku','Can change alternate sku','Can delete alternate sku']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        for i in range(0,randint(2,4)):
            UserOrganization.objects.create(user=self.user,organization=Organization.objects.all()[i])
        self.client.force_login(self.user)

    def test_apiview_alternatesku_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        sku = AlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"manufacturer":"{}'.format(sku.manufacturer))
        self.assertContains(response, '"sku":"{}'.format(sku.sku))
        response = self.client.get('{}?search={}'.format(self.baseurl, sku.manufacturer[0:8]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"manufacturer":"{}'.format(sku.manufacturer))
        self.assertContains(response, '"sku":"{}'.format(sku.sku))
        response = self.client.get('{}?manufacturer={}'.format(self.baseurl, sku.manufacturer))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"sku":"' + sku.sku)
        response = self.client.get('{}?sku={}'.format(self.baseurl, sku.sku))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"sku":"' + sku.sku)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, sku.sku)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, sku.sku)
        response = self.client.get('{}?limit=20&sort=sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, sku.sku)
        response = self.client.get('{}?limit=20&sort=-sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, sku.sku)
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        self.assertNotContains(response, sku.sku)
        response = self.client.get('{}?limit=20&sort=-sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertNotContains(response, sku.sku)
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertGreaterEqual(len(json.loads(response.content)), 30)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_alternatesku_post(self):
        # Make Data
        orgs = helpers.get_user_organizations(self.user)
        data = {'manufacturer': 'Zephyrus Computing', 'sku': 'abc123', 'organization': orgs[0].id}
        serializer = AlternateSKUSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        # Run Generic Tests
        TestGenerics().post(self, self.baseurl, data)
        # Run Specific Tests
        self.assertIsNotNone(AlternateSKU.objects.filter(sku='abc123').first())
        org = Organization.objects.exclude(id__in=orgs).first()
        data = {'manufacturer': 'Zephyrus Computing', 'sku': 'abc123', 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {'manufacturer': 'Zephyrus Computing', 'sku': 'abc123'}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.post(self.baseurl, data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_alternatesku_pk_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        sku = AlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl,sku.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"manufacturer":"' + sku.manufacturer)
        self.assertContains(response, '"sku":"' + sku.sku)
        sku = AlternateSKU.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/'.format(self.baseurl, sku.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/'.format(self.baseurl,sku.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_alternatesku_pk_put(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        sku = AlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        sku.manufacturer = 'Zephyrus Computing LLC'
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl,sku.id), AlternateSKUSerializer(sku).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'manufacturer': sku.manufacturer, 'sku': sku.sku.zfill(1000)}
        response = self.client.put('{}{}/'.format(self.baseurl,sku.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        sku.refresh_from_db()
        self.assertEqual(sku.manufacturer, 'Zephyrus Computing LLC')
        sku = AlternateSKU.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.put('{}{}/'.format(self.baseurl, sku.id), data=AlternateSKUSerializer(sku).data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.put('{}{}/'.format(self.baseurl,sku.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_alternatesku_pk_patch(self):
        # Get Object and Make Data
        orgs = helpers.get_user_organizations(self.user)
        sku = AlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        data = {'id': sku.id, 'manufacturer': 'Zephyrus Computing LLC'}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, sku.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': sku.id, 'sku': sku.sku.zfill(1000)}
        response = self.client.patch('{}{}/'.format(self.baseurl, sku.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        sku = AlternateSKU.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.patch('{}{}/'.format(self.baseurl, sku.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.patch('{}{}/'.format(self.baseurl,sku.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_alternatesku_pk_delete(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        sku = AlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        sku2 = AlternateSKU.objects.filter(tenant__organization__in=orgs).last()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1', description='This is a short-lived part', sku='00005', price=10.00, cost=5.00)
        TenantPart.objects.create(part=part, organization=sku.tenant.organization)
        part2 = Part.objects.create(name='Test_View_Part_2', description='This is a short-lived part', sku='00006', price=10.00, cost=5.00)
        TenantPart.objects.create(part=part2, organization=sku.tenant.organization)
        pa = PartAlternateSKU.objects.create(part=part, alt_sku=sku)
        TenantPartAlternateSKU.objects.create(partalternatesku=pa, organization=sku.tenant.organization)
        pa2 = PartAlternateSKU.objects.create(part=part2, alt_sku=sku2)
        TenantPartAlternateSKU.objects.create(partalternatesku=pa2, organization=sku2.tenant.organization)
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, sku)
        # Run Specific Tests
        self.assertIsNone(AlternateSKU.objects.filter(id=sku.id).first())
        response = self.client.delete('{}{}/'.format(self.baseurl, sku2.id))
        self.assertContains(response, "\"part\":[{}\"id\":{},".format('{', part2.id))
        sku = AlternateSKU.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.delete('{}{}/'.format(self.baseurl, sku.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.delete('{}{}/'.format(self.baseurl,sku.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Inventory API PartStorage Views
class InventoryApiPartStorageViewTests(TestCase):
    def setUp(self):
        for i in range(45,50):
            if i != 45:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,101):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i*10).zfill(5), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part, organization=org)
            for i in range(0,20):
                storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
                TenantStorage.objects.create(storage=storage, organization=org)
            for i in range(0,151):
                j = randint(1,100)
                k = randint(1,19)
                part = Part.objects.get(name='Test_View_Part_{}'.format(j),tenant__organization=org)
                storage = Storage.objects.get(name='Test_View_Storage_{}'.format(k),tenant__organization=org)
                ps = PartStorage.objects.create(part=part, storage=storage, count=12)
                TenantPartStorage.objects.create(partstorage=ps, organization=org)
        self.baseurl = api_baseurl + 'partstorage/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part storage','Can add part storage','Can change part storage','Can delete part storage','Can increment count part storage','Can decrement count part storage']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=self.user,organization=Organization.objects.all()[i])
        self.client.force_login(self.user)

    def test_apiview_partstorage_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"part\":{}\"id\":{},".format('{',ps.part.id))
        self.assertContains(response, "\"storage\":{}\"id\":{},".format('{',ps.storage.id))
        response = self.client.get(self.baseurl + '?search=' + ps.part.name[0:4])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"part\":{}\"id\":{},".format('{',ps.part.id))
        self.assertContains(response, "\"storage\":{}\"id\":{},".format('{',ps.storage.id))
        response = self.client.get('{}?storage__name={}'.format(self.baseurl, ps.storage.name))
        self.assertContains(response, ps.storage.name)
        response = self.client.get('{}?part__name={}'.format(self.baseurl, ps.part.name))
        self.assertContains(response, ps.part.name)
        response = self.client.get('{}?storage__description={}'.format(self.baseurl, ps.storage.description[0:8]))
        self.assertContains(response, ps.storage.description)
        response = self.client.get('{}?part__description={}'.format(self.baseurl, ps.part.description[0:8]))
        self.assertContains(response, ps.part.description)
        response = self.client.get('{}?part__sku={}'.format(self.baseurl, ps.part.sku))
        self.assertContains(response, ps.part.sku)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, ps.storage.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=storage__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, ps.storage.name)
        response = self.client.get('{}?limit=20&sort=-storage__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=-part__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertEqual(100, len(json.loads(response.content)))
        response = self.client.get(self.baseurl)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_post(self):
        # Get Objects and Make Data
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        storage = Storage.objects.filter(tenant__organization=part.tenant.organization).first()
        data = {'part': part.id, 'storage': storage.id, 'count': 3, 'organization': part.tenant.organization.id}
        # Run Specific Tests
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(PartStorage.objects.filter(part=part, storage=storage, count=3).first())
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(self.baseurl, {'part':0,'storage':0,'count':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        org = Organization.objects.exclude(id__in=orgs).first()
        data = {'part': part.id, 'storage': storage.id, 'count': 3, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        org = part.tenant.organization
        part = Part.objects.exclude(tenant__organization__in=orgs).first()
        data = {'part': part.id, 'storage': storage.id, 'count': 3, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        storage = Storage.objects.exclude(tenant__organization=org).first()
        data = {'part': part.id, 'storage': storage.id, 'count': 3, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {'part': part.id, 'storage': storage.id, 'count': 3}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.post(self.baseurl,data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_pk_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, ps.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"part\":{}\"id\":{},".format('{',ps.part.id))
        self.assertContains(response, "\"storage\":{}\"id\":{},".format('{',ps.storage.id))
        ps = PartStorage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/'.format(self.baseurl, ps.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/'.format(self.baseurl,ps.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_pk_put(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).first()
        ps.count = 0
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, ps.id), PartStorageSerializer(ps).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': ps.id, 'count': 0}
        response = self.client.put('{}{}/'.format(self.baseurl, ps.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        ps.refresh_from_db()
        self.assertEqual(ps.count, 0)
        ps = PartStorage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.put('{}{}/'.format(self.baseurl, ps.id), data=PartStorageSerializer(ps).data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.put('{}{}/'.format(self.baseurl,ps.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_pk_patch(self):
        # Get Object and Make Data
        orgs = helpers.get_user_organizations(self.user)
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).first()
        data = {'id': ps.id, 'count': 0}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, ps.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': ps.id, 'part': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, ps.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        ps = PartStorage.objects.exclude(tenant__organization__in=orgs).first()
        data = {'id': ps.id, 'count': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, ps.id), data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.patch('{}{}/'.format(self.baseurl,ps.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        
    def test_apiview_partstorage_pk_delete(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, ps)
        # Run Specific Tests
        self.assertIsNone(PartStorage.objects.filter(id=ps.id).first())
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).first()
        k = Kit.objects.create(name='Test_View_Kit_New', description='This is a short-lived kit', sku='newkit', price=1)
        KitPartStorage.objects.create(kit=k, partstorage=ps, count=1)
        storage = ps.storage
        part = ps.part
        response = self.client.delete('{}{}/'.format(self.baseurl, ps.id))
        self.assertContains(response, "\"storage\":{}\"id\":{},".format('{', storage.id))
        self.assertContains(response, "\"part\":{}\"id\":{},".format('{', part.id))
        self.assertContains(response, "\"kit\":[{}\"id\":{},".format('{', k.id))
        ps = PartStorage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.delete('{}{}/'.format(self.baseurl, ps.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.delete('{}{}/'.format(self.baseurl,ps.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_add(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).first()
        k = ps.count + 8
        # Build URL
        url = '{}addpart/{}/{}/8/'.format(api_baseurl, ps.storage.id, ps.part.id)
        # Build New Storage object for failure testing
        Storage.objects.create(name='Test_View_Storage_New', description='This is a new api view TestCase test storage')
        # Build failure URL
        bad_url1 = '{}addpart/10000000/{}/1/'.format(api_baseurl, ps.part.id)
        # Run Specific Tests
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(bad_url1)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        ps.refresh_from_db()
        self.assertEqual(ps.count, k)
        ps.delete()
        part = Part.objects.exclude(tenant__organization__in=orgs).first()
        storage = Storage.objects.exclude(tenant__organization__in=orgs).first()
        bad_url2 = '{}addpart/{}/{}/1/'.format(api_baseurl, storage.id, ps.part.id)
        bad_url3 = '{}addpart/{}/{}/1/'.format(api_baseurl, storage.id, part.id)
        response = self.client.get(bad_url2)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get(bad_url3)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get(url)
        if response.status_code == status.HTTP_200_OK:
            try:
                ps.refresh_from_db()
                ps.delete()
            except PartStorage.DoesNotExist:
                pass
            response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}addpart/{}/{}/1/'.format(api_baseurl,storage.id,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_add2(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).first()
        k = ps.count + 8
        # Build URL
        url = '{}{}/add/8/'.format(self.baseurl, ps.id)
        # Build failure URL
        bad_url1 = '{}10000000/add/1/'.format(self.baseurl)
        # Run Specific Tests
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(bad_url1)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        ps.refresh_from_db()
        self.assertEqual(ps.count, k)
        ps.delete()
        partstorage = PartStorage.objects.exclude(tenant__organization__in=orgs).first()
        bad_url2 = '{}{}/add/8/'.format(self.baseurl, partstorage.id)
        response = self.client.get(bad_url2)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get(url)
        if response.status_code == status.HTTP_200_OK:
            try:
                ps.refresh_from_db()
                ps.delete()
            except PartStorage.DoesNotExist:
                pass
            response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/add/1/'.format(self.baseurl,partstorage.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can increment count part storage'))
        response = self.client.get('{}{}/add/1/'.format(self.baseurl,partstorage.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_apiview_partstorage_sub(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).last()
        k = ps.count - 1
        # Build URL
        url = '{}subpart/{}/{}/{}/'.format(api_baseurl, ps.storage.id, ps.part.id, k)
        # Build New Part object for failure testing
        Part.objects.create(name='Test_View_Part_New', description='This is a new api view TestCase test part', sku='NEW', price=10.00, cost=5.00)
        # Build failure URL
        bad_url1 = '{}subpart/{}/1000000/1/'.format(api_baseurl, ps.storage.id)
        # Run Specific Tests
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(bad_url1)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        ps.refresh_from_db()
        self.assertEqual(ps.count, 1)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        ps.delete()
        part = Part.objects.exclude(tenant__organization__in=orgs).first()
        storage = Storage.objects.exclude(tenant__organization__in=orgs).first()
        bad_url2 = '{}subpart/{}/{}/1/'.format(api_baseurl, storage.id, ps.part.id)
        bad_url3 = '{}subpart/{}/{}/1/'.format(api_baseurl, storage.id, part.id)
        response = self.client.get(bad_url2)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get(bad_url3)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = self.client.get(url)
        if response.status_code == status.HTTP_200_OK:
            try:
                ps.refresh_from_db()
                ps.delete()
            except PartStorage.DoesNotExist:
                pass
            response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}subpart/{}/{}/1/'.format(api_baseurl,storage.id,part.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstorage_sub2(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        ps = PartStorage.objects.filter(tenant__organization__in=orgs).last()
        k = ps.count - 1
        # Build URL
        url = '{}{}/subtract/{}/'.format(self.baseurl, ps.id, k)
        # Build failure URL
        bad_url1 = '{}100000/subtract/1/'.format(self.baseurl)
        # Run Specific Tests
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(bad_url1)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        ps.refresh_from_db()
        self.assertEqual(ps.count, 1)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        ps.delete()
        partstorage = PartStorage.objects.exclude(tenant__organization__in=orgs).first()
        bad_url2 = '{}{}/subtract/1/'.format(self.baseurl, partstorage.id)
        response = self.client.get(bad_url2)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/subtract/1/'.format(self.baseurl,partstorage.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can decrement count part storage'))
        response = self.client.get('{}{}/subtract/1/'.format(self.baseurl,partstorage.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

# Test PartAlternateSKU Views
class InventoryApiPartAlternateSkuViewTests(TestCase):
    def setUp(self):
        for i in range(50,55):
            if i != 50:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,101):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i*3).zfill(6), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part, organization=org)
            for i in range(0,30):
                j = randint(0,100)
                part = Part.objects.get(name='Test_View_Part_{}'.format(j),tenant__organization=org)
                altsku = AlternateSKU.objects.create(manufacturer='Zephyrus Computing', sku='aa' + str(i).zfill(4))
                TenantAlternateSKU.objects.create(alternatesku=altsku, organization=org)
                pa = PartAlternateSKU.objects.create(part=part, alt_sku=altsku)
                TenantPartAlternateSKU.objects.create(partalternatesku=pa, organization=org)
        self.baseurl = api_baseurl + 'partaltsku/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part alternate sku','Can add part alternate sku','Can change part alternate sku','Can delete part alternate sku']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=self.user,organization=Organization.objects.all()[i])
        self.client.force_login(self.user)

    def test_apiview_partaltsku_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        pa = PartAlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(pa.part).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"alt_sku":' + json.dumps(AlternateSKUSerializer(pa.alt_sku).data, separators=(",",":"))[:-1])
        response = self.client.get(self.baseurl + '?search=' + pa.part.name[0:4])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(pa.part).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"alt_sku":' + json.dumps(AlternateSKUSerializer(pa.alt_sku).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?alt_sku__sku={}'.format(self.baseurl, pa.alt_sku.sku))
        self.assertContains(response, pa.alt_sku.sku)
        response = self.client.get('{}?alt_sku__manufacturer={}'.format(self.baseurl, pa.alt_sku.manufacturer))
        self.assertContains(response, pa.alt_sku.manufacturer)
        response = self.client.get('{}?part__name={}'.format(self.baseurl, pa.part.name))
        self.assertContains(response, pa.part.name)
        response = self.client.get('{}?part__description={}'.format(self.baseurl, pa.part.description[0:8]))
        self.assertContains(response, pa.part.description)
        response = self.client.get('{}?part__sku={}'.format(self.baseurl, pa.part.sku))
        self.assertContains(response, pa.part.sku)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, pa.part.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertGreaterEqual(len(json.loads(response.content)), 10)
        response = self.client.get('{}?limit=20&sort=part__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, pa.part.name)
        response = self.client.get('{}?limit=20&sort=-part__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=-altsku__sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertGreaterEqual(len(json.loads(response.content)), 30)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partaltsku_post(self):
        # Get Objects and Make Data
        orgs = helpers.get_user_organizations(self.user)
        part = Part.objects.filter(tenant__organization__in=orgs).first()
        altsku = AlternateSKU.objects.filter(tenant__organization=part.tenant.organization).first()
        data = {'part': part.id, 'alt_sku': altsku.id, 'organization': part.tenant.organization.id}
        # Run Specific Tests
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(PartAlternateSKU.objects.filter(part=part, alt_sku=altsku).first())
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(self.baseurl, {'part':0,'alt_sku':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        org = Organization.objects.exclude(id__in=orgs).first()
        data = {'part': part.id, 'alt_sku': altsku.id, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        org = part.tenant.organization
        altsku = AlternateSKU.objects.exclude(tenant__organization__in=orgs).first()
        data = {'part': part.id, 'alt_sku': altsku.id, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        part = Part.objects.exclude(tenant__organization__in=orgs).first()
        data = {'part': part.id, 'alt_sku': altsku.id, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {'part': part.id, 'alt_sku': altsku.id}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.post(self.baseurl,data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partaltsku_pk_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        pa = PartAlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, pa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"part\":{}\"id\":{},".format('{',pa.part.id))
        self.assertContains(response, "\"alt_sku\":{}\"id\":{},".format('{',pa.alt_sku.id))
        pa = PartAlternateSKU.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/'.format(self.baseurl, pa.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/'.format(self.baseurl,pa.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partaltsku_pk_put(self):
        # Get Objects
        orgs = helpers.get_user_organizations(self.user)
        p = Part.objects.filter(tenant__organization__in=orgs).first()
        pa = PartAlternateSKU.objects.filter(tenant__organization=p.tenant.organization).first()
        pa.part = p
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, pa.id), PartAlternateSKUSerializer(pa).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': pa.id, 'part': pa.part.id}
        response = self.client.put('{}{}/'.format(self.baseurl, pa.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        pa.refresh_from_db()
        self.assertEqual(pa.part, p)
        pa = PartAlternateSKU.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.put('{}{}/'.format(self.baseurl, pa.id), data=PartAlternateSKUSerializer(pa).data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.put('{}{}/'.format(self.baseurl,pa.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partaltsku_pk_patch(self):
        # Get Objects and Make Data
        orgs = helpers.get_user_organizations(self.user)
        altsku = AlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        pa = PartAlternateSKU.objects.filter(tenant__organization=altsku.tenant.organization).first()
        if pa.alt_sku == altsku:
            altsku = AlternateSKU.objects.filter(tenant__organization=altsku.tenant.organization).last()
        data = {'id': pa.id, 'alt_sku': altsku.id}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, pa.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': pa.id, 'part': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, pa.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        pa = PartAlternateSKU.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.patch('{}{}/'.format(self.baseurl, pa.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.patch('{}{}/'.format(self.baseurl,pa.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partaltsku_pk_delete(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        pa = PartAlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, pa)
        # Run Specific Tests
        self.assertIsNone(PartAlternateSKU.objects.filter(id=pa.id).first())
        pa = PartAlternateSKU.objects.filter(tenant__organization__in=orgs).first()
        altsku = pa.alt_sku
        part = pa.part
        response = self.client.delete('{}{}/'.format(self.baseurl, pa.id))
        self.assertContains(response, "\"alternatesku\":{}\"id\":{},".format('{', altsku.id))
        self.assertContains(response, "\"part\":{}\"id\":{},".format('{', part.id))
        pa = PartAlternateSKU.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.delete('{}{}/'.format(self.baseurl, pa.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.delete('{}{}/'.format(self.baseurl,pa.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Assembly Views
class InventoryApiAssemblyViewTests(TestCase):
    def setUp(self):
        for i in range(55,60):
            if i != 55:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,101):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i*3).zfill(6), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part, organization=org)
            for i in range(0,16):
                for j in range(0, randint(5,15)):
                    k = randint(1,99)
                    parent = Part.objects.filter(tenant__organization=org)[i]
                    part = Part.objects.filter(tenant__organization=org)[k]
                    assembly = Assembly.objects.create(parent=parent, part=part, count=randint(1,5))
                    TenantAssembly.objects.create(assembly=assembly, organization=org)
        self.baseurl = api_baseurl + 'assembly/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view assembly','Can add assembly','Can change assembly','Can delete assembly']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=self.user,organization=Organization.objects.all()[i])
        self.client.force_login(self.user)

    def test_apiview_assembly_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        a = Assembly.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"parent":' + json.dumps(PartSerializer(a.parent).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(a.part).data, separators=(",",":"))[:-1])
        response = self.client.get(self.baseurl + '?search=' + a.parent.name[0:4])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"parent":' + json.dumps(PartSerializer(a.parent).data, separators=(",",":"))[:-1])
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(a.part).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?parent__name={}'.format(self.baseurl, a.parent.name))
        self.assertContains(response, a.parent.name)
        response = self.client.get('{}?parent__description={}'.format(self.baseurl, a.parent.description[0:8]))
        self.assertContains(response, a.parent.description)
        response = self.client.get('{}?parent__sku={}'.format(self.baseurl, a.parent.sku))
        self.assertContains(response, a.parent.sku)
        response = self.client.get('{}?part__name={}'.format(self.baseurl, a.part.name))
        self.assertContains(response, a.part.name)
        response = self.client.get('{}?part__description={}'.format(self.baseurl, a.part.description[0:8]))
        self.assertContains(response, a.part.description)
        response = self.client.get('{}?part__sku={}'.format(self.baseurl, a.part.sku))
        self.assertContains(response, a.part.sku)
        response = self.client.get('{}?limit=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, a.part.name)
        response = self.client.get('{}?limit=20&offset=20'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=parent__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        self.assertContains(response, a.parent.name)
        response = self.client.get('{}?limit=20&sort=-part__name'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?offset=1000'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        response = self.client.get('{}?limit=20&sort=-parent__sku'.format(self.baseurl))
        self.assertEqual(20, len(json.loads(response.content)))
        response = self.client.get('{}?sort=-banana'.format(self.baseurl))
        self.assertGreater(len(json.loads(response.content)), 20)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_post(self):
        # Get Objects and Make Data
        orgs = helpers.get_user_organizations(self.user)
        parent = Part.objects.filter(tenant__organization__in=orgs).first()
        part = Part.objects.filter(tenant__organization=parent.tenant.organization).last()
        data = {'parent': parent.id, 'part': part.id, 'count': 1, 'organization': parent.tenant.organization.id}
        # Run Specific Tests
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(Assembly.objects.filter(parent=parent, part=part).first())
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(self.baseurl, {'parent':0,'part':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        org = Organization.objects.exclude(id__in=orgs).first()
        data = {'parent': parent.id, 'part': part.id, 'count': 1, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        org = part.tenant.organization
        part = Part.objects.exclude(tenant__organization__in=orgs).first()
        data = {'parent': parent.id, 'part': part.id, 'count': 1, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        assembly = Assembly.objects.exclude(tenant__organization__in=orgs).first()
        data = {'parent': assembly.parent.id, 'part': part.id, 'count': 1, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {'parent': assembly.parent.id, 'part': part.id, 'count': 1}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.post(self.baseurl, data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        a = Assembly.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, a.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"parent\":{}\"id\":{},".format('{',a.parent.id))
        self.assertContains(response, "\"part\":{}\"id\":{},".format('{',a.part.id))
        a = Assembly.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/'.format(self.baseurl, a.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/'.format(self.baseurl,a.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_put(self):
        # Get Objects
        orgs = helpers.get_user_organizations(self.user)
        a = Assembly.objects.filter(tenant__organization__in=orgs).first()
        p = a.part
        p2 = Part.objects.filter(tenant__organization=a.tenant.organization).first()
        a.part = p2
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, a.id), AssemblySerializer(a).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': a.id, 'part': a.part.id}
        response = self.client.put('{}{}/'.format(self.baseurl, a.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        a.refresh_from_db()
        self.assertEqual(a.part, p)
        a = Assembly.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.put('{}{}/'.format(self.baseurl, a.id), data=AssemblySerializer(a).data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.put('{}{}/'.format(self.baseurl,a.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_patch(self):
        # Get Objects and Make Data
        orgs = helpers.get_user_organizations(self.user)
        a = Assembly.objects.filter(tenant__organization__in=orgs).first()
        p = Part.objects.filter(tenant__organization=a.tenant.organization).first()
        data = {'id': a.id, 'parent': p.id}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, a.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': a.id, 'parent': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, a.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        a = Assembly.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.patch('{}{}/'.format(self.baseurl, a.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.patch('{}{}/'.format(self.baseurl,a.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_delete(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        a = Assembly.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, a)
        # Run Specific Tests
        self.assertIsNone(Assembly.objects.filter(id=a.id).first())
        a = Assembly.objects.filter(tenant__organization__in=orgs).first()
        parent = a.parent
        part = a.part
        response = self.client.delete('{}{}/'.format(self.baseurl, a.id))
        self.assertContains(response, "\"parent\":{}\"id\":{},".format('{', parent.id))
        self.assertContains(response, "\"part\":{}\"id\":{},".format('{', part.id))
        a = Assembly.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.delete('{}{}/'.format(self.baseurl, a.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.delete('{}{}/'.format(self.baseurl,a.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_assembly_pk_part(self):
        # Get Object
        self.user.user_permissions.add(Permission.objects.get(name='Can view part'))
        orgs = helpers.get_user_organizations(self.user)
        a = Assembly.objects.filter(tenant__organization__in=orgs).first()
        child = Part.objects.create(name='Test_View_Part_Child',description='This is a short-lived child part',sku='Child',price=1,cost=0.5)
        TenantPart.objects.create(part=child, organization=a.tenant.organization)
        a2 = Assembly.objects.create(parent=a.parent,part=child,count=1)
        TenantAssembly.objects.create(assembly=a2, organization=a.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/part/'.format(self.baseurl,a.id))
        self.assertContains(response, child.name)
        self.assertNotContains(response, a.parent.name)
        response = self.client.get('{}{}/part/?search=Test_View_Part_Chi&limit=1&sort=sku'.format(self.baseurl,a.id))
        self.assertContains(response, child.name)
        response = self.client.get('{}{}/part/?name=Test_View_Part_Chi&offset=1&sort=-name'.format(self.baseurl,a.id))
        self.assertNotContains(response, child.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/part/?sku=Chil&sort=banana'.format(self.baseurl,a.id))
        a = Assembly.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/part/'.format(self.baseurl,a.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/part/'.format(self.baseurl,a.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view assembly'))
        response = self.client.get('{}{}/part/'.format(self.baseurl,a.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view part'))
        response = self.client.get('{}{}/part/'.format(self.baseurl,a.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

# Test Kit Views
class InventoryApiKitViewTests(TestCase):
    def setUp(self):
        for i in range(60,65):
            if i != 60:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,101):
                kit = Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is an api view TestCase test kit', sku=str(i).zfill(6), price=10.00)
                TenantKit.objects.create(kit=kit, organization=org)
        self.baseurl = '{}kit/'.format(api_baseurl)
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view kit','Can add kit','Can change kit','Can delete kit','Can increment count kit','Can decrement count kit']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=self.user,organization=Organization.objects.all()[i])
        self.client.force_login(self.user)

    def test_apiview_kit_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().get(self, self.baseurl, kit)
        TestGenerics().get(self, '{}?search={}'.format(self.baseurl, kit.name[0:4]), kit)
        
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_post(self):
        # Make Data
        orgs = helpers.get_user_organizations(self.user)
        data = {'name': 'Test_View_Kit_New', 'description': 'This is a new api view TestCase test kit', 'sku': 'new', 'price': '10.00', 'organization': orgs[0].id}
        serializer = KitSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        # Run Generic Tests
        TestGenerics().post(self, self.baseurl, data, False) # This is set to False because of the Unique Constraint on the 'sku' field which causes a 400 reply instead of the usual expected 200
        # Run Specific Tests
        self.assertIsNotNone(Kit.objects.filter(name='Test_View_Kit_New').first())
        org = Organization.objects.exclude(id__in=orgs).first()
        data = {'name': 'Test_View_Kit_New', 'description': 'This is a new api view TestCase test kit', 'sku': 'new', 'price': '10.00', 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {'name': 'Test_View_Kit_New', 'description': 'This is a new api view TestCase test kit', 'sku': 'new', 'price': '10.00'}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get(self.baseurl,data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_pk_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().pk_get(self, self.baseurl, kit)
        kit = Kit.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/'.format(self.baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/'.format(self.baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_pk_put(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        kit.name = 'updated'
        # Run Generic Tests
        TestGenerics().pk_put(self, self.baseurl, kit)
        # Run Specific Tests
        kit.refresh_from_db()
        self.assertEqual(kit.name, 'updated')
        kit = Kit.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.put('{}{}/'.format(self.baseurl, kit.id), data=KitSerializer(kit).data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.put('{}{}/'.format(self.baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_pk_patch(self):
        # Get Object and Make Data
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        data = {'id': kit.id, 'sku': kit.sku, 'name': 'updated'}
        # Run Generic Tests
        TestGenerics().pk_patch(self, self.baseurl, data)
        kit = Kit.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.patch('{}{}/'.format(self.baseurl, kit.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.patch('{}{}/'.format(self.baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_pk_delete(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        kit2 = Kit.objects.filter(tenant__organization__in=orgs).last()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part1', description='This is a short-lived part', sku='shortlived', price=5.00, cost=0.00)
        TenantPart.objects.create(part=part, organization=kit.tenant.organization)
        part2 = Part.objects.create(name='Test_View_Part2', description='This is a short-lived part', sku='shortlived2', price=5.00, cost=0.00)
        TenantPart.objects.create(part=part2, organization=kit.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage1', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=kit.tenant.organization)
        storage2 = Storage.objects.create(name='Test_View_Storage2', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage2, organization=kit.tenant.organization)
        partstorage = PartStorage.objects.create(part=part, storage=storage, count=1)
        TenantPartStorage.objects.create(partstorage=partstorage, organization=kit.tenant.organization)
        partstorage2 = PartStorage.objects.create(part=part2, storage=storage2, count=1)
        TenantPartStorage.objects.create(partstorage=partstorage2, organization=kit.tenant.organization)
        kps = KitPartStorage.objects.create(kit=kit, partstorage=partstorage, count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=kit.tenant.organization)
        kps2 = KitPartStorage.objects.create(kit=kit2, partstorage=partstorage2, count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kps2, organization=kit2.tenant.organization)
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, kit)
        # Run Specific Tests
        response = self.client.delete('{}{}/'.format(self.baseurl, kit2.id))
        self.assertContains(response, "\"partstorage\":[{}\"id\":{},".format('{', partstorage2.id))
        self.assertIsNone(Kit.objects.filter(name=kit.name,tenant__organization=kit.tenant.organization).first())
        kit = Kit.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.delete('{}{}/'.format(self.baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.delete('{}{}/'.format(self.baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_sub(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part2', description='This is a short-lived part', sku='shortlived2', price=5.00, cost=0.00)
        TenantPart.objects.create(part=part, organization=kit.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage2', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=kit.tenant.organization)
        partstorage = PartStorage.objects.create(part=part, storage=storage, count=1)
        TenantPartStorage.objects.create(partstorage=partstorage, organization=kit.tenant.organization)
        kitpartstorage = KitPartStorage.objects.create(kit=kit, partstorage=partstorage, count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kitpartstorage, organization=kit.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}subkit/{}/{}/'.format(api_baseurl, kit.id, 1))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        partstorage.refresh_from_db()
        self.assertEqual(partstorage.count, 0)
        response = self.client.get('{}subkit/{}/{}/'.format(api_baseurl, kit.id, 1))
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        kit = Kit.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}subkit/{}/1/'.format(api_baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}subkit/{}/1/'.format(api_baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_sub2(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part2', description='This is a short-lived part', sku='shortlived2', price=5.00, cost=0.00)
        TenantPart.objects.create(part=part, organization=kit.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage2', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=kit.tenant.organization)
        partstorage = PartStorage.objects.create(part=part, storage=storage, count=1)
        TenantPartStorage.objects.create(partstorage=partstorage, organization=kit.tenant.organization)
        kitpartstorage = KitPartStorage.objects.create(kit=kit, partstorage=partstorage, count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kitpartstorage, organization=kit.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/subtract/1/'.format(self.baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        partstorage.refresh_from_db()
        self.assertEqual(partstorage.count, 0)
        response = self.client.get('{}{}/subtract/1/'.format(self.baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        kit = Kit.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/subtract/1/'.format(self.baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/subtract/1/'.format(self.baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_add(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part2', description='This is a short-lived part', sku='shortlived2', price=5.00, cost=0.00)
        TenantPart.objects.create(part=part, organization=kit.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage2', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=kit.tenant.organization)
        partstorage = PartStorage.objects.create(part=part, storage=storage, count=1)
        TenantPartStorage.objects.create(partstorage=partstorage, organization=kit.tenant.organization)
        kitpartstorage = KitPartStorage.objects.create(kit=kit, partstorage=partstorage, count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kitpartstorage, organization=kit.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/add/1/'.format(self.baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        partstorage.refresh_from_db()
        self.assertEqual(partstorage.count, 2)
        kit = Kit.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/add/1/'.format(api_baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/add/1/'.format(self.baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kit_pk_partstorage(self):
        # Get Object
        self.user.user_permissions.add(Permission.objects.get(name='Can view kit part storage'))
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1',description='This is a short-lived part',sku='Part1',price=2,cost=1)
        TenantPart.objects.create(part=part, organization=kit.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage_1',description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=kit.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=kit.tenant.organization)
        kps = KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=kit.tenant.organization)
        part = Part.objects.create(name='Test_View_Part_2',description='This is a short-lived part',sku='Part2',price=2,cost=1)
        TenantPart.objects.create(part=part, organization=kit.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=kit.tenant.organization)
        kps = KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=kit.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/partstorage/'.format(self.baseurl,kit.id))
        self.assertContains(response, ps.part.name)
        self.assertNotContains(response, kit.name)
        response = self.client.get('{}{}/partstorage/?search=Test_View_Part&limit=1&sort=part__description'.format(self.baseurl,kit.id))
        self.assertNotContains(response, ps.part.name)
        with self.assertRaises(FieldError):
            response = self.client.get('{}{}/partstorage/?name=Test_View_Storage&offset=1&sort=banana'.format(self.baseurl,kit.id))
        response = self.client.get('{}{}/partstorage/?part__description={}&sort=-part__name'.format(self.baseurl,kit.id,'This is a short'))
        self.assertContains(response, ps.part.name)
        kit = Kit.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/partstorage/'.format(api_baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/partstorage/'.format(self.baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apivew_kit_pk_count(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        # Build Test Specific Objects
        part = Part.objects.create(name='Test_View_Part_1',description='This is a short-lived part',sku='Part1',price=2,cost=1)
        TenantPart.objects.create(part=part, organization=kit.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage_1', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=kit.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=kit.tenant.organization)
        kps = KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=kit.tenant.organization)
        part = Part.objects.create(name='Test_View_Part_2',description='This is a short-lived part',sku='Part2',price=2,cost=1)
        TenantPart.objects.create(part=part, organization=kit.tenant.organization)
        storage = Storage.objects.create(name='Test_View_Storage_2', description='This is a short-lived container')
        TenantStorage.objects.create(storage=storage, organization=kit.tenant.organization)
        ps = PartStorage.objects.create(part=part,storage=storage,count=5)
        TenantPartStorage.objects.create(partstorage=ps, organization=kit.tenant.organization)
        kps = KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=kit.tenant.organization)
        # Run Specific Tests
        response = self.client.get('{}{}/count/'.format(self.baseurl,kit.id))
        self.assertEqual(response.content, b'{"count":5}')
        ps.subtract(3)
        response = self.client.get('{}{}/count/'.format(self.baseurl,kit.id))
        self.assertEqual(response.content, b'{"count":2}')
        ps.delete()
        kps.delete()
        response = self.client.get('{}{}/count/'.format(self.baseurl,kit.id))
        self.assertEqual(response.content, b'{"count":5}')
        kit = Kit.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/count/'.format(api_baseurl, kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        user = User.objects.get_or_create(username='baduser')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/count/'.format(self.baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        user.user_permissions.add(Permission.objects.get(name='Can view kit'))
        response = self.client.get('{}{}/count/'.format(self.baseurl,kit.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

# Test Inventory API KitPartStorage Views
class InventoryApiKitPartStorageViewTests(TestCase):
    def setUp(self):
        for i in range(65,70):
            if i != 65:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(101,201):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i).zfill(6), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part, organization=org)
                storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
                TenantStorage.objects.create(storage=storage, organization=org)
                kit = Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is an api view TestCase test kit', sku=str(i).zfill(6), price=10.00)
                TenantKit.objects.create(kit=kit, organization=org)
            for i in range(0,151):
                j = randint(101,200)
                k = randint(101,200)
                l = randint(20,50)
                part = Part.objects.get(name='Test_View_Part_{}'.format(j),tenant__organization=org)
                storage = Storage.objects.get(name='Test_View_Storage_{}'.format(k),tenant__organization=org)
                ps = PartStorage.objects.create(part=part, storage=storage, count=l)
                TenantPartStorage.objects.create(partstorage=ps, organization=org)
            for i in range(0,101):
                j = randint(101,199)
                l = randint(1,11)
                objs = PartStorage.objects.filter(tenant__organization=org)
                k = randint(0,objs.count()-1)
                ps = objs[k]
                kit = Kit.objects.get(name='Test_View_Kit_{}'.format(j),tenant__organization=org)
                kps = KitPartStorage.objects.create(kit=kit, partstorage=ps, count=l)
                TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=org)
        self.baseurl = '{}kitpartstorage/'.format(api_baseurl)
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view kit part storage','Can add kit part storage','Can change kit part storage','Can delete kit part storage']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=self.user,organization=Organization.objects.all()[i])
        self.client.force_login(self.user)

    def test_apiview_kitpartstorage_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kps = KitPartStorage.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"kit\":{}\"id\":{},".format('{', kps.kit.id))
        self.assertContains(response, "\"partstorage\":{}\"id\":{},".format('{', kps.partstorage.id))
        response = self.client.get(self.baseurl + '?search=' + kps.kit.name[0:4])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"kit\":{}\"id\":{},".format('{', kps.kit.id))
        self.assertContains(response, "\"partstorage\":{}\"id\":{},".format('{', kps.partstorage.id))
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_post(self):
        # Get Objects and Make Data
        orgs = helpers.get_user_organizations(self.user)
        kit = Kit.objects.filter(tenant__organization__in=orgs).first()
        ps = PartStorage.objects.filter(tenant__organization=kit.tenant.organization).last()
        data = {'kit': kit.id, 'partstorage': ps.id, 'count': 3, 'organization': kit.tenant.organization.id}
        # Run Specific Tests
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(KitPartStorage.objects.filter(kit=kit, partstorage=ps, count=3).first())
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(self.baseurl, {'kit':0,'partstorage':0,'count':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        org = Organization.objects.exclude(id__in=orgs).first()
        data = {'kit': kit.id, 'partstorage': ps.id, 'count': 3, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        org = kit.tenant.organization
        ps = PartStorage.objects.exclude(tenant__organization__in=orgs).last()
        data = {'kit': kit.id, 'partstorage': ps.id, 'count': 3, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        kit = Kit.objects.exclude(tenant__organization__in=orgs).last()
        data = {'kit': kit.id, 'partstorage': ps.id, 'count': 3, 'organization': org}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {'kit': kit.id, 'partstorage': ps.id, 'count': 3}
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.post(self.baseurl,data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_pk_get(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kps = KitPartStorage.objects.filter(tenant__organization__in=orgs).first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, kps.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "\"kit\":{}\"id\":{},".format('{', kps.kit.id))
        self.assertContains(response, "\"partstorage\":{}\"id\":{},".format('{', kps.partstorage.id))
        kps = KitPartStorage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.get('{}{}/'.format(self.baseurl, kps.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.get('{}{}/'.format(self.baseurl,kps.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_pk_put(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kps = KitPartStorage.objects.filter(tenant__organization__in=orgs).first()
        kps.count = 0
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, kps.id), KitPartStorageSerializer(kps).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(kps.id), 'count': 0}
        response = self.client.put('{}{}/'.format(self.baseurl, kps.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        kps.refresh_from_db()
        self.assertEqual(kps.count, 0)
        kps = KitPartStorage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.put('{}{}/'.format(self.baseurl, kps.id), data=KitPartStorageSerializer(kps).data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.put('{}{}/'.format(self.baseurl,kps.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_pk_patch(self):
        # Get Object and Make Data
        orgs = helpers.get_user_organizations(self.user)
        kps = KitPartStorage.objects.filter(tenant__organization__in=orgs).first()
        data = {'id': kps.id, 'count': 0}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, kps.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': kps.id, 'kit': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, kps.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        kps = KitPartStorage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.patch('{}{}/'.format(self.baseurl, kps.id), data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.patch('{}{}/'.format(self.baseurl,kps.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitpartstorage_pk_delete(self):
        # Get Object
        orgs = helpers.get_user_organizations(self.user)
        kps = KitPartStorage.objects.filter(tenant__organization__in=orgs).first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, self.baseurl, kps)
        # Run Specific Tests
        self.assertIsNone(KitPartStorage.objects.filter(id=kps.id).first())
        kps = KitPartStorage.objects.filter(tenant__organization__in=orgs).first()
        ps = kps.partstorage
        k = kps.kit
        response = self.client.delete('{}{}/'.format(self.baseurl, kps.id))
        self.assertContains(response, "\"kit\":{}\"id\":{},".format('{', k.id))
        self.assertContains(response, "\"partstorage\":{}\"id\":{},".format('{', ps.id))
        kps = KitPartStorage.objects.exclude(tenant__organization__in=orgs).first()
        response = self.client.delete('{}{}/'.format(self.baseurl, kps.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.force_login(User.objects.get_or_create(username='baduser')[0])
        response = self.client.delete('{}{}/'.format(self.baseurl,kps.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class TenantApiOrganizationViewTests(TestCase):
    def setUp(self):
        for i in range(70,80):
            if i != 70:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        self.baseurl = '{}organization/'.format(api_tenant_baseurl)
        self.user = User.objects.get_or_create(username='teststaff',is_staff=True)[0]
        for name in ['Can view organization','Can add organization','Can change organization','Can delete organization']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_organization_get(self):
        # Get Object
        i = randint(70,79)
        org = Organization.objects.get(name='Test_View_Organization_{}'.format(i))
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"id":{}'.format(org.id))
        response = self.client.get(self.baseurl + '?search=' + org.name[0:4])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"id":{}'.format(org.id))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_organization_post(self):
        data = {'name': 'New World Organization', 'description': 'A New Organization'}
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        org = Organization.objects.filter(name='New World Organization').first()
        self.assertIsNotNone(org)
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.post(self.baseurl, {'name':'bad','count':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {'name': 'New World HR', 'description': 'A New HR Organization', 'parent': org.id}
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_organization_pk_get(self):
        # Get Object
        org = Organization.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, org.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"id":{}'.format(org.id))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get('{}{}/'.format(self.baseurl, org.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_organization_pk_put(self):
        # Get Object
        i = randint(70,79)
        org = Organization.objects.get(name='Test_View_Organization_{}'.format(i))
        if org.parent is None:
            data = {'name': org.name, 'description': 'A New Description', 'parent': ''}
        else:
            data = {'name': org.name, 'description': 'A New Description', 'parent': org.parent.id}
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, org.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'A New Description')
        data = {'name': org.name}
        response = self.client.put('{}{}/'.format(self.baseurl, org.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.put('{}{}/'.format(self.baseurl, org.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_organization_pk_patch(self):
        # Get Object
        i = randint(70,79)
        org = Organization.objects.get(name='Test_View_Organization_{}'.format(i))
        if org.parent is None:
            data = {'name': org.name, 'description': 'A New Description', 'parent': ''}
        else:
            data = {'name': org.name, 'description': 'A New Description', 'parent': org.parent.id}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, org.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'A New Description')
        data = {'name': 'New {}'.format(org.name)}
        response = self.client.patch('{}{}/'.format(self.baseurl, org.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        baddata = {'parent': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, org.id), baddata, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.patch('{}{}/'.format(self.baseurl, org.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_organization_pk_delete(self):
        # Get Object
        i = randint(71,79)
        org = Organization.objects.get(name='Test_View_Organization_{}'.format(i))
        # Run Specific Tests
        response = self.client.delete('{}{}/'.format(self.baseurl, org.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        if i == 79:
            i = 70
        else:
            i += 1
        org = Organization.objects.get(name='Test_View_Organization_{}'.format(i))
        response = self.client.delete('{}{}/'.format(self.baseurl, org.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class TenantApiUserOrganizationViewTests(TestCase):
    def setUp(self):
        for i in range(80,90):
            if i != 80:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for i in range(0,100):
            j = randint(80,89)
            user = User.objects.create(username='test_user_{}'.format(i),first_name='test',last_name='user_{}'.format(i))
            UserOrganization.objects.create(user=user,organization=Organization.objects.get(name='Test_View_Organization_{}'.format(j)))
        for i in range(0,randint(30,60)):
            j = randint(80,89)
            k = randint(0,99)
            user = User.objects.get(username='test_user_{}'.format(k))
            org = Organization.objects.get(name='Test_View_Organization_{}'.format(j))
            if UserOrganization.objects.filter(user=user,organization=org).first() is None:
                UserOrganization.objects.create(user=user,organization=org)
        self.baseurl = '{}userorganization/'.format(api_tenant_baseurl)
        self.user = User.objects.get_or_create(username='teststaff',is_staff=True)[0]
        for name in ['Can view user organization','Can add user organization','Can delete user organization']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)


    def test_apiview_userorganization_get(self):
        # Get Object
        i = randint(0,99)
        user = User.objects.get(username='test_user_{}'.format(i))
        uo = UserOrganization.objects.filter(user=user).first()
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"organization":{}'.format(uo.organization.id))
        response = self.client.get(self.baseurl + '?search=' + user.username[0:4])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"user":{}'.format(user.id))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_userorganization_post(self):
        i = randint(0,99)
        user = User.objects.get(username='test_user_{}'.format(i))
        orgs = helpers.get_user_organizations(user)
        org = Organization.objects.exclude(id__in=orgs).first()
        data = {'user': user.id, 'organization': org.id}
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        uo = UserOrganization.objects.filter(user=user,organization=org).first()
        self.assertIsNotNone(uo)
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(self.baseurl, {'user':0,'organization':0})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.post(self.baseurl, data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_userorganization_pk_get(self):
        # Get Object
        userorg = UserOrganization.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, userorg.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"user":{}'.format(userorg.user.id))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get('{}{}/'.format(self.baseurl, userorg.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_apiview_userorganization_pk_delete(self):
        # Get Object
        userorg = UserOrganization.objects.last()
        response = self.client.delete('{}{}/'.format(self.baseurl, userorg.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        userorg = UserOrganization.objects.last()
        response = self.client.delete('{}{}/'.format(self.baseurl, userorg.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class TestGenerics():
    def get_serialized_data(self, obj):
    # Determine the model and return the data serialized
        if obj.__class__.__name__ == 'Part':
            return PartReadSerializer(obj).data
        if obj.__class__.__name__ == 'Storage':
            return StorageReadSerializer(obj).data
        if obj.__class__.__name__ == 'AlternateSKU':
            return AlternateSKUReadSerializer(obj).data
        if obj.__class__.__name__ == 'PartAlternateSKU':
            return PartAlternateSKUReadSerializer(obj).data
        if obj.__class__.__name__ == 'PartStorage':
            return PartStorageReadSerializer(obj).data
        if obj.__class__.__name__ == 'Assembly':
            return AssemblyReadSerializer(obj).data
        if obj.__class__.__name__ == 'Kit':
            return KitReadSerializer(obj).data
        if obj.__class__.__name__ == 'KitPartStorage':
            return KitPartStorageReadSerializer(obj).data
        else:
            return json.dumps(obj)

    def get_completed_url(self, url, obj_or_data):
        serialized = self.get_serialized_data(obj_or_data)
        if isinstance(serialized, str):
            serialized = json.loads(serialized)
        return '{}{}/'.format(url,serialized.get('id'))

    def get(self, case, url, obj):
        # GET Valid Request
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertContains(response, obj)

    def post(self, case, url, data, dup=True):
        # POST Valid Data
        response = case.client.post(url, data)
        case.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # POST Duplicate Data
        if dup:
            response = case.client.post(url, data)
            case.assertTrue((response.status_code == status.HTTP_200_OK) or (response.status_code == status.HTTP_201_CREATED))
        # POST Invalid Data
        bad_data = {'name': 'bad'}
        response = case.client.post(url, bad_data)
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def pk_get(self, case, url, obj):
        # GET Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.get(valid)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertContains(response, obj)
        # GET Invalid Request
        response = case.client.get('{}0/'.format(url))
        case.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def pk_put(self, case, url, obj):
        serialized = self.get_serialized_data(obj)
        # PUT Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.put(valid, serialized, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # PUT Invalid Request
        bad_data = {'id': obj.id, 'name': obj.name}
        response = case.client.put(valid, bad_data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def pk_patch(self, case, url, data):
        # PATCH Valid Data
        valid = self.get_completed_url(url, data)
        response = case.client.patch(valid, data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # PATCH Invalid Data
        bad_data = {'name': 'bad'.zfill(1000)}
        response = case.client.patch(valid, bad_data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def pk_delete(self, case, url, obj):
        # DELETE Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.delete(valid)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
