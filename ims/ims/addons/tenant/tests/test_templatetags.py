import django
django.setup()
from django.contrib.auth.models import User
from django.test import TestCase
from random import randint

from ims.addons.tenant.models import Organization, UserOrganization
from ims.addons.tenant.templatetags.organizations import get_organizations

class OrganizationsFilterTests(TestCase):
    def setUp(self):
        for i in range(0,randint(2,4)):
            Organization.objects.create(name='test_tenant_organization_{}'.format(i),description='Test Tenant Organization')
        for i in range(0,randint(2,12)):
            j = randint(0,len(Organization.objects.all())-1)
            user = User.objects.create(username='test_tenant_user_{}'.format(i),first_name='test_tenant',last_name='user_{}'.format(i))
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[j])

    def test_simple_get_organizations(self):
        i = randint(0, len(User.objects.all())-1)
        user = User.objects.all()[i]
        orgs = get_organizations(user)
        uo = UserOrganization.objects.filter(user=user).first()
        self.assertTrue(uo.organization in orgs)

    def test_complex_get_organizations(self):
        user = User.objects.create(username='test_tenant_user_temp',first_name='test_tenant',last_name='user_temp')
        expected = Organization.objects.all()
        for org in expected:
            UserOrganization.objects.create(user=user,organization=org)
        current = get_organizations(user)
        for org in current:
            self.assertTrue(org in expected)
