import django
django.setup()
from django.contrib.auth.models import User, Permission
from django.test import TestCase, override_settings
from random import randint
from rest_framework import status
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from ims.addons.tenant.models import Organization, UserOrganization, TenantPart, TenantStorage, TenantAlternateSKU, TenantPartAlternateSKU, TenantPartStorage, TenantAssembly, TenantKit, TenantKitPartStorage
import ims.addons.tenant.helpers as helpers

baseurl = '/inventory/'

# Test ims view redirect
class ImsViewTests(TestCase):
    def test_unit_root_view(self):
        response = self.client.get('/')
        self.assertTrue(response.status_code, status.HTTP_301_MOVED_PERMANENTLY)
        self.assertTrue(response.url, '/inventory/')

# Test inventory views
class InventoryPartViewTests(TestCase):
    def setUp(self):
        for i in range(0,5):
            if i != 0:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,101):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i*10).zfill(5), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part,organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = '{}part/'.format(baseurl)

    def test_view_parts(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertRedirects(response, '/accounts/login/?next=/inventory/part/')
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_part')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/part/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')

    def test_view_part_details(self):
        # Test a valid part
        i = randint(1,100)
        user = User.objects.get_or_create(username='testuser')[0]
        org = helpers.get_user_organizations(user).first()
        p = Part.objects.get(name='Test_View_Part_{}'.format(i),tenant__organization=org)
        response = self.client.get('{}{}/'.format(self.baseurl, p.id))
        self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, p.id))
        permission = Permission.objects.get(codename='view_part')
        user.user_permissions.add(permission)
        UserOrganization.objects.get_or_create(user=user, organization=p.tenant.organization)
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, p.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/part/detail.html')
        # Test an invalid part
        response = self.client.get('{}1000000/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        UserOrganization.objects.get(user=user, organization=p.tenant.organization).delete()
        response = self.client.get('{}{}/'.format(self.baseurl, p.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class InventoryStorageViewTests(TestCase):
    def setUp(self):
        for i in range(5,10):
            if i != 5:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,22):
                if i < 5:
                    storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
                    TenantStorage.objects.create(storage=storage,organization=org)
                else:
                    k = randint(0,4)
                    parent = Storage.objects.get(name='Test_View_Storage_{}'.format(k),tenant__organization=org)
                    storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage', parent=parent)
                    TenantStorage.objects.create(storage=storage,organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = '{}storage/'.format(baseurl)

    def test_view_storages(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_storage')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/storage/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')

    def test_view_storage_detail(self):
        # Test a valid storage
        i = randint(0,21)
        user = User.objects.get_or_create(username='testuser')[0]
        org = helpers.get_user_organizations(user).first()
        s = Storage.objects.get(name='Test_View_Storage_{}'.format(i),tenant__organization=org)
        response = self.client.get('{}{}/'.format(self.baseurl, s.id))
        self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, s.id))
        permission = Permission.objects.get(codename='view_storage')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, s.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/storage/detail.html')
        # Test Anonymous Access Restriction
        # Test an invalid storage
        response = self.client.get('{}100000/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        UserOrganization.objects.get(user=user, organization=s.tenant.organization).delete()
        response = self.client.get('{}{}/'.format(self.baseurl, s.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_storage_hierarchy_list(self):
        i = randint(0,4)
        user = User.objects.get_or_create(username='testuser')[0]
        org = helpers.get_user_organizations(user).first()
        s = Storage.objects.get(name='Test_View_Storage_{}'.format(i),tenant__organization=org)
        s2 = s.get_children().first()
        for i in range(0,randint(1,5)):
            storage = Storage.objects.create(name='Test_Hierarchy_Storage_{}_{}'.format(s2.id,i), description='short-lived test storage', parent=s2)
            TenantStorage.objects.create(storage=storage, organization=org)
        response = self.client.get('/inventory/container/')
        self.assertRedirects(response, '/accounts/login/?next={}'.format('/inventory/container/'))
        permission = Permission.objects.get(codename='view_storage')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('/inventory/container/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_storage_hierarchy_detail(self):
        i = randint(0,4)
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        s = Storage.objects.get(name='Test_View_Storage_{}'.format(i),tenant__organization=org)
        for i in range(1101,1200):
            part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='short-lived test part', sku=str(i).zfill(6), price=1.0, cost=0.5)
            TenantPart.objects.create(part=part, organization=org)
        for child in s.get_children():
            storage = Storage.objects.create(name='Test_Hierarchy_Storage_{}'.format(child.id), description='short-lived test storage', parent=child)
            TenantStorage.objects.create(storage=storage, organization=org)
            i = randint(1,5)
            j = randint(1101,1199)
            for k in range(0,i):
                ps = PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j),tenant__organization=org), storage=child, count=5)
                TenantPartStorage.objects.create(partstorage=ps,organization=org)
                ps = PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j),tenant__organization=org), storage=storage, count=10)
                TenantPartStorage.objects.create(partstorage=ps,organization=org)

        # Tests
        url = '/inventory/container/{}/'.format(s.id)
        response = self.client.get(url)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(url))
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_storage')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class InventoryAlternateSKUViewTests(TestCase):
    def setUp(self):
        for i in range(10,15):
            if i != 10:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(16,21):
                altsku = AlternateSKU.objects.create(manufacturer='ZephyrusComputing', sku=str(i).zfill(4))
                TenantAlternateSKU.objects.create(alternatesku=altsku, organization=org)
            for i in range(701,801):
                part = Part.objects.create(name='Test_Part_{}'.format(i), description='This is a view TestCase test part', sku='1' + str(i), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part, organization=org)
            for i in range(0,6):
                j = randint(16,20)
                k = randint(701,800)
                part = Part.objects.get(name='Test_Part_{}'.format(k),tenant__organization=org)
                alt_sku = AlternateSKU.objects.get(sku=str(j).zfill(4),tenant__organization=org)
                pa = PartAlternateSKU.objects.create(part=part, alt_sku=alt_sku)
                TenantPartAlternateSKU.objects.create(partalternatesku=pa, organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = '{}altsku/'.format(baseurl)


    def test_view_alternates(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_alternatesku')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/partaltsku/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-5'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-5')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')

class InventoryAssemblyViewTests(TestCase):
    def setUp(self):
        for i in range(15,20):
            if i != 15:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(901,1001):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i*10).zfill(5), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part, organization=org)
            for i in range(0,11):
                j = randint(901,1000)
                parent = Part.objects.get(name='Test_View_Part_{}'.format(j),tenant__organization=org)
                k = randint(7,14)
                for l in range(0,k):
                    m = randint(901,1000)
                    part = Part.objects.get(name='Test_View_Part_{}'.format(m),tenant__organization=org)
                    assembly = Assembly.objects.create(parent=parent, part=part, count=1)
                    TenantAssembly.objects.create(assembly=assembly, organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = '{}assembly/'.format(baseurl)

    def test_view_assemblies(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_assembly')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/assembly/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-5'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-5')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')

    def test_view_assembly_detail(self):
        # Test a valid alternate
        user = User.objects.get_or_create(username='testuser')[0]
        org = helpers.get_user_organizations(user).first()
        assemblies = Assembly.objects.filter(tenant__organization=org)
        i = randint(0, assemblies.count() -1)
        a = assemblies[i]
        response = self.client.get('{}{}/'.format(self.baseurl, a.parent.id))
        self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, a.parent.id))
        permission = Permission.objects.get(codename='view_assembly')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, a.parent.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/assembly/detail.html')
        # Test a valid part without assembly
        parts = Part.objects.filter(tenant__organization=a.tenant.organization)
        while True:
            j = randint(0,parts.count() -1)
            part = parts[j]
            if len(Assembly.objects.filter(part=part)) == 0:
                response = self.client.get('{}{}/'.format(self.baseurl, part.id))
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                #self.assertTemplateUsed(response, 'inventory/assembly/detail.html')
                break;
        # Test an invalid part
        response = self.client.get('{}1000000/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get('{}{}/'.format(self.baseurl, a.parent.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class InventoryKitViewTests(TestCase):
    def setUp(self):
        for i in range(20,25):
            if i != 20:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(1001,1100):
                kit = Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a view TestCase test kit', sku=str(i*10).zfill(5), price=10.00)
                TenantKit.objects.create(kit=kit, organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = '{}kit/'.format(baseurl)
            
    def test_view_kits(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_kit')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/kit/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
            
    def test_view_kit_details(self):
        # Test a valid kit
        i = randint(1001,1099)
        user = User.objects.get_or_create(username='testuser')[0]
        org = helpers.get_user_organizations(user).first()
        k = Kit.objects.get(name='Test_View_Kit_{}'.format(i),tenant__organization=org)
        url = '{}{}/'.format(self.baseurl, k.id)
        response = self.client.get(url)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(url))
        permission = Permission.objects.get(codename='view_kit')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertTemplateUsed(response, 'inventory/kit/detail.html')
        # Test an invalid part
        response = self.client.get('{}1000000/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get('{}{}/'.format(self.baseurl, k.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class AJaxViewTests(TestCase):
    def setUp(self):
        for i in range(25,30):
            if i != 25:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,100):
                part = Part.objects.create(name='Test_View_Part_Ajax{}'.format(i), description='This is a view TestCase test part', sku='ajax{}'.format(i), price=9.99, cost=4.49)
                TenantPart.objects.create(part=part, organization=org)
                storage = Storage.objects.create(name='Test_View_Storage_Ajax{}'.format(i), description='This is a view TestCase test storage')
                TenantStorage.objects.create(storage=storage, organization=org)
            for i in range(0,5):
                kit = Kit.objects.create(name='Test_View_Kit_Ajax{}'.format(i), description='This is a view TestCase test kit', sku='ajax{}'.format(i), price=12.50)
                TenantKit.objects.create(kit=kit, organization=org)
            for i in range(0,21):
                j = randint(0,99)
                k = randint(0,99)
                part = Part.objects.get(name='Test_View_Part_Ajax{}'.format(j),tenant__organization=org)
                storage = Storage.objects.get(name='Test_View_Storage_Ajax{}'.format(k),tenant__organization=org)
                ps = PartStorage.objects.create(part=part, storage=storage, count=5)
                TenantPartStorage.objects.create(partstorage=ps, organization=org)
            for kit in Kit.objects.filter(tenant__organization=org):
                for i in range(0,5):
                    j = randint(0,20)
                    ps = PartStorage.objects.filter(tenant__organization=org)[j]
                    kps = KitPartStorage.objects.create(kit=kit, partstorage=ps, count=2)
                    TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])

    def test_ajax_kit_subtract(self):
        user = User.objects.get_or_create(username='testuser')[0]
        self.baseurl = '{}kit/'.format(baseurl)
        org = helpers.get_user_organizations(user).first()
        kit = Kit.objects.filter(tenant__organization=org).first()
        self.assertEqual(kit.get_available_count(), 2)
        url = '{}{}/sub/1/'.format(self.baseurl, kit.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='decrement_kit')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Insufficient parts')
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Access is Denied')

    def test_ajax_kit_add(self):
        user = User.objects.get_or_create(username='testuser')[0]
        self.baseurl = '{}kit/'.format(baseurl)
        org = helpers.get_user_organizations(user).first()
        kit = Kit.objects.filter(tenant__organization=org).first()
        self.assertEqual(kit.get_available_count(), 2)
        url = '{}{}/add/1/'.format(self.baseurl, kit.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='increment_kit')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Access is Denied')

    def test_ajax_kit_update(self):
        user = User.objects.get_or_create(username='testuser')[0]
        self.baseurl = '{}kit/'.format(baseurl)
        org = helpers.get_user_organizations(user).first()
        kits = Kit.objects.filter(tenant__organization=org)
        i = randint(0,kits.count() -1)
        kit = kits[i]
        self.assertEqual(kit.get_available_count(), 2)
        url = '{}{}/update/kitdetails/'.format(self.baseurl, kit.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        permission = Permission.objects.get(codename='view_kit')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
        self.assertContains(response, kit.description)
        url = '{}{}/update/kitcomponents/'.format(self.baseurl, kit.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_ajax_partstorage_add(self):
        user = User.objects.get_or_create(username='testuser')[0]
        self.baseurl = '{}partstorage/'.format(baseurl)
        org = helpers.get_user_organizations(user).first()
        ps = PartStorage.objects.filter(tenant__organization=org).first()
        ps.count = 2
        ps.save()
        url = '{}{}/add/1/'.format(self.baseurl, ps.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        permission = Permission.objects.get(codename='increment_partstorage')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'green')
        ps.refresh_from_db()
        self.assertEqual(ps.count, 3)
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Access is Denied')

    def test_ajax_partstorage_sub(self):
        user = User.objects.get_or_create(username='testuser')[0]
        self.baseurl = '{}partstorage/'.format(baseurl)
        org = helpers.get_user_organizations(user).first()
        ps = PartStorage.objects.filter(tenant__organization=org).last()
        ps.count = 2
        ps.save()
        url = '{}{}/sub/1/'.format(self.baseurl, ps.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        permission = Permission.objects.get(codename='decrement_partstorage')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'green')
        ps.refresh_from_db()
        self.assertEqual(ps.count, 1)
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')
        self.assertContains(response, 'Access is Denied')

    def test_ajax_partstorage_update(self):
        user = User.objects.get_or_create(username='testuser')[0]
        self.baseurl = '{}partstorage/'.format(baseurl)
        org = helpers.get_user_organizations(user).first()
        pss = PartStorage.objects.filter(tenant__organization=org)
        i = randint(0,pss.count() -1)
        ps = pss[i]
        url = '{}{}/update/pspart/'.format(self.baseurl, ps.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        permission = Permission.objects.get(codename='view_partstorage')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
        self.assertContains(response, ps.part.name)
        url = '{}{}/update/psstorage/'.format(self.baseurl, ps.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result2.html')
        self.assertContains(response, ps.storage.name)
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_ajax_dropdown_list(self):
        self.baseurl = '/tenant/ajax/'
        url = '{}part/?selected={}'.format(self.baseurl, Organization.objects.first().id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_login(user)
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        for model in 'part','storage','kit','partstorage','alternatesku':
            permission = Permission.objects.get(codename='view_{}'.format(model))
            user.user_permissions.add(permission)
            url = '{}{}/?selected={}'.format(self.baseurl, model, org.id)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        UserOrganization.objects.get(user=user, organization=org).delete()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class StaticPageViewTests(TestCase):
    def test_view_about(self):
        url = '{}about/'.format(baseurl)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.client.force_login(User.objects.get_or_create(username='teststaff',is_staff=1)[0])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_login(User.objects.get_or_create(username='newuser',is_staff=0)[0])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertTrue(response.url, '/inventory/')

    def test_view_search(self):
        response = self.client.get('{}search/'.format(baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}search/'.format(baseurl))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get('{}search/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class SearchViewTests(TestCase):
    def setUp(self):
        org = Organization.objects.create(name='Test_View_Organization_Search1', description='This is a view TestCase test organization')
        part = Part.objects.create(name='Test_View_Assembly_Search1', description='This is a view TestCase test part', sku='searchass1', price=10.00, cost=5.00)
        TenantPart.objects.create(part=part, organization=org)
        for i in range(1,3):
            part = Part.objects.create(name='Test_View_Part_Search{}'.format(i), description='This is a view TestCase test part', sku='searchpart{}'.format(i), price=5.00, cost=2.50)
            TenantPart.objects.create(part=part, organization=org)
        assembly = Assembly.objects.create(parent=Part.objects.get(name='Test_View_Assembly_Search1'), part=Part.objects.get(name='Test_View_Part_Search1'), count=1)
        TenantAssembly.objects.create(assembly=assembly, organization=org)
        storage = Storage.objects.create(name='Test_View_Storage_Search1', description='This is a view TestCase test storage')
        TenantStorage.objects.create(storage=storage, organization=org)
        ps = PartStorage.objects.create(storage=Storage.objects.get(name='Test_View_Storage_Search1'), part=Part.objects.get(name='Test_View_Assembly_Search1'), count=1)
        TenantPartStorage.objects.create(partstorage=ps, organization=org)
        ps = PartStorage.objects.create(storage=Storage.objects.get(name='Test_View_Storage_Search1'), part=Part.objects.get(name='Test_View_Part_Search2'), count=1)
        TenantPartStorage.objects.create(partstorage=ps, organization=org)
        altsku = AlternateSKU.objects.create(sku='ZephyrusSearch1', manufacturer='Zephyrus Computing')
        TenantAlternateSKU.objects.create(alternatesku=altsku, organization=org)
        pa = PartAlternateSKU.objects.create(part=Part.objects.get(name='Test_View_Assembly_Search1'), alt_sku=AlternateSKU.objects.get(sku='ZephyrusSearch1'))
        TenantPartAlternateSKU.objects.create(partalternatesku=pa, organization=org)
        kit = Kit.objects.create(name='Test_View_Kit_Search1', description='This is a view TestCase test kit', sku='searchkit', price=10.00)
        TenantKit.objects.create(kit=kit, organization=org)
        kps = KitPartStorage.objects.create(kit=Kit.objects.get(name='Test_View_Kit_Search1'), partstorage=PartStorage.objects.first(), count=1)
        TenantKitPartStorage.objects.create(kitpartstorage=kps, organization=org)
        user = User.objects.create(username='testuser')
        UserOrganization.objects.create(user=user,organization=org)
        self.client.force_login(user)
        self.baseurl = '{}search/'.format(baseurl)

    def test_view_search_part(self):
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_part')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}?m=part&q=2&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Part_Search2')
        self.assertNotContains(response, 'Test_View_Assembly_Search1')
        response = self.client.get('{}?m=part&q=1&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Part_Search')
        self.assertContains(response, 'Test_View_Assembly_Search1')

    def test_view_search_assembly(self):
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_assembly')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}?m=assembly&q=1&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Assembly_Search1')
        self.assertNotContains(response, 'Test_View_Part_Search1')
        response = self.client.get('{}?m=assembly&q=2&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Assembly_Search1')

    def test_view_search_alternatesku(self):
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_alternatesku')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}?m=alternatesku&q=1&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Zephyrus Computing-ZephyrusSearch1')
        response = self.client.get('{}?m=alternatesku&q=test&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Zephyrus Computing-ZephyrusSearch1')

    def test_view_search_storage(self):
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_storage')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}?m=storage&q=1&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Storage_Search1')
        response = self.client.get('{}?m=storage&q=2&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Storage_Search1')

    def test_view_search_kit(self):
        user = User.objects.get_or_create(username='testuser')[0]
        permission = Permission.objects.get(codename='view_kit')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}?m=kit&q=1&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Kit_Search1')
        response = self.client.get('{}?m=kit&q=2&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Kit_Search1')

    def test_view_search_querytype(self):
        user = User.objects.get_or_create(username='testuser')[0]
        for model in 'part','storage','alternatesku','kit':
            permission = Permission.objects.get(codename='view_{}'.format(model))
            user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}?m=storage&q=1&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Storage_Search1')
        response = self.client.get('{}?m=part&q=Test_View_Part_Search2&t=2'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Part_Search')
        self.assertNotContains(response, 'Test_View_Part_Search1')
        response = self.client.get('{}?m=part&q=Test_View_Part_Search&t=2'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Part_Search1')
        response = self.client.get('{}?m=alternatesku&q=Zephyrus&t=1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Zephyrus Computing-ZephyrusSearch1')
        response = self.client.get('{}?m=alternatesku&q=Test&t=1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Zephyrus Computing-ZephyrusSearch1')
        response = self.client.get('{}?m=kit&q=Test&t=2'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Test_View_Kit_Search1')

    def test_view_search_multiple(self):
        user = User.objects.get_or_create(username='testuser')[0]
        for model in 'part','storage','alternatesku':
            permission = Permission.objects.get(codename='view_{}'.format(model))
            user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}?m=storage&m=part&q=1&t=3'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Storage_Search1')
        self.assertContains(response, 'Test_View_Part_Search1')
        response = self.client.get('{}?m=storage&m=alternatesku&q=Test&t=1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Test_View_Storage_Search1')
        self.assertNotContains(response, 'Zephyrus Computing-ZephyrusSearch1')

