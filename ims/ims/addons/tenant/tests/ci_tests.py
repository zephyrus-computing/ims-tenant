import requests
import json
from django.contrib.auth.models import User, Permission, Group
from random import randint
from rest_framework import status
from rest_framework.authtoken.models import Token
from inventory.models import Part,Storage,AlternateSKU,PartAlternateSKU,PartStorage,Assembly

bad_response_threshold = 3
bad_response_count = 0
api_version = 'v1'
api_module_inv = 'inventory'
api_module_ten = 'tenant'
api_baseurl = '/api/{}/{}/'.format(api_version, api_module_inv)
hostname = 'localhost:8000'
proto = 'http'
baseuri = '{}://{}{}'.format(proto, hostname, api_baseurl)
baseuri_tenant = '{}://{}/api/{}/{}/'.format(proto, hostname, api_version, api_module_ten)

s = requests.Session()
s2 = requests.Session()

print("Generating ci_test_user and validating webserver")
ci_user = User.objects.get_or_create(username='ci_test_user',email='support@zephyruscomputing.com',first_name='ci_test',last_name='user',is_superuser=True,is_staff=True)[0]
token = Token.objects.get_or_create(user=ci_user)[0]
s.headers['Authorization'] = 'Token {}'.format(token.key)
try:
    response = s.get('{}part/'.format(baseuri))
except:
    print("Exception while trying to get web response")
    ci_user.delete()
    exit()
if not response.ok:
    print("Web response was not OK")
    ci_user.delete()
    exit()

try:
    print("Generating users and API token")
    for i in range(0,4):
        for j in range(0,randint(3,10)):
            user2 = User.objects.get_or_create(username='Tenant{}_User{}'.format(i,j),email='support+{}-{}@zephyruscomputing.com'.format(i,j),first_name='Tenant{}'.format(i),last_name='User{}'.format(j))[0]
            Token.objects.get_or_create(user=user2)

    print("Assigning Permissions to Users...")
    inventory_permissions = []
    for perm in Permission.objects.filter(content_type__app_label='inventory'):
        inventory_permissions.append(perm.id)
    group = Group.objects.get_or_create(name='inventory_admin')[0]
    group.permissions.set(inventory_permissions)
    for user in User.objects.exclude(username='ci_test_user'):
        user.groups.add(group)

    def _check_response(response, model, data=''):
        if model == 'SubPart':
            if response.status_code == 409:
                return
        if not response.ok:
            print('++++++++++++++++++++')
            if data != '':
                print("Failed while creating {} with data \"{}\"".format(model, data))
            else:
                print("Failed to retrieve {}".format(model))
            print(response.status_code)
            print('--------------------')
            print(response.content)
            print('====================')
            if bad_response_count >= bad_response_threshold:
                raise Exception("Bad Response Threshold Met: {}".format(bad_response_threshold))
            else:
                bad_response_count += 1

    def _get_session_token(tenant_id):
        response = s.get('{}userorganization/'.format(baseuri_tenant))
        _check_response(response,'UserOrganization')
        all_userorgs = json.loads(response.text)
        user_ids = []
        for userorg in all_userorgs:
            if userorg['organization'] == tenant_id:
                if userorg['user'] != ci_user.id:
                    user_ids.append(userorg['user'])
        i = randint(0,len(user_ids) -1)
        user2 = User.objects.get(id=user_ids[i])
        return Token.objects.get_or_create(user=user2)[0]

    print("Creating Tenants...")
    data = {'name':'Zephyrus Computing','description':'SaaS Provider','parent':''}
    response = s.post('{}organization/'.format(baseuri_tenant), data=data)
    _check_response(response,'Organization',data)
    zc_tenant_id = json.loads(response.text)['id']

    tenant_array = [{'name':'Alpha','description':'First greek letter','parent':''},{'name':'Zeta','description':'Last Latin letter','parent':''},{'name':'Gort','description':'Gaelic letter G','parent':''}]

    for data in tenant_array:
        response = s.post('{}organization/'.format(baseuri_tenant), data=data)
        _check_response(response,'Organization',data)

    print("Creating Tenant User Relationships...")
    users = User.objects.all()
    response = s.get('{}organization/'.format(baseuri_tenant))
    _check_response(response,'Organization')
    tenants = json.loads(response.text)
    for user in users:
        if user.username != 'ci_test_user':
            if user.first_name != '':
                i = user.first_name[-1]
                data = {'user':user.id,'organization':tenants[int(i)]['id']}
                response = s.post('{}userorganization/'.format(baseuri_tenant), data=data)
                _check_response(response,'UserOrganization',data)
    for tenant in tenants:
        data = {'user':ci_user.id,'organization':tenant['id']}
        response = s.post('{}userorganization/'.format(baseuri_tenant), data=data)
        _check_response(response,'UserOrganization',data)

    print("Creating Storage Hierarchy for Zephyrus Computing...")
    token2 = _get_session_token(zc_tenant_id)
    s2.headers['Authorization'] = 'Token {}'.format(token2.key)

    data = {'name':'Warehouse','description':'The main building','parent':'','organization':zc_tenant_id}
    response = s2.post('{}storage/'.format(baseuri),data=data)
    _check_response(response,'Storage',data)
    warehouse_id = json.loads(response.text)['id']

    data = {'name':'Manufacturing Area','description':'The floor space used for manufacturing parts','parent':warehouse_id,'organization':zc_tenant_id}
    response = s2.post('{}storage/'.format(baseuri),data=data)
    _check_response(response,'Storage',data)
    manufacturing_area_id = json.loads(response.text)['id']

    storage_array = [{'name':'Workstation 1','description':'Big CNC Machine Workspace','parent':manufacturing_area_id,'organization':zc_tenant_id},{'name':'Workstation 2','description':'Smaller CNC Workspace','parent':manufacturing_area_id,'organization':zc_tenant_id}]
    for data in storage_array:
        response = s2.post('{}storage/'.format(baseuri),data=data)
        _check_response(response,'Storage',data)

    data = {'name':'Office Area','description':'The office spaces','parent':warehouse_id,'organization':zc_tenant_id}
    response = s2.post('{}storage/'.format(baseuri),data=data)
    _check_response(response,'Storage',data)
    office_area_id = json.loads(response.text)['id']

    storage_array = [{'name':'Common Room','description':'Central office room','parent':office_area_id,'organization':zc_tenant_id},{'name':'Conference Room','description':'Telecommunications room','parent':office_area_id,'organization':zc_tenant_id},{'name':'Office 1','description':'Private office 1','parent':office_area_id,'organization':zc_tenant_id},{'name':'Office 2','description':'Private office 2','parent':office_area_id,'organization':zc_tenant_id}]

    for data in storage_array:
        response = s2.post('{}storage/'.format(baseuri),data=data)
        _check_response(response,'Storage',data)

    data = {'name':'Storage Area','description':'The primary storage space of totes and bins','parent':warehouse_id,'organization':zc_tenant_id}
    response = s2.post('{}storage/'.format(baseuri),data=data)
    _check_response(response,'Storage',data)
    storage_area_id = json.loads(response.text)['id']

    storage_array = [{'name':'Row 1','description':'First Row of Shelves','parent':storage_area_id,'organization':zc_tenant_id},{'name':'Row 2','description':'Second Row of Shelves','parent':storage_area_id,'organization':zc_tenant_id},{'name':'Row 3','description':'Third Row of Shelves','parent':storage_area_id,'organization':zc_tenant_id}]
    i = 1
    colors = ['blank','Grey','Blue','Yellow','Green','Azure','Purple','Lavender','Red','Aqua','Cyan','Magenta','Amber']
    for data in storage_array:
        response = s2.post('{}storage/'.format(baseuri),data=data)
        _check_response(response,'Storage',data)
        row_id = json.loads(response.text)['id']
        row_num = data['name'][-1]
        storage_array2 = [{'name':'Shelf {}-1'.format(row_num),'description':'Shelf 1 in row {}'.format(row_num),'parent':row_id,'organization':zc_tenant_id},{'name':'Shelf {}-2'.format(row_num),'description':'Shelf 2 in row {}'.format(row_num),'parent':row_id,'organization':zc_tenant_id}]
        for data2 in storage_array2:
            response2 = s2.post('{}storage/'.format(baseuri),data=data2)
            _check_response(response2,'Storage',data2)
            shelf_id = json.loads(response2.text)['id']
            storage_array3 = [{'name':'Tote {}'.format(i),'description':'{} Tote'.format(colors[i]),'parent':shelf_id,'organization':zc_tenant_id},{'name':'Tote {}'.format(i+1),'description':'{} Tote'.format(colors[i+1]),'parent':shelf_id,'organization':zc_tenant_id}]
            for data3 in storage_array3:
                response3 = s2.post('{}storage/'.format(baseuri),data=data3)
                _check_response(response3,'Storage',data3)
                i+=1

    print("Creating Storage Hierarchy for other tenants...")
    for tenant in tenants:
        token2 = _get_session_token(tenant['id'])
        s2.headers['Authorization'] = 'Token {}'.format(token2.key)
        if tenant['name'] != 'Zephyrus Computing':
            data = {'name':'{} Root Level Storage'.format(tenant['name']),'description':'This is at the root level','parent':'','organization':tenant['id']}
            response = s2.post('{}storage/'.format(baseuri),data=data)
            _check_response(response,'Storage',data)
            parent_id = json.loads(response.text)['id']
            storage_array = [{'name':'{} Sublevel Storage 1'.format(tenant['name']),'description':'This is storage at a sublevel','parent':parent_id,'organization':tenant['id']},{'name':'{} Sublevel Storage 2'.format(tenant['name']),'description':'This is storage at a sublevel','parent':parent_id,'organization':tenant['id']},{'name':'{} Sublevel Storage 3'.format(tenant['name']),'description':'This is storage at a sublevel','parent':parent_id,'organization':tenant['id']},{'name':'{} Sublevel Storage 4'.format(tenant['name']),'description':'This is storage at a sublevel','parent':parent_id,'organization':tenant['id']}]
            for data in storage_array:
                response = s2.post('{}storage/'.format(baseuri),data=data)
                _check_response(response,'Storage',data)

    print("Creating Parts, Assemblies, AlternateSKUs, and PartAlternateSKUs for Zephyrus Computing...")
    token2 = _get_session_token(zc_tenant_id)
    s2.headers['Authorization'] = 'Token {}'.format(token2.key)

    data = {'name':'AR15 Lower Assembly','description':'Lower Assembly of AR 15','sku':'ar15lower1','price':20.0,'cost':10.0,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15lower1_id = json.loads(response.text)['id']

    part_array = [{'name':'Trigger Guard','description':'Guards the trigger from accidental strikes','sku':'tg101','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Disconnector','description':'disconnects','sku':'discon021','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Trigger','description':'Makes it go pew pew','sku':'trig100','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Bolt Catch','description':'Catches the bolt after the last round','sku':'bcatch15','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Magazine Catch','description':'Keeps the magazine inserted in the magazine well','sku':'mcatch12','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Magazine Catch Button','description':'For releasing the insterted magazine','sku':'butt-mcatch11','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Hammer','description':'Strikes the firing pin to discharge a round','sku':'117hammer','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Selector','description':'Selects the rate of fire','sku':'selec66','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Trigger-Hammer','description':'Pin for containing the Trigger and Hammer in place','sku':'pin9919','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Detent, Takedown-Pivot','description':'Spring for Detent','sku':'spring100242','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Selector','description':'Spring for Selector','sku':'spring100223','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Bolt Catch','description':'Spring for Bolt Catch','sku':'spring100195','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Buffer Retainer','description':'Spring for Buffer Retainer','sku':'spring100311','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Disconnector','description':'Spring for Disconnector','sku':'spring100594','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Magazine Catch','description':'Spring for Magazine Catch','sku':'spring100968','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Trigger','description':'Spring for Trigger','sku':'spring100105','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Washer, Lock, Pistol Grip','description':'Lock Washer for Pistol Grip','sku':'lwash73','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pistol Grip','description':'Pistol Grip','sku':'pgrip3','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Takedown','description':'Takedown Pin','sku':'pin9828','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Detent, Takedown-Pivot','description':'Pin for Detent','sku':'pin9984','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Detent, Selector','description':'Detent for Selector','sku':'det43','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Bolt Catch Buffer','description':'Buffers the Bolt Catch','sku':'bcbuff03','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Buffer Retainer','description':'Retains the Buffer','sku':'buffret12','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Roll, Trigger Guard','description':'Roll Pin for Trigger Guard','sku':'pin9891','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Roll, Bolt Catch','description':'Roll Pin for Bolt Catch','sku':'pin9846','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Hammer','description':'Spring for Hammer','sku':'spring100742','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pistol Grip Screw','description':'Screw for Pistol Grib','sku':'scrpisgrip9','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Pivot','description':'Pivot Pin','sku':'pin9867','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data2 = {'parent':ar15lower1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data2)
        _check_response(response,'Assembly',data2)

    data = {'name':'AR15 Buttstock Assembly','description':'Buttstock Assembly of AR 15','sku':'ar15buttstock1','price':12.0,'cost':6,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15buttstock1_id = json.loads(response.text)['id']

    part_array = [{'name':'Receiver Extension','description':'Extends the Receiver','sku':'rext6542','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Buttcap Spacer','description':'Spaces the Buttcap','sku':'butt35','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Buffer Assembly','description':'Assembled buffers','sku':'ba9','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Action','description':'Spring of Action','sku':'spring100056','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Buttstock','description':'Stock of Butts','sku':'butt7','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Buttplate','description':'Plate of Butts','sku':'butt9','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Screw, Buttplate','description':'Screw for Plat of Butts','sku':'screw937','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Screw, Rear Swivel','description':'Screw for Swiveling ones Rear','sku':'screw614','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Hinge, Access Door','description':'Hinge for Access Door','sku':'hinge99','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Hinge, Access Door','description':'Pin for Hinge for Access Door','sku':'pin5862','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Access Door','description':'Door for Accessing the inside of Butts','sku':'ad321','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Rear Sling Swivel','description':'Swiveling the Rear Sling','sku':'rss308','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data2 = {'parent':ar15buttstock1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data2)
        _check_response(response,'Assembly',data2)

    data = {'name':'AR15 Charging Handle Assembly','description':'Charging Handle Assembly of AR 15','sku':'ar15chargehandle1','price':1.0,'cost':0.5,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15chargehandle1_id = json.loads(response.text)['id']

    part_array = [{'name':'Pin, Roll, Charging Handle Latch','description':'Roll Pin for Charging Handle Latch','sku':'pin9715','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Charging Handle Latch','description':'Latch to the Charging Handle','sku':'chl831','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Charging Handle Latch','description':'Spring for Charging Handle Latch','sku':'spring100842','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Charging Handle','description':'Handles the charge','sku':'ch17','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data = {'parent':ar15chargehandle1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data)
        _check_response(response,'Assembly',data)

    data = {'name':'AR15 Bolt Carrier Assembly','description':'Bolt Carrier Assembly of AR 15','sku':'ar15boltcarrier1','price':6.0,'cost':3.0,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15boltcarrier1_id = json.loads(response.text)['id']

    part_array = [{'name':'Pin, Firing','description':'Pin of fire','sku':'pin0010','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Screw, Bolt Carrier Key','description':'Screw for Bolt Carrier Key','sku':'screw124','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Bolt Carrier Key','description':'Carrier of Bolt Keys','sku':'bckar15','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Retaining Pin, Firing','description':'Pin of Retaining of Fire','sku':'pin0021','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Cam','description':'Pin of Cam','sku':'cam0424','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Bolt Carrier','description':'Carrier of Bolts; Possibly for Zues','sku':'bcar15','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    altsku_array = [{'manufacturer':'Bushmaster','sku':'8448503','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448508','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448506','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448504','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448502','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448507','organization':zc_tenant_id}]
    i = 0
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data = {'parent':ar15boltcarrier1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data)
        _check_response(response,'Assembly',data)
        response = s2.post('{}altsku/'.format(baseuri),data=altsku_array[i])
        _check_response(response, altsku_array[i], 'AlternateSKU')
        altsku_id = json.loads(response.text)['id']
        data = {'alt_sku':altsku_id,'part':part_id,'organization':zc_tenant_id}
        response = s2.post('{}partaltsku/'.format(baseuri),data=data)
        _check_response(response,'PartAlternateSKU',data)
        i+=1

    data = {'name':'AR15 Bolt Assembly','description':'Bolt Assembly of AR 15','sku':'ar15bolt1','price':1.0,'cost':0.5,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15bolt1_id = json.loads(response.text)['id']

    part_array = [{'name':'Ring, Bolt Gas','description':'Bolt Gas Ring','sku':'ring468','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Bolt','description':'of Lightning','sku':'bolt43','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Extractor','description':'Extracts rounds from the upper receiver','sku':'ext32','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Extractor','description':'Spring of Extraction +3','sku':'spring100819','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Extractor Spring Insert','description':'Spring Insert for Extractor','sku':'instext99','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Extractor','description':'Pin of Extractor','sku':'pin9746','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Roll, Ejector','description':'Roll Pin for Ejector','sku':'pin9861','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Ejector','description':'Spring for Ejector','sku':'spring100167','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Ejector','description':'Ejects things','sku':'eject47','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    altsku_array = [{'manufacturer':'Bushmaster','sku':'8448511K','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448510','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448512','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448755SP','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448754','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448513','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'MS16562-98','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448516','organization':zc_tenant_id},{'manufacturer':'Bushmaster','sku':'8448515','organization':zc_tenant_id}]
    i = 0
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data = {'parent':ar15bolt1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data)
        _check_response(response,'Assembly',data)
        response = s2.post('{}altsku/'.format(baseuri),data=altsku_array[i])
        _check_response(response, altsku_array[i], 'AlternateSKU')
        altsku_id = json.loads(response.text)['id']
        data = {'alt_sku':altsku_id,'part':part_id,'organization':zc_tenant_id}
        response = s2.post('{}partaltsku/'.format(baseuri),data=data)
        _check_response(response,'PartAlternateSKU',data)
        i+=1

    data = {'name':'AR15 Forward Assist Assembly','description':'Forward Assist Assembly of AR 15','sku':'ar15forwardassist1','price':7.0,'cost':3.5,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15forwardassist1_id = json.loads(response.text)['id']

    part_array = [{'name':'Forward Assist Plunger','description':'Plunger of Forward Assistance','sku':'fap99','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Detent, Pawl','description':'Detent for Pawl','sku':'dentpawl9','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Pawl','description':'Spring for Pawl','sku':'spring100815','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Forward Assist Pawl','description':'Assist with the forwarding','sku':'fap44','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Spring, Pawl','description':'Pin for Spring for Pawl','sku':'pin6342','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Forward Assist','description':'Spring for Forward Assistance','sku':'spring100846','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Spring, Forward Assist','description':'Pin for Spring for Forward Assistance','sku':'pin4657','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data = {'parent':ar15forwardassist1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data)
        _check_response(response,'Assembly',data)

    data = {'name':'AR15 Upper Receiver Assembly','description':'Upper Receiver Assembly OF AR 15','sku':'ar15upper1','price':1.0,'cost':0.5,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15upper1_id = json.loads(response.text)['id']

    part_array = [{'name':'Upper Receiver','description':'Upper housing the AR15','sku':'upper75','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Ejection Port Cover','description':'Covers the Ejection Port','sku':'eject23','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Ejection Port Cover','description':'Spring for Ejection Port Cover','sku':'spring100053','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Ejection Port Cover','description':'Pin for Ejection Port Cover','sku':'pin1867','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Ring, Retaining Cover','description':'Ring of the Cover Retaining','sku':'ring756','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data = {'parent':ar15upper1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data)
        _check_response(response,'Assembly',data)

    data = {'name':'AR15 Rear Sight Assembly','description':'Rear Sight Assembly of AR 15','sku':'ar15rearsight1','price':8.0,'cost':4.0,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15rearsight1_id = json.loads(response.text)['id']

    part_array = [{'name':'Rear Sight Base','description':'Base of the rear sights','sku':'rsb131','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Screw, Rear Sight Windage','description':'Screw for Rear Sight Windage','sku':'screw519','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Knob, Rear Sight Windage','description':'Ajusting knob for Rear Sight Windage','sku':'knob22','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Knob, Rear Sight Windage','description':'Pin for Knob for Rear Sight Windage','sku':'pin3162','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Rear Sight Helical','description':'Spring for Rear Sight Helical','sku':'spring100516','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Rear Sight Ball Bearing','description':'Ball Bearing for the Rear Sight','sku':'bb7','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Rear Sight Aperture','description':'Aperture of the Rear Sight','sku':'rsa97','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Rear Sight Flat','description':'Flat Spring for Rear Sight','sku':'spring100674','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data = {'parent':ar15rearsight1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data)
        _check_response(response,'Assembly',data)

    data = {'name':'AR15 Carry Handle Assembly','description':'Carry Handle Assembly of AR 15','sku':'ar15carryhandle1','price':10.0,'cost':5.0,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15carryhandle1_id = json.loads(response.text)['id']

    part_array = [{'name':'Carry Handle - Forged','description':'The specially forged handle of carrying +1','sku':'ch661','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Clamping Bar','description':'Bar that clamps to the upper receiver','sku':'cb108','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Bolt, Cross','description':'Bolt for Cross','sku':'bolt67','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Thumb Nut Assembly','description':'Assembles the thumb nut','sku':'tna765','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Elevation','description':'Spring for Elevation','sku':'spring100629','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Spring, Rear Sight Elevation','description':'Pin for Spring for Rear Sight Elevation','sku':'pin2161','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Index','description':'Spring for Index','sku':'spring100492','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Knob, Elevation','description':'Knob for Elevation','sku':'knob36','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Index, Elevation','description':'Index for Elevation','sku':'index8','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Screwn, Index','description':'Screw for Index','sku':'screw483','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data = {'parent':ar15carryhandle1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data)
        _check_response(response,'Assembly',data)

    data = {'name':'AR15 Barrel Assembly','description':'Barrel Assembly of AR 15','sku':'ar15barrel1','price':20.0,'cost':10.0,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15barrel1_id = json.loads(response.text)['id']

    part_array = [{'name':'Ring, Hand Guard Snap','description':'Ring for Hand Guard Snap','sku':'ring017','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Weld','description':'Spring for Weld','sku':'spring100082','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Ring, Delta','description':'Ring for Delta','sku':'ring153','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Barrel Index','description':'Pin for Barrel Index','sku':'pin1438','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Barrel Extension','description':'Extension of the Barrel','sku':'bext853','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Nut, Barrel','description':'Barrel\'s Nut','sku':'nut911','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Thermoset Handguard','description':'Thermo safty handgaurds','sku':'handy88','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Carbine Gas Tube','description':'Tube for Carbine Gas escapage','sku':'cgt111','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Handguard Cap','description':'Caps the handgaurds','sku':'handy56','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Front Sight Post','description':'The pointy aiming thingy','sku':'fsp919','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Detent, Front Sight','description':'Detent for Front Sight','sku':'dentfs12','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Spring, Detent, Front Sight','description':'Spring for Detent for Front Sight','sku':'spring100739','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Roll, Gas Tube','description':'Roll Pin for Gas Tub','sku':'pin0385','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Front Sight Base With Bayonet Lug','description':'Attach point for bayonet and Front Sight Assembly','sku':'fsbwbl3','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Barrel Subassembly','description':'The barrel goes brrrr','sku':'pewpew69','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Pin, Front Sight Taper','description':'Pin for Front Sight Taper','sku':'pin0914','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Front Sling Swivel','description':'Front Sling attachment swivel','sku':'fss100','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Rivet, Front Sling Swivel','description':'Rivet for Front Sling Swivel','sku':'rivet002','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Washer, Crush','description':'Washer of Crushing +8','sku':'cwash8','price':1.0,'cost':0.5,'organization':zc_tenant_id},{'name':'Izzy Flass Suppressor','description':'Suppresses Flashes','sku':'ifs3276','price':1.0,'cost':0.5,'organization':zc_tenant_id}]
    for data in part_array:
        response = s2.post('{}part/'.format(baseuri),data=data)
        _check_response(response,'Part',data)
        part_id = json.loads(response.text)['id']
        data = {'parent':ar15barrel1_id,'part':part_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data)
        _check_response(response,'Assembly',data)

    data = {'name':'AR15','description':'Completely assembled AR15 rifle','sku':'ar15-1','price':120.0,'cost':60.0,'organization':zc_tenant_id}
    response = s2.post('{}part/'.format(baseuri),data=data)
    _check_response(response,'Part',data)
    ar15_id = json.loads(response.text)['id']

    for assembly_id in [ar15barrel1_id,ar15carryhandle1_id,ar15rearsight1_id,ar15upper1_id,ar15forwardassist1_id,ar15bolt1_id,ar15boltcarrier1_id,ar15chargehandle1_id,ar15buttstock1_id,ar15lower1_id]:
        data = {'parent':ar15_id,'part':assembly_id,'count':1,'organization':zc_tenant_id}
        response = s2.post('{}assembly/'.format(baseuri),data=data)
        _check_response(response,'Assembly',data)


    print("Creating Parts and Assemblies for other tenants")
    part_id = parent_id = 0
    for tenant in tenants:
        token2 = _get_session_token(tenant['id'])
        s2.headers['Authorization'] = 'Token {}'.format(token2.key)
        if tenant['name'] != 'Zephyrus Computing':
            for i in range(0,randint(10,20)):
                data = {'name':'{}-{}'.format(tenant['name'], i),'description':'{} Part number {}'.format(tenant['name'], i),'sku':'{}'.format(str(i).zfill(5)),'price':1.0,'cost':0.5,'organization':tenant['id']}
                response = s2.post('{}part/'.format(baseuri),data=data)
                _check_response(response,'Part',data)
                if i == 0:
                    parent_id = json.loads(response.text)['id']
                else:
                    part_id = json.loads(response.text)['id']
                if parent_id != 0 and i > 0:
                    data = {'parent':parent_id,'part':part_id,'count':1,'organization':tenant['id']}
                    response = s2.post('{}assembly/'.format(baseuri),data=data)
                    _check_response(response,'Assembly',data)

    print("Creating PartStorages for Zephyrus Computing...")
    token2 = _get_session_token(zc_tenant_id)
    s2.headers['Authorization'] = 'Token {}'.format(token2.key)
    response = s2.get('{}assembly/'.format(baseuri))
    _check_response(response,'Assembly')
    assemblies = json.loads(response.text)
    response = s2.get('{}storage/?search=Tote'.format(baseuri))
    _check_response(response,'Storage')
    storages = json.loads(response.text)
    response = s2.get('{}storage/?search=Warehouse'.format(baseuri))
    _check_response(response,'Storage')
    warehouse = json.loads(response.text)[0]
    response = s2.get('{}part/'.format(baseuri))
    _check_response(response,'Part')
    parts = json.loads(response.text)

    for part in parts:
        i = randint(0,len(storages) -1)
        data = {'part':part['id'],'storage':storages[i]['id'],'count':10,'organization':zc_tenant_id}
        response = s2.post('{}partstorage/'.format(baseuri),data=data)
        _check_response(response,'PartStorage', data)

    for assembly in assemblies:
        data = {'part':assembly['parent']['id'],'storage':warehouse['id'],'count':4,'organization':zc_tenant_id}
        response = s2.post('{}partstorage/'.format(baseuri),data=data)
        _check_response(response,'PartStorage', data)


    print("Creating PartStorage for other tenants...")
    for tenant in tenants:
        token2 = _get_session_token(tenant['id'])
        s2.headers['Authorization'] = 'Token {}'.format(token2.key)
        if tenant['name'] != 'Zephyrus Computing':
            response = s2.get('{}assembly/'.format(baseuri))
            _check_response(response,'Assembly')
            assemblies = json.loads(response.text)
            response = s2.get('{}storage/?search=Sublevel'.format(baseuri))
            _check_response(response,'Storage')
            storages = json.loads(response.text)
            response = s2.get('{}storage/?search=Root'.format(baseuri))
            _check_response(response,'Storage')
            warehouse = json.loads(response.text)[0]
            response = s2.get('{}part/'.format(baseuri))
            _check_response(response,'Part')
            parts = json.loads(response.text)

            for part in parts:
                i = randint(0,len(storages) -1)
                data = {'part':part['id'],'storage':storages[i]['id'],'count':10,'organization':tenant['id']}
                response = s2.post('{}partstorage/'.format(baseuri),data=data)
                _check_response(response,'PartStorage', data)
            
            for assembly in assemblies:
                data = {'part':assembly['parent']['id'],'storage':warehouse['id'],'count':4,'organization':tenant['id']}
                response = s2.post('{}partstorage/'.format(baseuri),data=data)
                _check_response(response,'PartStorage', data)

    print("Creating Kits for Zephyrus Computing...")
    token2 = _get_session_token(zc_tenant_id)
    s2.headers['Authorization'] = 'Token {}'.format(token2.key)

    data = {'name':'AR 15 Upper Receiver Kit','description':'Includes all of the assemblies of an Upper Receiver for an AR 15','sku':'kit5643','price':50.0,'organization':zc_tenant_id}
    response = s2.post('{}kit/'.format(baseuri),data=data)
    _check_response(response,'Kit',data)
    ar15upkit_id = json.loads(response.text)['id']
    for assembly_id in [ar15chargehandle1_id,ar15boltcarrier1_id,ar15bolt1_id,ar15forwardassist1_id,ar15upper1_id]:
        response = s2.get('{}part/{}/'.format(baseuri,assembly_id))
        _check_response(response,'Part')
        part = json.loads(response.text)
        response = s2.get('{}assembly/?search={}'.format(baseuri, part['name']))
        _check_response(response,'Assembly')
        assemblies = json.loads(response.text)
        for assembly in assemblies:
            if assembly['parent']['name'] == part['name']:
                response = s2.get('{}partstorage/?search={}'.format(baseuri, assembly['part']['name']))
                _check_response(response,'PartStorage')
                partstorage = json.loads(response.text)[0]
                data = {'kit':ar15upkit_id,'partstorage':partstorage['id'],'count':1,'organization':zc_tenant_id}
                response = s2.post('{}kitpartstorage/'.format(baseuri),data=data)
                _check_response(response,'KitPartStorage',data)

    data = {'name':'Barrel Kit','description':'Completed assemblies of a new barrel','sku':'kit1834','price':20.0,'organization':zc_tenant_id}
    response = s2.post('{}kit/'.format(baseuri),data=data)
    _check_response(response,'Kit',data)
    ar15barkit_id = json.loads(response.text)['id']
    response = s2.get('{}part/{}/'.format(baseuri,ar15barrel1_id))
    _check_response(response,'Part')
    part = json.loads(response.text)
    response = s2.get('{}assembly/?search={}'.format(baseuri, part['name']))
    _check_response(response,'Assembly')
    assemblies = json.loads(response.text)
    for assembly in assemblies:
        if assembly['parent']['name'] == part['name']:
            response = s2.get('{}partstorage/?search={}'.format(baseuri, assembly['part']['name']))
            _check_response(response,'PartStorage')
            partstorage = json.loads(response.text)[0]
            data = {'kit':ar15barkit_id,'partstorage':partstorage['id'],'count':1,'organization':zc_tenant_id}
            response = s2.post('{}kitpartstorage/'.format(baseuri),data=data)
            _check_response(response,'KitPartStorage',data)

    data = {'name':'Carry Handle and Sights Kit','description':'Completed assemblies of a new Carrying Handle and Rear Sight','sku':'kit2671','price':20.0,'organization':zc_tenant_id}
    response = s2.post('{}kit/'.format(baseuri),data=data)
    _check_response(response,'Kit',data)
    ar15carkit_id = json.loads(response.text)['id']
    for assembly_id in [ar15rearsight1_id,ar15carryhandle1_id]:
        response = s2.get('{}part/{}/'.format(baseuri,assembly_id))
        _check_response(response,'Part')
        part = json.loads(response.text)
        response = s2.get('{}assembly/?search={}'.format(baseuri, part['name']))
        _check_response(response,'Assembly')
        assemblies = json.loads(response.text)
        for assembly in assemblies:
            if assembly['parent']['name'] == part['name']:
                response = s2.get('{}partstorage/?search={}'.format(baseuri, assembly['part']['name']))
                _check_response(response,'PartStorage')
                partstorage = json.loads(response.text)[0]
                data = {'kit':ar15carkit_id,'partstorage':partstorage['id'],'count':1,'organization':zc_tenant_id}
                response = s2.post('{}kitpartstorage/'.format(baseuri),data=data)
                _check_response(response,'KitPartStorage',data)

    data = {'name':'AR 15 Lower Receiver Kit','description':'Includes all of the assemblies of an Lower Receiver for an AR 15','sku':'kit4823','price':50.0,'organization':zc_tenant_id}
    response = s2.post('{}kit/'.format(baseuri),data=data)
    _check_response(response,'Kit',data)
    ar15lowkit_id = json.loads(response.text)['id']
    for assembly_id in [ar15buttstock1_id,ar15lower1_id]:
        response = s2.get('{}part/{}/'.format(baseuri,assembly_id))
        _check_response(response,'Part')
        part = json.loads(response.text)
        response = s2.get('{}assembly/?search={}'.format(baseuri, part['name']))
        _check_response(response,'Assembly')
        assemblies = json.loads(response.text)
        for assembly in assemblies:
            if assembly['parent']['name'] == part['name']:
                response = s2.get('{}partstorage/?search={}'.format(baseuri, assembly['part']['name']))
                _check_response(response,'PartStorage')
                partstorage = json.loads(response.text)[0]
                data = {'kit':ar15lowkit_id,'partstorage':partstorage['id'],'count':1,'organization':zc_tenant_id}
                response = s2.post('{}kitpartstorage/'.format(baseuri),data=data)
                _check_response(response,'KitPartStorage',data)


    print("Creating Kits for other tenants...")
    for tenant in tenants:
        token2 = _get_session_token(tenant['id'])
        s2.headers['Authorization'] = 'Token {}'.format(token2.key)
        if tenant['name'] != 'Zephyrus Computing':
            response = s2.get('{}assembly/'.format(baseuri))
            _check_response(response,'Assembly')
            parents = json.loads(response.text)
            parent_ids = []
            for parent in parents:
                if not parent['parent']['id'] in parent_ids:
                    parent_ids.append(parent['parent']['id'])
            for parent_id in parent_ids:
                data = {'name':'{} Kit {}'.format(tenant['name'], parent_id),'description':'Randomly generated {} kit'.format(tenant['name']),'sku':'{}{}'.format(tenant['id'],parent_id),'price':50.0,'organization':tenant['id']}
                response = s2.post('{}kit/'.format(baseuri),data=data)
                _check_response(response,'Kit',data)
                kit_id = json.loads(response.text)['id']
                response = s2.get('{}part/{}/'.format(baseuri,parent_id))
                _check_response(response,'Part')
                part = json.loads(response.text)
                response = s2.get('{}assembly/?search={}'.format(baseuri, part['name']))
                _check_response(response,'Assembly')
                assemblies = json.loads(response.text)
                for assembly in assemblies:
                    if assembly['parent']['name'] == part['name']:
                        response = s2.get('{}partstorage/?search={}'.format(baseuri, assembly['part']['name']))
                        _check_response(response,'PartStorage')
                        partstorage = json.loads(response.text)[0]
                        data = {'kit':kit_id,'partstorage':partstorage['id'],'count':1,'organization':tenant['id']}
                        response = s2.post('{}kitpartstorage/'.format(baseuri),data=data)
                        _check_response(response,'KitPartStorage',data)

    print("Moving inventory around...")
    for tenant in tenants:
        token2 = _get_session_token(tenant['id'])
        s2.headers['Authorization'] = 'Token {}'.format(token2.key)
        response = s2.get('{}partstorage/'.format(baseuri))
        _check_response(response,'PartStorage')
        partstorages = json.loads(response.text)
        for i in range(0,randint(1,20)):
            j = randint(0,len(partstorages) -1)
            k = randint(0,1)
            partstorage = partstorages[j]
            count = partstorage['count']
            if k == 0:
                count = count + 1
            else:
                count = count - 1
            response = s2.patch('{}partstorage/{}/'.format(baseuri,partstorage['id']),data={'count':count})
            _check_response(response,'PartStorage')

    print("Shipping products to customers...")
    for tenant in tenants:
        token2 = _get_session_token(tenant['id'])
        s2.headers['Authorization'] = 'Token {}'.format(token2.key)
        response = s2.get('{}partstorage/'.format(baseuri))
        _check_response(response,'PartStorage')
        partstorages = json.loads(response.text)
        for i in range(0,randint(1,20)):
            j = randint(0,len(partstorages) -1)
            partstorage = partstorages[j]
            count = randint(1,partstorage['count'] -1)
            response = s2.get('{}subpart/{}/{}/{}/'.format(baseuri,partstorage['storage']['id'],partstorage['part']['id'],count))
            _check_response(response,'SubPart')

    print("Receiving new shipments...")
    for tenant in tenants:
        token2 = _get_session_token(tenant['id'])
        s2.headers['Authorization'] = 'Token {}'.format(token2.key)
        response = s2.get('{}partstorage/'.format(baseuri))
        _check_response(response,'PartStorage')
        partstorages = json.loads(response.text)
        for i in range(0,randint(1,20)):
            j = randint(0,len(partstorages) -1)
            partstorage = partstorages[j]
            count = randint(1,100)
            response = s2.get('{}addpart/{}/{}/{}/'.format(baseuri,partstorage['storage']['id'],partstorage['part']['id'],count))
            _check_response(response,'AddPart')
except Exception as err:
    print(err)
    print("Cleaning up...")
    # Clean up
    response = s.get('{}partaltsku/'.format(baseuri))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}partaltsku/{}/'.format(baseuri, obj['id']))
    response = s.get('{}altsku/'.format(baseuri))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}altsku/{}/'.format(baseuri, obj['id']))
    response = s.get('{}kitpartstorage/'.format(baseuri))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}kitpartstorage/{}/'.format(baseuri, obj['id']))
    response = s.get('{}kit/'.format(baseuri))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}kit/{}/'.format(baseuri, obj['id']))
    response = s.get('{}partstorage/'.format(baseuri))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}partstorage/{}/'.format(baseuri, obj['id']))
    response = s.get('{}storage/'.format(baseuri))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}storage/{}/'.format(baseuri, obj['id']))
    response = s.get('{}assembly/'.format(baseuri))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}assembly/{}/'.format(baseuri, obj['id']))
    response = s.get('{}part/'.format(baseuri))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}part/{}/'.format(baseuri, obj['id']))
    response = s.get('{}userorganization/'.format(baseuri_tenant))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}userorganization/{}/'.format(baseuri_tenant, obj['id']))
    response = s.get('{}organization/'.format(baseuri_tenant))
    objs = json.loads(response.text)
    for obj in objs:
        s.delete('{}organization/{}/'.format(baseuri_tenant, obj['id']))

    users = User.objects.all()
    ci_user.delete()
    for user in users:
        if "Tenant" in user.first_name:
            user.delete()
    group = Group.objects.get_or_create(name='inventory_admin')[0]
    group.delete()

print("CI testing complete")

