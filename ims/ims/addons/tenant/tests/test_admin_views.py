import django
django.setup()
from django.contrib.auth.models import AnonymousUser, User, Permission
from django.test import TestCase, override_settings
from random import randint
from rest_framework import status
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from ims.addons.tenant.models import Organization, UserOrganization, TenantPart, TenantStorage, TenantAlternateSKU, TenantPartAlternateSKU, TenantPartStorage, TenantAssembly, TenantKit, TenantKitPartStorage
import ims.addons.tenant.helpers as helpers


baseurl = '/inventory/admin/'

# Test module root view
class InventoryRootViewTests(TestCase):
    def test_view_root(self):
        response = self.client.get(baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(baseurl))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tenant/admin/home.html')

class TenantRootViewTests(TestCase):
    def setUp(self):    
        self.baseurl = '/tenant/admin/'

    def test_view_home(self):
        response = self.client.get(self.baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/accounts/login/?next={}'.format(self.baseurl))
        user = User.objects.get_or_create(username='teststaff',is_staff=True)[0]
        permission = Permission.objects.get(codename='view_organization')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tenant/admin/tenant_home.html')

# Test PartThreshold views
class InventoryPartAdminViewTests(TestCase):
    def setUp(self):
        for i in range(100,105):
            if i != 100:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(2001,2010):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part,organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = baseurl + 'part/'

    def test_view_part_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_part')
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/view.html')


    def test_view_part_create(self):
        # Test the create view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='add_part')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/create.html')
        data = {'name': 'Test_View_Part_New', 'description': 'New Test View Part', 'sku': 'new', 'price': 9.99, 'cost': 4.99, 'organization': org.id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tenant/admin/create.html')
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        data = {'name': 'Test_View_Part_New2', 'description': 'New Test View Part', 'sku': 'new2', 'price': 9.99, 'cost': 4.99, 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_part_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_part')
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        p = Part.objects.filter(tenant__organization=org).last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, p.id), permission, 'tenant/admin/update.html')
        data = {'name': 'Test_View_PartThreshold_Modified', 'description': 'Modified description', 'sku': 'new', 'price': 5, 'cost': 4}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, p.id), data, permission)
        data = {'name': 'Test_View_PartThreshold_Modified', 'description': 'Does this work too?'}
        response = self.client.post('{}change/{}/'.format(self.baseurl, p.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tenant/admin/update.html')
        p = Part.objects.exclude(tenant__organization__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        data = {'name': 'Test_View_PartThreshold_Modified', 'description': 'Modified description', 'sku': 'new', 'price': 5, 'cost': 4}
        response = self.client.post('{}change/{}/'.format(self.baseurl, p.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_part_delete(self):
        # Test the delete view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='delete_part')
        p = Part.objects.filter(tenant__organization=org).first()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, p.id), permission)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        p = Part.objects.filter(tenant__organization=org).first()
        response = self.client.get('{}delete/{}/'.format(self.baseurl, p.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertContains(response, 'red')

class InventoryPartStorageAdminViewTests(TestCase):
    def setUp(self):
        for i in range(105,110):
            if i != 105:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(2011,2020):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part,organization=org)
                storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
                TenantStorage.objects.create(storage=storage,organization=org)
            for i in range(0,5):
                j = randint(2011,2019)
                k = randint(2011,2019)
                ps = PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j),tenant__organization=org), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k),tenant__organization=org), count=12)
                TenantPartStorage.objects.create(partstorage=ps,organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = baseurl + 'partstorage/'

    def test_view_partstorage_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partstorage')
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/view.html')

    def test_view_partstorage_create(self):
        # Test the create view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='add_partstorage')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/create.html')
        part = Part.objects.filter(tenant__organization=org).first()
        storage = Storage.objects.filter(tenant__organization=org).first()
        self.client.get('{}create/?part={}'.format(self.baseurl, part.id))
        self.client.get('{}create/?storage={}'.format(self.baseurl, storage.id))
        self.client.get('{}create/?part={}&storage={}'.format(self.baseurl, part.id, storage.id))
        data = {'part': part.id, 'storage': storage.id, 'count': '9', 'organization': org.id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        user = User.objects.get(username='testuser')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/admin/partstorage/')
        data = {'count': 10}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tenant/admin/create.html')
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        part = Part.objects.filter(tenant__organization=org).first()
        storage = Storage.objects.filter(tenant__organization=org).first()
        data = {'part': part.id, 'storage': storage.id, 'count': '9', 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_partstorage_change(self):
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='change_partstorage')
        pa = PartStorage.objects.filter(tenant__organization=org).last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, pa.id), permission, 'tenant/admin/update.html')
        data = {'part': pa.part.id, 'storage': pa.storage.id, 'count': 8}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, pa.id), data, permission)
        data = {'count': 'banana'}
        response = self.client.post('{}change/{}/'.format(self.baseurl, pa.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tenant/admin/update.html')
        pa = PartStorage.objects.exclude(tenant__organization__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        response = self.client.post('{}change/{}/'.format(self.baseurl, pa.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        pa = PartStorage.objects.filter(tenant__organization=org).first()
        response = self.client.post('{}change/{}/'.format(self.baseurl, pa.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_partstorage_delete(self):
        # Test the delete view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='delete_partstorage')
        ps = PartStorage.objects.filter(tenant__organization=org).last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, ps.id), permission)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        ps = PartStorage.objects.filter(tenant__organization=org).first()
        response = self.client.get('{}delete/{}/'.format(self.baseurl, ps.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

class InventoryPartAlternateSKUAdminViewTests(TestCase):
    def setUp(self):
        for i in range(110,115):
            if i != 110:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(2021,2030):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part, organization=org)
                altsku = AlternateSKU.objects.create(manufacturer='Zephyrus Computing', sku=str(i))
                TenantAlternateSKU.objects.create(alternatesku=altsku, organization=org)
            for i in range(0,6):
                j = randint(2021,2029)
                k = randint(2021,2029)
                pas = PartAlternateSKU.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j),tenant__organization=org), alt_sku=AlternateSKU.objects.get(sku=str(k),tenant__organization=org))
                TenantPartAlternateSKU.objects.create(partalternatesku=pas, organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = baseurl + 'partalternatesku/'

    def test_view_partalternatesku_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partalternatesku')
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/view.html')

    def test_view_partalternatesku_create(self):
        # Test the create view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='add_partalternatesku')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/create.html')
        part = Part.objects.filter(tenant__organization=org).first()
        altsku = AlternateSKU.objects.filter(tenant__organization=org).first()
        self.client.get('{}create/?part={}'.format(self.baseurl, part.id))
        self.client.get('{}create/?alternatesku={}'.format(self.baseurl, altsku.id))
        self.client.get('{}create/?part={}&alternatesku={}'.format(self.baseurl, part.id, altsku.id))
        data = {'part': part.id, 'alt_sku': altsku.id, 'organization': org.id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/admin/partalternatesku/')
        data = {'part': part.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        part = Part.objects.filter(tenant__organization=org).first()
        data = {'part': part.id, 'alt_sku': altsku.id, 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_partalternatesku_delete(self):
        # Test the delete view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='delete_partalternatesku')
        pas = PartAlternateSKU.objects.filter(tenant__organization=org).last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, pas.id), permission)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        pas = PartAlternateSKU.objects.filter(tenant__organization=org).last()
        response = self.client.get('{}delete/{}/'.format(self.baseurl, pas.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

class InventoryAssemblyAdminViewTests(TestCase):
    def setUp(self):
        for i in range(115,120):
            if i != 115:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(2031,2040):
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part,organization=org)
            for i in range(2041,2050):
                j = randint(2031,2039)
                parent = Part.objects.get(name='Test_View_Part_{}'.format(j),tenant__organization=org)
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part,organization=org)
                assembly = Assembly.objects.create(parent=parent, part=part, count=5)
                TenantAssembly.objects.create(assembly=assembly,organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = baseurl + 'assembly/'

    def test_view_assembly_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_assembly')
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/view.html')

    def test_view_assembly_create(self):
        # Test the create view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='add_assembly')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/create.html')
        part = Part.objects.filter(tenant__organization=org).first()
        self.client.get('{}create/?part={}'.format(self.baseurl, part.id))
        parent = Part.objects.filter(tenant__organization=org).last()
        self.client.get('{}create/?parent={}'.format(self.baseurl, parent.id))
        self.client.get('{}create/?parent={}&part={}'.format(self.baseurl, parent.id, part.id))
        data = {'part': part.id, 'parent': parent.id, 'count': 4, 'organization': org.id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/admin/assembly/')
        data = {'part': part.id, 'parent': parent.id, 'count': 'sending explosion...', 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {'part': part.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        part = Part.objects.filter(tenant__organization=org).first()
        parent = Part.objects.filter(tenant__organization=org).last()
        data = {'part': part.id, 'parent': parent.id, 'count': 4, 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_assembly_change(self):
        # Test the change view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='change_assembly')
        a = Assembly.objects.filter(tenant__organization=org).last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, a.id), permission, 'tenant/admin/update.html')
        data = {'part': a.part.id, 'parent': a.parent.id, 'count': 8}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, a.id), data, permission)
        data = {'name': 'banana'}
        response = self.client.post('{}change/{}/'.format(self.baseurl, a.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        a = Assembly.objects.exclude(tenant__organization__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        data = {'part': a.part.id, 'parent': a.parent.id, 'count': 8}
        response = self.client.post('{}change/{}/'.format(self.baseurl, a.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_assembly_delete(self):
        # Test the delete view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='delete_assembly')
        a = Assembly.objects.filter(tenant__organization=org).last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, a.id), permission)
        a = Assembly.objects.exclude(tenant__organization__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        response = self.client.get('{}delete/{}/'.format(self.baseurl,a.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

class InventoryStorageAdminViewTests(TestCase):
    def setUp(self):
        for i in range(120,125):
            if i != 120:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(2051,2060):
                storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a TestCase test storage')
                TenantStorage.objects.create(storage=storage,organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = baseurl + 'storage/'

    def test_view_storage_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_storage')
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/view.html')

    def test_view_storage_create(self):
        # Test the create view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='add_storage')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/create.html')
        data = {'name': 'Test_View_Storage_New', 'description': 'New Storage', 'organization': org.id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/admin/storage/')
        data = {'name': 'banana'}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        data = {'name': 'Test_View_Storage_New', 'description': 'New Storage', 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_storage_change(self):
        # Test the change view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='change_storage')
        s = Storage.objects.filter(tenant__organization=org).last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, s.id), permission, 'tenant/admin/update.html')
        data = {'name': s.name, 'description': 'A new description'}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, s.id), data, permission)
        response = self.client.post('{}change/{}/'.format(self.baseurl, s.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {'count': 8}
        response = self.client.post('{}change/{}/'.format(self.baseurl, s.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        s = Storage.objects.filter(tenant__organization=org).last()
        response = self.client.post('{}change/{}/'.format(self.baseurl, s.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_storage_delete(self):
        # Test the delete view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='delete_storage')
        s = Storage.objects.filter(tenant__organization=org).last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, s.id), permission)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        s = Storage.objects.filter(tenant__organization=org).last()
        response = self.client.get('{}delete/{}/'.format(self.baseurl, s.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

class InventoryAlternateSKUAdminViewTests(TestCase):
    def setUp(self):
        for i in range(125,130):
            if i != 125:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(2061,2070):
                altsku = AlternateSKU.objects.create(manufacturer='Zephyrus Computing',sku=str(i))
                TenantAlternateSKU.objects.create(alternatesku=altsku,organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = baseurl + 'alternatesku/'

    def test_view_alternatesku_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_alternatesku')
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/view.html')

    def test_view_alternatesku_create(self):
        # Test the create view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='add_alternatesku')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/create.html')
        data = {'manufacturer': 'Zephyrus Computing', 'sku': 'New', 'organization': org.id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/admin/alternatesku/')
        data = {'name': 'banana'}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        data = {'manufacturer': 'Zephyrus Computing', 'sku': 'New', 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_alternatesku_delete(self):
        # Test the delete view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='delete_alternatesku')
        a = AlternateSKU.objects.filter(tenant__organization=org).last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, a.id), permission)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        a = AlternateSKU.objects.filter(tenant__organization=org).last()
        response = self.client.get('{}delete/{}/'.format(self.baseurl, a.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

class InventoryKitAdminViewTests(TestCase):
    def setUp(self):
        for i in range(130,135):
            if i != 130:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(2071,2080):
                kit = Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a TestCase test kit', sku=str(i), price=10.00)
                TenantKit.objects.create(kit=kit,organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = '{}kit/'.format(baseurl)

    def test_view_kit_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_kit')
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/view.html')
        
    def test_view_kit_create(self):
        # Test the create view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='add_kit')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/create.html')
        data = {'name': 'Test_View_Kit_New', 'description': 'New Test View Kit', 'sku': 'new', 'price': 9.99, 'organization': org.id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/admin/kit/')
        data = {'name': 'banana'}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        data = {'name': 'Test_View_Kit_New2', 'description': 'New Test View Kit', 'sku': 'new2', 'price': 9.99, 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        
    def test_view_kit_change(self):
        # Test the change view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='change_kit')
        k = Kit.objects.filter(tenant__organization=org).last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, k.id), permission, 'tenant/admin/update.html')
        data = {'name': 'Test_View_Kit_Modified', 'description': 'Modified description', 'sku': 'new', 'price': 5}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, k.id), data, permission)
        response = self.client.post('{}change/{}/'.format(self.baseurl, k.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {'name': 'Test_View_Kit_Modified', 'description': 'Does this work too?'}
        response = self.client.post('{}change/{}/'.format(self.baseurl, k.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        k = Kit.objects.filter(tenant__organization=org).last()
        response = self.client.post('{}change/{}/'.format(self.baseurl, k.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        
    def test_view_kit_delete(self):
        # Test the delete view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='delete_kit')
        k = Kit.objects.filter(tenant__organization=org).last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, k.id), permission)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        k = Kit.objects.filter(tenant__organization=org).last()
        response = self.client.get('{}delete/{}/'.format(self.baseurl, k.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

class InventoryKitPartStorageAdminViewTests(TestCase):
    def setUp(self):
        for i in range(135,140):
            if i != 135:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(2081,2090):
                kit = Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a TestCase test kit', sku=str(i), price=10.00)
                TenantKit.objects.create(kit=kit,organization=org)
                part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a TestCase test part', sku=str(i), price=10.00, cost=5.00)
                TenantPart.objects.create(part=part,organization=org)
                storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a TestCase test storage')
                TenantStorage.objects.create(storage=storage,organization=org)
                ps = PartStorage.objects.create(part=part,storage=storage,count=5)
                TenantPartStorage.objects.create(partstorage=ps,organization=org)
                kps = KitPartStorage.objects.create(kit=kit,partstorage=ps,count=2)
                TenantKitPartStorage.objects.create(kitpartstorage=kps,organization=org)
        user = User.objects.create(username='testuser')
        for i in range(0,randint(1,3)):
            UserOrganization.objects.create(user=user,organization=Organization.objects.all()[i])
        self.baseurl = '{}kitpartstorage/'.format(baseurl)
        
    def test_view_kitpartstorage_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_kitpartstorage')
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/view.html')

    def test_view_kitpartstorage_create(self):
        # Test the create view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='add_kitpartstorage')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/create.html')
        kit = Kit.objects.filter(tenant__organization=org).first()
        partstorage = PartStorage.objects.filter(tenant__organization=org).last()
        self.client.get('{}create/?kit={}'.format(self.baseurl, kit.id))
        self.client.get('{}create/?partstorage={}'.format(self.baseurl, partstorage.id))
        data = {'kit': kit.id, 'partstorage': partstorage.id, 'count': 4, 'organization': org.id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/admin/kitpartstorage/')
        data = {'kit': kit.id, 'partstorage': partstorage.id, 'count': 'sending explosion...', 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {'name': 'banana'}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        kit = Kit.objects.filter(tenant__organization=org).first()
        data = {'kit': kit.id, 'partstorage': partstorage.id, 'count': 4, 'organization': org.id}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_kitpartstorage_change(self):
        # Test the change view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='change_kitpartstorage')
        kps = KitPartStorage.objects.filter(tenant__organization=org).last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, kps.id), permission, 'tenant/admin/update.html')
        data = {'kit': kps.kit.id, 'partstorage': kps.partstorage.id, 'count': 8, 'organization': kps.tenant.organization}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, kps.id), data, permission)
        response = self.client.post('{}change/{}/'.format(self.baseurl, kps.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {'name': 'banana'}
        response = self.client.post('{}change/{}/'.format(self.baseurl, kps.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        kps = KitPartStorage.objects.filter(tenant__organization=org).last()
        response = self.client.post('{}change/{}/'.format(self.baseurl, kps.id), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        
    def test_view_kitpartstorage_delete(self):
        # Test the delete view
        org = helpers.get_user_organizations(User.objects.get(username='testuser')).first()
        permission = Permission.objects.get(codename='delete_kitpartstorage')
        kps = KitPartStorage.objects.filter(tenant__organization=org).last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, kps.id), permission)
        org = Organization.objects.exclude(id__in=helpers.get_user_organizations(User.objects.get(username='testuser'))).first()
        kps = KitPartStorage.objects.filter(tenant__organization=org).last()
        response = self.client.get('{}delete/{}/'.format(self.baseurl, kps.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

class TenantOrganizationAdminViewTests(TestCase):
    def setUp(self):
        for i in range(140,150):
            if i != 140:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        user = User.objects.create(username='testuser', is_staff=True)
        self.baseurl = '/tenant/admin/organization/'

    def test_view_organization_view(self):
        user = User.objects.get_or_create(username='testuser2')[0]
        permission = Permission.objects.get(codename='view_organization')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/')
        self.client.logout()
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/tenant_view.html')

    def test_view_organization_create(self):
        user = User.objects.get_or_create(username='testuser2')[0]
        permission = Permission.objects.get(codename='add_organization')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}create/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/')
        self.client.logout()
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/tenant_create.html')
        data = {'name': 'Test_View_Organization_New', 'description': 'This is a new view TestCase test organization'}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/tenant/admin/')
        data = {'name': 'Test_View_Organization_Bad'}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertTemplateUsed(response, 'tenant/admin/tenant_create.html')

    def test_view_organization_change(self):
        org = Organization.objects.first()
        user = User.objects.get_or_create(username='testuser2')[0]
        permission = Permission.objects.get(codename='change_organization')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}change/{}/'.format(self.baseurl, org.id))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/')
        self.client.logout()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, org.id), permission, 'tenant/admin/tenant_update.html')
        data = {'name': 'New Organization Name', 'description': 'The new description of this organization'}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, org.id), data, permission)
        self.client.post('{}change/{}/'.format(self.baseurl, org.id), data)
        data = {'count': 'banana'}
        response = self.client.post('{}change/{}/'.format(self.baseurl, org.id), data)
        self.assertTemplateUsed(response, 'tenant/admin/tenant_update.html')

    def test_view_organization_delete(self):
        org = Organization.objects.last()
        user = User.objects.get_or_create(username='testuser2')[0]
        permission = Permission.objects.get(codename='delete_organization')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}delete/{}/'.format(self.baseurl, org.id))
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.logout()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, org.id), permission)

class TenantUserOrganizationAdminViewTests(TestCase):
    def setUp(self):
        for i in range(150,155):
            if i != 150:
                if randint(0,1) == 1:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
                else:
                    Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization', parent=Organization.objects.first())
            else:
                Organization.objects.create(name='Test_View_Organization_{}'.format(i), description='This is a view TestCase test organization')
        for org in Organization.objects.all():
            for i in range(0,randint(5,10)):
                user = User.objects.create(username='testuser{}-{}'.format(org.id, i), first_name='test', last_name='user{}-{}'.format(org.id, i))
                UserOrganization.objects.create(user=user,organization=org)
        user = User.objects.create(username='testuser', is_staff=True)
        self.baseurl = '/tenant/admin/userorganization/'

    def test_view_userorganization_view(self):
        user = User.objects.get_or_create(username='testuser2')[0]
        permission = Permission.objects.get(codename='view_userorganization')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/')
        self.client.logout()
        TestGenerics().get(self, self.baseurl, permission, 'tenant/admin/tenant_view.html')

    def test_view_userorganization_create(self):
        user = User.objects.get_or_create(username='testuser2')[0]
        permission = Permission.objects.get(codename='add_userorganization')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}create/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/inventory/')
        self.client.logout()
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tenant/admin/tenant_create.html')
        i = randint(150,154)
        org = Organization.objects.get(name='Test_View_Organization_{}'.format(i))
        user = UserOrganization.objects.exclude(organization=org).first().user
        data = {'user': user.id, 'organization': org.id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(response.url, '/tenant/admin/')
        data = {'user': 0}
        response = self.client.post('{}create/'.format(self.baseurl), data)
        self.assertTemplateUsed(response, 'tenant/admin/tenant_create.html')

    def test_view_userorganization_delete(self):
        userorg = UserOrganization.objects.last()
        user = User.objects.get_or_create(username='testuser2')[0]
        permission = Permission.objects.get(codename='delete_userorganization')
        user.user_permissions.add(permission)
        self.client.force_login(user)
        response = self.client.get('{}delete/{}/'.format(self.baseurl, userorg.id))
        self.assertTemplateUsed(response, 'inventory/ajax_result.html')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.logout()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, userorg.id), permission)

class TestGenerics():
    def get(self, case, url, perm, temp):
        response = case.client.get(url)
        case.assertRedirects(response, '/accounts/login/?next={}'.format(url))
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertTemplateUsed(response, temp)

    def post(self, case, url, data, perm):
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.post(url, data)
        case.assertNotEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        case.assertNotEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        case.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.assertNotEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        case.assertNotEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, case, url, perm):
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertTemplateUsed(response, 'inventory/ajax_result.html')
        case.assertContains(response, 'green')

