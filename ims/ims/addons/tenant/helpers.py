from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q
from django.db.models.base import ModelBase
from django.forms.models import ModelChoiceField
from itertools import chain
import logging

from ims.addons.tenant.api.serializers import PartReadSerializer, OrganizationSerializer, UserOrganizationSerializer, StorageReadSerializer, AlternateSKUReadSerializer, PartAlternateSKUReadSerializer, PartStorageReadSerializer, AssemblyReadSerializer, KitReadSerializer, KitPartStorageReadSerializer
from ims.addons.tenant.models import Organization, UserOrganization, TenantPart, TenantAlternateSKU, TenantAssembly, TenantKit, TenantKitPartStorage, TenantPartAlternateSKU, TenantPartStorage, TenantStorage
from inventory.admin.forms import PartForm, StorageForm, AlternateSKUForm, KitForm, AssemblyForm, PartStorageForm, PartAlternateSKUForm, KitPartStorageForm
from inventory.api.serializers import PartSerializer, StorageSerializer, AlternateSKUSerializer, PartAlternateSKUSerializer, PartStorageSerializer, AssemblySerializer, KitSerializer, KitPartStorageSerializer
from inventory.models import Part, Storage, AlternateSKU, Kit, Assembly, PartStorage, PartAlternateSKU, KitPartStorage
from log.logger import getLogger

logger = logging.getLogger(__name__)
logger2 = getLogger('inventory')

def lookup_dictionary(dic, v):
    for k in dic:
        if k[0] == v:
            return k[1]
    return 'unknown'

def rlookup_dictionary(dic, v):
    for k in dic:
        if k[1] == v:
            return k[0]
    return ''

# Return list of Organizations that specified user is assigned
def get_user_organizations(user):
    orgs = UserOrganization.objects.filter(user=user)
    org_list = []
    for org in orgs:
        if org.organization.enabled:
            if org.organization.id not in org_list:
                org_list.append(org.organization.id)
    return Organization.objects.filter(id__in=org_list)

# Creat a TenantObject for the specified object and organization
def create_tenant_object(obj, org):
    logger.debug('create_tenant_object called')
    if obj.__class__.__name__ == 'AlternateSKU':
        TenantAlternateSKU.objects.create(alternatesku=obj,organization=org)
    if obj.__class__.__name__ == 'Assembly':
        TenantAssembly.objects.create(assembly=obj,organization=org)
    if obj.__class__.__name__ == 'Kit':
        TenantKit.objects.create(kit=obj,organization=org)
    if obj.__class__.__name__ == 'KitPartStorage':
        TenantKitPartStorage.objects.create(kitpartstorage=obj,organization=org)
    if obj.__class__.__name__ == 'Part':
        TenantPart.objects.create(part=obj,organization=org)
    if obj.__class__.__name__ == 'PartAlternateSKU':
        TenantPartAlternateSKU.objects.create(partalternatesku=obj,organization=org)
    if obj.__class__.__name__ == 'PartStorage':
        TenantPartStorage.objects.create(partstorage=obj,organization=org)
    if obj.__class__.__name__ == 'Storage':
        TenantStorage.objects.create(storage=obj,organization=org)
    logger.debug('create_tenant_object completed')

# Return parsed permission name
def get_admin_permission(app, action, model):
    return '{}.{}_{}'.format(app, action, model.lower())

# Return a specific object by id
def get_object(objtype, objid):
    logger.debug('get_object called')
    try:
        if objtype == 'AlternateSKU':
            return AlternateSKU.objects.get(id=objid)
        if objtype == 'Assembly':
            return Assembly.objects.get(id=objid)
        if objtype == 'Part':
            return Part.objects.get(id=objid)
        if objtype == 'PartAlternateSKU':
            return PartAlternateSKU.objects.get(id=objid)
        if objtype == 'PartStorage':
            return PartStorage.objects.get(id=objid)
        if objtype == 'Storage':
            return Storage.objects.get(id=objid)
        if objtype == 'Kit':
            return Kit.objects.get(id=objid)
        if objtype == 'KitPartStorage':
            return KitPartStorage.objects.get(id=objid)
        return ''
    except Exception as err:
        if type(err) in [AlternateSKU.DoesNotExist, Assembly.DoesNotExist, Kit.DoesNotExist, 
                         KitPartStorage.DoesNotExist, Part.DoesNotExist, PartAlternateSKU.DoesNotExist,
                         PartStorage.DoesNotExist, Storage.DoesNotExist]:
            logger.warning('Unable to locate {} with id {}. Error message: {}'.format(objtype, objid, err))
        else:
            logger.warning('There was an unknown problem in processing get_object')
            logger.debug(err)
        raise
    finally:
        logger.debug('get_object completed')

# Return list of objects of the specified type
def get_object_list(objtype, orgs):
    logger.debug('get_object_list called')
    try:
        if objtype == 'AlternateSKU':
            return AlternateSKU.objects.filter(tenant__organization__in=orgs)
        if objtype == 'Assembly':
            return Assembly.objects.filter(tenant__organization__in=orgs)
        if objtype == 'Part':
            return Part.objects.filter(tenant__organization__in=orgs)
        if objtype == 'PartAlternateSKU':
            return PartAlternateSKU.objects.filter(tenant__organization__in=orgs)
        if objtype == 'PartStorage':
            return PartStorage.objects.filter(tenant__organization__in=orgs)
        if objtype == 'Storage':
            return Storage.objects.filter(tenant__organization__in=orgs)
        if objtype == 'Kit':
            return Kit.objects.filter(tenant__organization__in=orgs)
        if objtype == 'KitPartStorage':
            return KitPartStorage.objects.filter(tenant__organization__in=orgs)
        return ''
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_object_list')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_object_list completed')

# Return list of fields for use with Search queries
def get_search_fields(model):
    logger.debug('get_search_fields called')
    try:
        if model == 'AlternateSKU':
            return ['manufacturer', 'sku']
        if model == 'Assembly':
            return ['parent__name', 'parent__sku', 'parent__description', 'part__name', 'part__sku', 'part__description']
        if model == 'Part':
            return ['name', 'sku', 'description']
        if model == 'PartAlternateSKU':
            return ['part__name', 'part__sku', 'part__description', 'alt_sku__manufacturer', 'alt_sku__sku']
        if model == 'PartStorage':
            return ['part__name', 'part__sku', 'part__description', 'storage__name', 'storage__description']
        if model == 'Storage':
            return ['name', 'description']
        if model == 'Kit':
            return ['name', 'sku', 'description']
        if model == 'KitPartStorage':
            return ['kit__name', 'kit__sku', 'kit__description', 'partstorage__part__name', 'partstorage__part__sku', 'partstorage__part__description', 'partstorage__storage__name', 'partstorage__storage__description']
        return []
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_search_fields')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_search_fields completed')
    

# Return a Form object for a Patch/Post/Put request
def get_admin_form_p(model, data=None, instance=None):
    logger.debug('get_admin_form_p called')
    try:
        if model == 'AlternateSKU':
            form = AlternateSKUForm(data=data,instance=instance)
        if model == 'Assembly':
            form = AssemblyForm(data=data,instance=instance)
        if model == 'Part':
            form = PartForm(data=data,instance=instance)
        if model == 'PartAlternateSKU':
            form = PartAlternateSKUForm(data=data,instance=instance)
        if model == 'PartStorage':
            form = PartStorageForm(data=data,instance=instance)
        if model == 'Storage':
            form = StorageForm(data=data,instance=instance)
        if model == 'Kit':
            form = KitForm(data=data,instance=instance)
        if model == 'KitPartStorage':
            form = KitPartStorageForm(data=data,instance=instance)
        return form
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_admin_form_p')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_admin_form_p completed')

# Return a Form object for a Get request
def get_admin_form_g(model, orgs, GET=None, instance=None):
    logger.debug('get_admin_form_g called')
    try:
        if model == 'AlternateSKU':
            form = AlternateSKUForm(initial=GET,instance=instance)
        if model == 'Assembly':
            form = AssemblyForm(initial=GET,instance=instance)
            queryset = Part.objects.filter(tenant__organization__in=orgs)
            form.fields['part'] = ModelChoiceField(queryset=queryset)
            form.fields['parent'] = ModelChoiceField(queryset=queryset)
        if model == 'Part':
            form = PartForm(instance=instance)
        if model == 'PartAlternateSKU':
            form = PartAlternateSKUForm(initial=GET,instance=instance)
            queryset = Part.objects.filter(tenant__organization__in=orgs)
            form.fields['part'] = ModelChoiceField(queryset=queryset)
            queryset = AlternateSKU.objects.filter(tenant__organization__in=orgs)
            form.fields['alt_sku'] = ModelChoiceField(queryset=queryset)
        if model == 'PartStorage':
            form = PartStorageForm(initial=GET,instance=instance)
            queryset = Part.objects.filter(tenant__organization__in=orgs)
            form.fields['part'] = ModelChoiceField(queryset=queryset)
            queryset = Storage.objects.filter(tenant__organization__in=orgs)
            form.fields['storage'] = ModelChoiceField(queryset=queryset)
        if model == 'Storage':
            form = StorageForm(initial=GET,instance=instance)
            queryset = Storage.objects.filter(tenant__organization__in=orgs)
            form.fields['parent'] = ModelChoiceField(queryset=queryset)
        if model == 'Kit':
            form = KitForm(initial=GET,instance=instance)
        if model == 'KitPartStorage':
            form = KitPartStorageForm(initial=GET,instance=instance)
            queryset = PartStorage.objects.filter(tenant__organization__in=orgs)
            form.fields['partstorage'] = ModelChoiceField(queryset=queryset)
            queryset = Kit.objects.filter(tenant__organization__in=orgs)
            form.fields['kit'] = ModelChoiceField(queryset=queryset)
        return form
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_admin_form_g')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_admin_form_g completed')

# Return the name of an object from the form data
def get_form_object_name(form):
    logger.debug('get_form_object_name called')
    try:
        if form.__class__.__name__ == 'AlternateSKU':
            return '{}-{}'.format(form.fields['manufacturer'], form.fields['sku'])
        if form.__class__.__name__ == 'AssemblyForm':
            return '{}-{}'.format(form.fields['parent'], form.fields['part'])
        if form.__class__.__name__ == 'KitForm':
            return form.fields['name']
        if form.__class__.__name__ == 'KitPartStorageForm':
            return '{}-{}'.format(form.fields['kit'], form.fields['partstorage'])
        if form.__class__.__name__ == 'PartForm':
            return form.fields['name']
        if form.__class__.__name__ == 'PartAlternateSKUForm':
            return '{}-{}'.format(form.fields['part'], form.fields['alt_sku'])
        if form.__class__.__name__ == 'PartStorageForm':
            return '{}-{}'.format(form.fields['part'], form.fields['storage'])
        if form.__class__.__name__ == 'StorageForm':
            return form.fields['name']
        return ''
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_form_object_name')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_form_object_name completed')


# Return the object type Serializer
def get_serializer(obj, requestdata, partial=False):
    logger.debug('get_serializer called')
    try:
        serializer = None
        if obj.__class__.__name__ == 'str':
            if obj == 'AlternateSKU': serializer = AlternateSKUSerializer(data=requestdata)
            if obj == 'Assembly': serializer = AssemblySerializer(data=requestdata)
            if obj == 'Part': serializer = PartSerializer(data=requestdata)
            if obj == 'PartAlternateSKU': serializer = PartAlternateSKUSerializer(data=requestdata)
            if obj == 'PartStorage': serializer = PartStorageSerializer(data=requestdata)
            if obj == 'Storage': serializer = StorageSerializer(data=requestdata)
            if obj == 'Kit': serializer = KitSerializer(data=requestdata)
            if obj == 'KitPartStorage': serializer = KitPartStorageSerializer(data=requestdata)
        else:
            if obj.__class__.__name__ == 'AlternateSKU':
                serializer = AlternateSKUSerializer(obj, data=requestdata, partial=partial)
            if obj.__class__.__name__ == 'Assembly':
                serializer = AssemblySerializer(obj, data=requestdata, partial=partial)
            if obj.__class__.__name__ == 'Part':
                serializer = PartSerializer(obj, data=requestdata, partial=partial)
            if obj.__class__.__name__ == 'PartAlternateSKU':
                serializer = PartAlternateSKUSerializer(obj, data=requestdata, partial=partial)
            if obj.__class__.__name__ == 'PartStorage':
                serializer = PartStorageSerializer(obj, data=requestdata, partial=partial)
            if obj.__class__.__name__ == 'Storage':
                serializer = StorageSerializer(obj, data=requestdata, partial=partial)
            if obj.__class__.__name__ == 'Kit':
                serializer = KitSerializer(obj, data=requestdata, partial=partial)
            if obj.__class__.__name__ == 'KitPartStorage':
                serializer = KitPartStorageSerializer(obj, data=requestdata, partial=partial)
        return serializer
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_serializer')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_serializer completed')

# Return the object type Serializer
def get_serializer_data(model, objects, many=False):
    logger.debug('get_serializer_data called')
    try:
        serializer = None
        if model == 'AlternateSKU':
            serializer = AlternateSKUSerializer(objects,many=many)
        if model == 'Assembly':
            serializer = AssemblySerializer(objects,many=many)
        if model == 'Part':
            serializer = PartSerializer(objects,many=many)
        if model == 'PartAlternateSKU':
            serializer = PartAlternateSKUSerializer(objects,many=many)
        if model == 'PartStorage':
            serializer = PartStorageSerializer(objects,many=many)
        if model == 'Storage':
            serializer = StorageSerializer(objects,many=many)
        if model == 'Kit':
            serializer = KitSerializer(objects,many=many)
        if model == 'KitPartStorage':
            serializer = KitPartStorageSerializer(objects,many=many)
        return serializer.data
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_serializer_data')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_serializer_data completed')

# Return the object type ReadSerializer
def get_readserializer_data(model, objects, many=False):
    logger.debug('get_readserializer_data called')
    try:
        serializer = None
        if model == 'AlternateSKU':
            serializer = AlternateSKUReadSerializer(objects,many=many)
        if model == 'Assembly':
            serializer = AssemblyReadSerializer(objects,many=many)
        if model == 'Part':
            serializer = PartReadSerializer(objects,many=many)
        if model == 'PartAlternateSKU':
            serializer = PartAlternateSKUReadSerializer(objects,many=many)
        if model == 'PartStorage':
            serializer = PartStorageReadSerializer(objects,many=many)
        if model == 'Storage':
            serializer = StorageReadSerializer(objects,many=many)
        if model == 'Kit':
            serializer = KitReadSerializer(objects,many=many)
        if model == 'KitPartStorage':
            serializer = KitPartStorageReadSerializer(objects,many=many)
        return serializer.data
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_readserializer_data')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_readserializer_data completed')

# Return bool if provided object is an int
def is_int(i):
    try:
        int(i)
        return True
    except ValueError:
        return False
    except TypeError:
        return False

# Return the query type for use with Search
def parse_querytype(value):
    if value == 1 or value == '1':
        return '^'
    if value == 2 or value == '2':
        return '='
    return ''

def child_storage_loop(storage, child_ids = []):
    # infinite loop protection counter
    counter = 0
    for child in storage.get_children():
        if child.id not in child_ids:
            child_ids.append(child.id)
            counter += 1
    if counter == 0:
        # possible loop, just return
        return child_ids
    for child in storage.get_children():
        subchild_ids = child_storage_loop(child, child_ids)
        if subchild_ids != child_ids:
            for subchild in subchild_ids:
                if subchild not in child_ids:
                    child_ids.append(subchild)
    return child_ids
