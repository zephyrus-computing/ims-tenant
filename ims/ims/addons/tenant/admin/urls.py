from django.urls import re_path
from ims.addons.tenant.admin import views

app_name='ims.addons.tenant.admin'
urlpatterns = [
    re_path(r'^$', views.tenant_home, name='home'),
    re_path(r'^organization/$', views.admin_organization_view, name='admin_organization_view'),
    re_path(r'^organization/create/$', views.admin_organization_create, name='admin_organization_create'),
    re_path(r'^organization/change/(?P<id>[\d]+)/$', views.admin_organization_change, name='admin_organization_change'),
    re_path(r'^organization/delete/(?P<id>[\d]+)/$', views.ajax_organization_delete, name='ajax_organization_delete'),
    re_path(r'^userorganization/$', views.admin_userorganization_view, name='admin_userorganization_view'),
    re_path(r'^userorganization/create/$', views.admin_userorganization_create, name='admin_userorganization_create'),
    re_path(r'^userorganization/delete/(?P<id>[\d]+)/$', views.ajax_userorganization_delete, name='ajax_userorganization_delete'),
]
