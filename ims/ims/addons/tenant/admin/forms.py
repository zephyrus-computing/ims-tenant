from django.forms import ModelForm

from inventory.models import Part, Assembly, Storage, AlternateSKU, PartStorage, PartAlternateSKU, Kit, KitPartStorage
from ims.addons.tenant.models import Organization, UserOrganization, TenantPart
import ims.addons.tenant.helpers as helpers

class PartForm(ModelForm):
    class Meta:
        model = Part
        exclude = ['created', 'lastmodified']

class TenantForm(ModelForm):
    class Meta:
        model = TenantPart
        fields = ['organization']

    def __init__(self, user=None, **kwargs):
        super().__init__(**kwargs)
        if user:
            orgs = helpers.get_user_organizations(user)
            self.fields['organization'].queryset = Organization.objects.filter(id__in=orgs)
        else:
            self.fields['organization'].queryset = Organization.objects.none()

class TenantPOSTForm(ModelForm):
     class Meta:
         model = TenantPart
         fields = ['organization']

class TenantViewOnlyForm(ModelForm):
    class Meta:
        model = TenantPart
        fields = ['organization']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.initial:
            self.fields['organization'].queryset = Organization.objects.filter(id=self.initial['organization'])
        else:
            self.fields['organization'].queryset = Organization.objects.none()
        self.fields['organization'].disabled = True

class AssemblyForm(ModelForm):
    class Meta:
        model = Assembly
        exclude = ['created', 'lastmodified']

class StorageForm(ModelForm):
    class Meta:
        model = Storage
        exclude = ['created', 'lastmodified']

class AlternateSKUForm(ModelForm):
    class Meta:
        model = AlternateSKU
        exclude = ['created', 'lastmodified']

class PartStorageForm(ModelForm):
    class Meta:
        model = PartStorage
        exclude = ['created', 'lastmodified']

class PartAlternateSKUForm(ModelForm):
    class Meta:
        model = PartAlternateSKU
        exclude = ['created', 'lastmodified']

class KitForm(ModelForm):
    class Meta:
        model = Kit
        exclude = ['created', 'lastmodified']

class KitPartStorageForm(ModelForm):
    class Meta:
        model = KitPartStorage
        exclude = ['created', 'lastmodified']

class OrganizationForm(ModelForm):
    class Meta:
        model = Organization
        exclude = ['created', 'lastmodified']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['parent'].required = False

class UserOrganizationForm(ModelForm):
    class Meta:
        model = UserOrganization
        exclude = ['created', 'lastmodified']

