from django import forms, template
from django.urls import reverse
from django.utils.safestring import mark_safe
from ims.addons.tenant.models import Organization, UserOrganization
register = template.Library()

@register.filter("organizations")
def get_organizations(user):
    user_orgs = UserOrganization.objects.filter(user=user)
    orgs = []
    for user_org in user_orgs:
        if user_org.organization.id not in orgs:
            orgs.append(user_org.organization.id)
    return Organization.objects.filter(id__in=orgs)
