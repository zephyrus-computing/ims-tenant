from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from itertools import chain
from pip._internal.operations.freeze import freeze
import logging

from inventory.search import Search
from inventory.models import Part, Storage, AlternateSKU, PartStorage, PartAlternateSKU, Assembly, Kit, KitPartStorage
from inventory.forms import ModelSearchForm
from ims.addons.tenant.models import Organization, UserOrganization
from log.logger import getLogger
import ims.addons.tenant.helpers as helpers

# Configure logger
logger = logging.getLogger(__name__)
logger2 = getLogger('inventory')

@permission_required('inventory.view_part')
# Retrieve all of the Parts and display a table
def part_list(request):
    logger.debug('Request made to part_list view')
    # Get all Parts
    logger2.debug(request.user, 'Request to view Part List')
    logger.debug('Generating object_list of Part')
    logmsg = 'Accessed Part List'
    orgs = helpers.get_user_organizations(request.user)
    object_list = Part.objects.filter(tenant__organization__in=orgs)
    if not settings.SHOW_DELETED_OBJECTS:
        object_list = object_list.exclude(name='deleted')

    # Create the Search filter and apply to the existing results
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of Part with query')
        querytype = request.GET.get('t')
        qt = helpers.parse_querytype(querytype)
        sf = Search().filter(['name', 'sku', 'description'], qt + query)
        object_list = object_list.filter(sf)

    # Create the Paginator and get the existing page
    logger.debug('Paginate object_list of Part')
    paginator = Paginator(object_list, settings.PART_PAGE_SIZE)
    page = request.GET.get('page')

    # Try to display the page or catch and process the errors
    try:
        parts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        parts = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
        parts = paginator.page(paginator.num_pages)
    # Return the list of parts
    logger.info('Rendering request for part_list view')
    logger2.info(request.user, logmsg)
    return render(request, 'inventory/part/list.html', {'query': query, 'page': page, 'parts': parts})

@permission_required('inventory.view_part')
# Retrieve information about the specified Part
def part_detail(request, id):
    logger.debug('Request made to part_detail view')
    logger2.debug(request.user, 'Request to view Part Details for id {}'.format(id))
    # Get the Part object to display in detail
    logger.debug('Getting requested Part')
    orgs = helpers.get_user_organizations(request.user)
    p = get_object_or_404(Part, id=id)
    if not p.tenant.organization in orgs:
        logger.debug('User not authorized to view this part')
        logger2.warning(request.user, 'User not authorized to view Part Detail for id {}'.format(id))
        raise PermissionDenied
    logger.info('Rendering request for part_detail view')
    logger2.info(request.user, 'Accessed Part id {}'.format(id))
    return render(request, 'inventory/part/detail.html', {'part': p})

@permission_required('inventory.view_storage')
# Retrieve all of the Storage containers and display them in a table
def storage_list(request):
    logger.debug('Request made to storage_list view')
    logger2.debug(request.user, 'Request to view Storage List')
    logmsg = 'Accessed Storage List'
    # Get all Storage
    logger.debug('Generating object_list of Storage')
    orgs = helpers.get_user_organizations(request.user)
    object_list = Storage.objects.filter(tenant__organization__in=orgs)
    if not settings.SHOW_DELETED_OBJECTS:
        object_list = object_list.exclude(name='deleted')

    # Create the Search filter and apply to the existing results
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of Storage with query')
        querytype = request.GET.get('t')
        qt = helpers.parse_querytype(querytype)
        sf = Search().filter(['name', 'description'], qt + query)
        object_list = object_list.filter(sf)

    # Create the Paginator and get the current page
    logger.debug('Paginate object_list of Storage')
    paginator = Paginator(object_list, settings.STORAGE_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process errors
    try:
        containers = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        containers = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
        containers = paginator.page(paginator.num_pages)
    # Return the list of containers
    logger.info('Rendering request for storage_list view')
    logger2.info(request.user, logmsg)
    return render(request, 'inventory/storage/list.html', {'query': query, 'page': page, 'containers': containers})

@permission_required('inventory.view_storage')
# Retrieve information about the specified container
def storage_detail(request, id):
    logger.debug('Request made to storage_detail view')
    logger2.debug(request.user, 'Request to view Storage Details for id {}'.format(id))
    # Get the Storage object to display in detail
    logger.debug('Getting requested Storage')
    orgs = helpers.get_user_organizations(request.user)
    s = get_object_or_404(Storage, id=id)
    if not s.tenant.organization in orgs:
        logger.debug('User not authorized to view this storage')
        logger2.warning(request.user, 'User not authorized to view Storage Detail for id {}'.format(id))
        raise PermissionDenied
    logger.debug('Getting previous and next Storage objects')
    sp = Storage.objects.filter(id__lt=s.id, tenant__organization__in=orgs).order_by('-id').first()
    if sp == None:
        sp = Storage.objects.filter(tenant__organization__in=orgs).order_by('-id').first()
    sn = Storage.objects.filter(id__gt=s.id, tenant__organization__in=orgs).order_by('id').first()
    if sn == None:
        sn = Storage.objects.filter(tenant__organization__in=orgs).order_by('id').first()
    # Return the Storage Details
    logger.info('Rendering request for storage_detail view')
    logger2.info(request.user, 'Accessed Storage id {}'.format(id))
    return render(request, 'inventory/storage/detail.html', {'container': s, 'next': sn.get_absolute_url, 'previous': sp.get_absolute_url})

@permission_required('inventory.view_storage')
# Retrieve all of the Storage containers and display them in a tree
def storage_hierarchy_list(request):
    logger.debug('Request made to storage_hierarchy_list view')
    logger2.debug(request.user, 'Request to view Storage Hierarchy List')
    # Get all Storage
    logger.debug('Generating object_list of Storage')
    orgs = helpers.get_user_organizations(request.user)
    object_list = Storage.objects.filter(parent=None,tenant__organization__in=orgs)
           
    # Return the list of containers
    logger.info('Rendering request for storage_hierarchy view')
    logger2.info(request.user, 'Accessed Storage Hierarchy List')
    return render(request, 'inventory/storage/hierarchy_list.html', {'containers': object_list})

@permission_required('inventory.view_storage')
# Retrieve information about the specified Storage container in tree summary
def storage_hierarchy_detail(request, id):
    logger.debug('Request made to storage_hierarchy_detail view')
    logger2.debug(request.user, 'Request to view Storage Hierarchy Details for id {}'.format(id))
    # Get the Storage object to display in detail
    logger.debug('Getting requested Storage')
    orgs = helpers.get_user_organizations(request.user)
    s = get_object_or_404(Storage, id=id)
    if not s.tenant.organization in orgs:
        logger.debug('User not authorized to view this storage')
        logger2.warning(request.user, 'User not authorized to view hierarchy detail for Storage id {}'.format(id))
        raise PermissionDenied
    sp = Storage.objects.filter(id__lt=s.id, tenant__organization__in=orgs).order_by('-id').first()
    if sp == None:
        sp = Storage.objects.filter(tenant__organization__in=orgs).order_by('-id').first()
    sn = Storage.objects.filter(id__gt=s.id, tenant__organization__in=orgs).order_by('id').first()
    if sn == None:
        sn = Storage.objects.filter(tenant__organization__in=orgs).order_by('id').first()
    pss = {}
    children_ids = helpers.child_storage_loop(s, [s.id])
    if children_ids is not None:
        for ps in PartStorage.objects.filter(storage_id__in=children_ids):
            if ps.part.id not in pss.keys():
                if ps.tenant.organization in orgs:
                    pss[ps.part.id] = ps.count
            else:
                pss[ps.part.id] = ps.count + pss[ps.part.id]
    # Get list of Parts
    parts = Part.objects.filter(id__in=pss.keys())
    # Return the Storage details
    logger.info('Rendering request for storage_hierarchy_detail view')
    logger2.info(request.user, 'Accessed Storage Hierarchy for id {}'.format(id))
    return render(request, 'inventory/storage/hierarchy_detail.html', {'container': s, 'partstoragesummary': pss, 'parts': parts, 'next': sn.get_container_url(), 'previous': sp.get_container_url()})

@permission_required('inventory.view_alternatesku')
# Retrieve all of the Alternate SKUs and display them in a table
def altsku_list(request):
    logger.debug('Request made to altsku_list view')
    logger2.debug(request.user, 'Request to view AlternateSKU List')
    logmsg = 'Accessed AlternateSku List'
    # Get all AlternateSKUs
    logger.debug('Generating object_list of AlternateSKU')
    orgs = helpers.get_user_organizations(request.user)
    object_list = AlternateSKU.objects.filter(tenant__organization__in=orgs)
    if not settings.SHOW_DELETED_OBJECTS:
        object_list = object_list.exclude(sku='deleted')

    # Create the Search filter and apply to the existing results
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of AlternateSKU with query')
        querytype = request.GET.get('t')
        qt = helpers.parse_querytype(querytype)
        sf = Search().filter(['manufacturer', 'sku'], qt + query)
        object_list = object_list.filter(sf)

    # Create the Paginator and get the current page
    logger.debug('Paginate object_list of AlternateSKU')
    paginator = Paginator(object_list, settings.ALTSKU_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process errors
    try:
        alternates = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        alternates = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
        alternates = paginator.page(paginator.num_pages)
    # Return the list of alternates
    logger.debug('Rendering request for altsku_list view')
    logger2.info(request.user, logmsg)
    return render(request, 'inventory/altsku/list.html', {'query': query, 'page': page, 'alternates': alternates})

@permission_required('inventory.view_assembly')
# Retrieve all of the Assemblies and display them in a table
def assembly_list(request):
    logger.debug('Request made to assembly_list view')
    logger2.debug(request.user, 'Request to view Assembly List')
    logmsg = 'Accessed Assembly List'
    # Get all Assemblies
    logger.debug('Generating object_list of Assembly')
    orgs = helpers.get_user_organizations(request.user)
    objects = list(set(Assembly.objects.values_list('parent', flat=True)))
    object_list = Part.objects.filter(pk__in=objects,tenant__organization__in=orgs)
    if not settings.SHOW_DELETED_OBJECTS:
        object_list = object_list.exclude(name='deleted')

    # Create the Search filter and apply to the existing results
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of Assembly with query')
        querytype = request.GET.get('t')
        qt = helpers.parse_querytype(querytype)
        sf = Search().filter(['name', 'sku', 'description'], qt + query)
        object_list = object_list.filter(sf)

    # Create the Paginator and get the current page
    logger.debug('Paginate object_list of Assembly')
    paginator = Paginator(object_list, settings.ASSEMBLY_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process errors
    try:
        assemblies = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        assemblies = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
        assemblies = paginator.page(paginator.num_pages)
    # Return the list of alternates
    logger.debug('Rendering request for assembly_list view')
    logger2.info(request.user, logmsg)
    return render(request, 'inventory/assembly/list.html', {'query': query, 'page': page, 'assemblies': assemblies})

@permission_required('inventory.view_assembly')
# Retrieve information about the specified Assembly
def assembly_detail(request, id):
    logger.debug('Request made to assembly_detail view')
    logger2.debug(request.user, 'Request to view Assembly id {}'.format(id))
    # Get the Assembly object to display in detail
    logger.debug('Getting requested Assembly')
    orgs = helpers.get_user_organizations(request.user)
    a = get_object_or_404(Part, id=id)
    if not a.tenant.organization in orgs:
        logger.debug('User not authorized to view this assembly')
        logger2.warning(request.user, 'User not authorized to view Assembly id {}'.format(id))
        raise PermissionDenied
    logger.debug('Rendering request for assembly_detail view')
    logger2.info(request.user, 'Accessed Assembly id {}'.format(id))
    return render(request, 'inventory/assembly/detail.html', {'assembly': a})

@permission_required('inventory.view_kit')
# Retrieve all of the Assemblies and display them in a table
def kit_list(request):
    cname = 'kit_list'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Kit list')
    logmsg = 'Accessed Kit List'
    # Get all Kits
    logger.debug('Generating object_list of Kit')
    orgs = helpers.get_user_organizations(request.user)
    object_list = Kit.objects.filter(tenant__organization__in=orgs)
    if not settings.SHOW_DELETED_OBJECTS:
        object_list = object_list.exclude(name='deleted')

    # Create the Search filter and apply to the existing results
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of Assembly with query')
        querytype = request.GET.get('t')
        qt = helpers.parse_querytype(querytype)
        sf = Search().filter(['name', 'sku', 'description'], qt + query)
        object_list = object_list.filter(sf)
            
    # Create the Paginator and get the current page
    logger.debug('Paginate object_list of Kit')
    paginator = Paginator(object_list, settings.KIT_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process errors
    try:
        kits = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        kits = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        kits = paginator.page(paginator.num_pages)
    # Return the list of alternates
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'inventory/kit/list.html', {'query': query, 'page': page, 'kits': kits})

@permission_required('inventory.view_kit')
def kit_detail(request, id):
    cname = 'kit_detail'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view Kit id {}'.format(id))
    # Get the Kit object to display in detail
    logger.debug('Getting requested Kit')
    orgs = helpers.get_user_organizations(request.user)
    k = get_object_or_404(Kit, id=id)
    if not k.tenant.organization in orgs:
        logger.debug('User not authorized to view this kit')
        logger2.warning(request.user, 'User not authorized to view Kit id {}'.format(id))
        raise PermissionDenied
    logger.debug('Getting previous and next Kit objects')
    kp = Kit.objects.filter(id__lt=k.id, tenant__organization__in=orgs).order_by('-id').first()
    if kp == None:
        kp = Kit.objects.filter(tenant__organization__in=orgs).order_by('-id').first()
    kn = Kit.objects.filter(id__gt=k.id, tenant__organization__in=orgs).order_by('id').first()
    if kn == None:
        kn = Kit.objects.filter(tenant__organization__in=orgs).order_by('id').first()
    # Return the Storage Details
    logger.info('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Accessed Kit id {}'.format(id))
    return render(request, 'inventory/kit/detail.html', {'kit': k, 'next': kn.get_absolute_url, 'previous': kp.get_absolute_url})

@permission_required('inventory.decrement_kit')
def ajax_kit_subtract(request, id, inc):
    cname = 'ajax_kit_subtract'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to subtract {} from Kit id {}'.format(inc, id))
    logger.debug('Getting requested Kit')
    orgs = helpers.get_user_organizations(request.user)
    kit = get_object_or_404(Kit, id=id)
    if not kit.tenant.organization in orgs:
        logger.debug('User not authorized to modify this kit')
        logger2.warning(request.user, 'User not authorized to modify Kit id {}'.format(id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is Denied.'})
    try:
        logger.debug('Subtracting {} for Kit id {}'.format(inc, kit.id))
        kit.subtract(inc)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Subtracted {} from Kit id {}'.format(inc, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Update successful'})
    except ValueError:
        logger.debug('Unable to subtract 1 for Kit id {}'.format(kit.id))
        logger2.warning(request.user, 'Insufficient parts to subtract {} from Kit id {}'.format(inc, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Insufficient parts'})
    except Exception as err:
        logger.error(err)
        logger2.error(request.user, err)
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Unknown error'})

@permission_required('inventory.increment_kit')
def ajax_kit_add(request, id, inc):
    cname = 'ajax_kit_add'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to add {} to Kit id {}'.format(inc, id))
    logger.debug('Getting requested Kit')
    orgs = helpers.get_user_organizations(request.user)
    kit = get_object_or_404(Kit, id=id)
    if not kit.tenant.organization in orgs:
        logger.debug('User not authorized to modify this kit')
        logger2.warning(request.user, 'User not authorized to modify Kit id {}'.format(id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is Denied.'})
    try:
        logger.debug('Adding {} for Kit id {}'.format(inc, kit.id))
        kit.add(inc)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Added {} to Kit id {}'.format(inc, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Update successful'})
    except Exception as err:
        logger.error(err)
        logger2.error(request.user, err)
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Unknown error'})

@permission_required('inventory.view_kit')
def ajax_kit_update(request, id, section):
    cname = 'ajax_kit_update'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to retrieve {} updates for Kit id {}'.format(section, id))
    orgs = helpers.get_user_organizations(request.user)
    kit = get_object_or_404(Kit, id=id)
    if not kit.tenant.organization in orgs:
        logger.debug('User not authorized to view this kit')
        logger2.warning(request.user, 'User not authorized to view section \'{}\' of Kit id {}'.format(section, id))
        raise PermissionDenied
    logger2.info(request.user, 'Accessed {} updates for Kit id {}'.format(section, id))
    return render(request, 'inventory/ajax_result2.html', {'type': section, 'kit': kit})

@permission_required('inventory.increment_partstorage')
def ajax_partstorage_add(request, id, inc):
    cname = 'ajax_partstorage_add'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to add {} to PartStorage id {}'.format(inc, id))
    logger.debug('Getting requested PartStorage'.format(cname))
    orgs = helpers.get_user_organizations(request.user)
    ps = get_object_or_404(PartStorage, id=id)
    if not ps.tenant.organization in orgs:
        logger.debug('User not authorized to modify this partstorage')
        logger2.warning(request.user, 'User not authorized to modify PartStorage id {}'.format(id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is Denied.'})
    try:
        logger.debug('Adding {} for PartStorage id {}'.format(inc, ps.id))
        ps.add(inc)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Added {} to PartStorage id {}'.format(inc, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Update successful'})
    except Exception as err:
        logger.debug('Unable to add {} for PartStorage id {}'.format(inc, ps.id))
        logger2.error(request.uer, err)
        logger2.warning(request.user, 'Unable to add {} to PartStorage id {}'.format(inc, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count.'})

@permission_required('inventory.decrement_partstorage')
def ajax_partstorage_sub(request, id, inc):
    cname = 'ajax_partstorage_sub'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to subtract {} to PartStorage id {}'.format(inc, id))
    logger.debug('Getting requested PartStorage'.format(cname))
    orgs = helpers.get_user_organizations(request.user)
    ps = get_object_or_404(PartStorage, id=id)
    if not ps.tenant.organization in orgs:
        logger.debug('User not authorized to modify this partstorage')
        logger2.warning(request.user, 'User not authorized to modify PartStorage id {}'.format(id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Access is Denied.'})
    try:
        logger.debug('Subtracting {} for PartStorage id {}'.format(inc, ps.id))
        ps.subtract(inc)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Subtracted {} to PartStorage id {}'.format(inc, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'green', 'result': 'Update successful'})
    except ValueError:
        logger.debug('Unable to subtract {} for Kit id {}'.format(inc, ps.id))
        logger2.warning(request.user, 'Insufficient parts to subtract {} from PartStorage id {}'.format(inc, id))
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Insufficient parts'})
    except Exception as err:
        logger.error(err)
        logger2.error(request.user, err)
        return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to modify count. Unknown error'})

@permission_required('inventory.view_partstorage')
def ajax_partstorage_update(request, id, section):
    cname = 'ajax_partstorage_update'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to retrieve {} updates for PartStorage id {}'.format(section, id))
    orgs = helpers.get_user_organizations(request.user)
    ps = get_object_or_404(PartStorage, id=id)
    if not ps.tenant.organization in orgs:
        logger.debug('User not authorized to view this partstorage')
        logger2.warning(request.user, 'User not authorized to view section \'{}\' of PartStorage id {}'.format(section, id))
        raise PermissionDenied
    logger2.info(request.user, 'Accessed {} updates for PartStorage id {}'.format(section, id))
    return render(request, 'inventory/ajax_result2.html', {'type': section, 'ps': ps})

@login_required()
def search(request):
    logger.debug('Request made to search view')
    logger2.debug(request.user, 'Request to Search page')
    search_form = ModelSearchForm(data=request.GET)
    if search_form.is_valid():
        logger.debug('Generating search parameters')
        orgs = helpers.get_user_organizations(request.user)
        querytype = helpers.parse_querytype(request.GET.get('t'))
        objecttypes = request.GET.getlist('m')
        terms = request.GET.get('q')
        raw = ''
        if 'part' in objecttypes and request.user.has_perm('inventory.view_part'):
            logger.debug('Searching for Part')
            sf = Search().filter([querytype + 'name', querytype + 'sku', querytype + 'description'], terms)
            raw = chain(raw, Part.objects.filter(sf,tenant__organization__in=orgs))
        if 'assembly' in objecttypes and request.user.has_perm('inventory.view_assembly'):
            logger.debug('Searching for Assembly')
            sf = Search().filter([querytype + 'name', querytype + 'sku', querytype + 'description'], terms)
            assemblies = Assembly.objects.all()
            ids = []
            for i in range(assemblies.count()):
                j = assemblies[i].parent_id
                if j not in ids:
                    ids.append(j)
            raw = chain(raw, Part.objects.filter(id__in=ids,tenant__organization__in=orgs).filter(sf))
        if 'kit' in objecttypes and request.user.has_perm('inventory.view_kit'):
            logger.debug('Searching for Kit')
            sf = Search().filter([querytype + 'name', querytype + 'sku', querytype + 'description'], terms)
            raw = chain(raw, Kit.objects.filter(sf,tenant__organization__in=orgs))
        if 'alternatesku' in objecttypes and request.user.has_perm('inventory.view_alternatesku'):
            logger.debug('Searching for AlternateSKU')
            sf = Search().filter([querytype + 'sku', querytype + 'manufacturer'], terms)
            raw = chain(raw, AlternateSKU.objects.filter(sf,tenant__organization__in=orgs))
        if 'storage' in objecttypes and request.user.has_perm('inventory.view_storage'):
            logger.debug('Searching for Storage')
            sf = Search().filter([querytype + 'name', querytype + 'description'], terms)
            raw = chain(raw, Storage.objects.filter(sf,tenant__organization__in=orgs))
        logger.debug('Removing duplicates and sorting results')
        unique = set(raw)
        results = sorted(list(unique), key=lambda r: str(r))
        logger.debug('Rendering request for search view')
        logger2.info(request.user, 'Accessed Search results of objects {} for query {}'.format(','.join(objecttypes), terms))
        return render(request, 'inventory/search.html', {'search_form': search_form, 'results': results})
    else:
        logger.debug('Generating new SearchForm and Rendering request for search view')
        logger2.info(request.user, 'Accessed Search Page')
        search_form = ModelSearchForm()
        return render(request, 'inventory/search.html', {'search_form': search_form})

# Retrieve information to display About page
@login_required()
def about_ims(request):
    logger.debug('Request made to about_ims view')
    logger2.debug(request.user, 'Request to view About page')
    if request.user.is_staff:
        buildnumber = '{} build {}'.format(settings.IMS_VERSION, settings.IMS_BUILD)
        pip_list = []
        logger.debug('Generating pip_list')
        for i in settings.INSTALLED_ADDONS:
            pip_list.append('IMS Addon|{}=={}-{}'.format(i['name'], i['version'], i['build']))
        for i in freeze(local_only=True):
            pip_list.append(i)
        for i in settings.THIRD_PARTY_LIBRARIES:
            pip_list.append('{}|{}=={}'.format(i['vendor'], i['package'], i['version']))
        logger.debug('Rendering request for about_ims view')
        logger2.info(request.user, 'Accessed About Page')
        return render(request, 'inventory/about.html', {'buildnumber': buildnumber, 'pip_list': pip_list})
    logger.debug('User is not authorized to view the about page. Redirecting to home.')
    logger2.warning(request.user, 'User is not authorized to view the About Page')
    return redirect('/inventory/')

@login_required()
def ajax(request, model):
    logger.debug('Request made to ajax view')
    logger2.debug(request.user, 'Request to ajax view for list of {}'.format(model))
    items = ''
    selected = request.GET.get('selected') # Organization__id
    org = Organization.objects.get(id=selected)
    orgs = helpers.get_user_organizations(request.user)
    if not org in orgs:
        logger.debug('User not authorized for Organization')
        logger2.warning(request.user, 'User not authorized for Organization id {}'.format(selected))
        raise PermissionDenied
    if model == "part" and request.user.has_perm('inventory.view_part'):
        logger.debug('Generating a list of Part objects')
        items = Part.objects.filter(tenant__organization=org)
    if model == "storage" and request.user.has_perm('inventory.view_storage'):
        logger.debug('Generating a list of Storage objects')
        items = Storage.objects.filter(tenant__organization=org)
    if model == "kit" and request.user.has_perm('inventory.view_kit'):
        logger.debug('Generating a list of Kit objects')
        items = Kit.objects.filter(tenant__organization=org)
    if model == "partstorage" and request.user.has_perm('inventory.view_partstorage'):
        logger.debug('Generating a list of PartStorage objects')
        items = PartStorage.objects.filter(tenant__organization=org)
    if model == 'alternatesku' and request.user.has_perm('inventory.view_alternatesku'):
        logger.debug('Generating a list of AlternateSKU objects')
        items = AlternateSKU.objects.filter(tenant__organization=org)
    logger.debug('Rendering request for ajax view')
    logger2.info(request.user, 'Accessed list of {} for Organization id {}'.format(model, selected))
    return render(request, 'tenant/dropdown_list_options.html', {'items': items})




