from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from itertools import chain
import logging

from inventory.models import Part, Storage, PartStorage, AlternateSKU, PartAlternateSKU, Assembly, Kit, KitPartStorage

logger = logging.getLogger(__name__)

class Organization(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=256)
    enabled = models.BooleanField(default=True)
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, null=True)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for Organization id {}'.format(self.id))
        return reverse('ims.addons.tenant:organization_detail', args=[self.id])

    #def get_users(self):

    #def get_suborg(self):

class UserOrganization(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['organization','user']

    def __str__(self):
        return str('{}-{}'.format(self.organization, self.user))

class TenantPart(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    part = models.OneToOneField(Part, on_delete=models.CASCADE, related_name='tenant')

    class Meta:
        ordering = ['organization','part']

class TenantStorage(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    storage = models.OneToOneField(Storage, on_delete=models.CASCADE, related_name='tenant')
    
    class Meta:
        ordering = ['organization','storage']

class TenantPartStorage(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    partstorage = models.OneToOneField(PartStorage, on_delete=models.CASCADE, related_name='tenant')

    class Meta:
        ordering = ['organization','partstorage']

class TenantAlternateSKU(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    alternatesku = models.OneToOneField(AlternateSKU, on_delete=models.CASCADE, related_name='tenant')
    
    class Meta:
        ordering = ['organization','alternatesku']

class TenantPartAlternateSKU(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    partalternatesku = models.OneToOneField(PartAlternateSKU, on_delete=models.CASCADE, related_name='tenant')
    
    class Meta:
        ordering = ['organization','partalternatesku']

class TenantAssembly(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    assembly = models.OneToOneField(Assembly, on_delete=models.CASCADE, related_name='tenant')

    class Meta:
        ordering = ['organization','assembly']

class TenantKit(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    kit = models.OneToOneField(Kit, on_delete=models.CASCADE, related_name='tenant')
    
    class Meta:
        ordering = ['organization','kit']

class TenantKitPartStorage(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    kitpartstorage = models.OneToOneField(KitPartStorage, on_delete=models.CASCADE, related_name='tenant')
    
    class Meta:
        ordering = ['organization','kitpartstorage']

