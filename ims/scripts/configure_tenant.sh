#!/bin/bash
folder=$(dirname $0) # Should be /etc/zc/ims/scripts
parentfolder=$(dirname $folder)
if [[ ! -f "$folder/configure_tenant.py" ]]; then
    echo "ERROR: Missing files";
    exit 1;
fi
logfile="/var/log/zc/ims/install.log";
if [[ -f "$parentfolder/.env" ]]; then
    if [[ $(grep -e "LOG_DIR" $parentfolder/.env) ]]; then
        logfile=$(echo $(grep -e "LOG_DIR" $envfile | cut -d'=' -f2 | sed -e "s/\/$//" -e "s/'//g")"/install.log");
    fi
fi
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log"
fi
echo "$(date)" >> $logfile
echo "Configuring Initial Tenant..." >> $logfile
su -c "cd; source $parentfolder/venv/bin/activate; python $parentfolder/manage.py shell --command \"exec(open('$parentfolder/scripts/configure_tenant.py').read())\"; deactivate;" imsuser | tee -a $logfile
