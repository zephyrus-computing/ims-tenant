#!/usr/bin/env python

from django.contrib.auth.models import User
from ims.addons.tenant.models import Organization,UserOrganization,TenantPart,TenantStorage,TenantAlternateSKU,TenantPartStorage,TenantPartAlternateSKU,TenantAssembly,TenantKit,TenantKitPartStorage
from inventory.models import Part,Storage,AlternateSKU,PartStorage,PartAlternateSKU,Assembly,Kit,KitPartStorage

name = description = None
while (name is None or name == "") or (description == "" or description is None):
    name = input("Organization Name: ")
    description = input("Organization Description: ")
org = Organization.objects.get_or_create(name=name,description=description)[0]
for user in User.objects.all():
    prompt = input("Add {} to Organization {}? (Y/n):".format(user.username, org.name))
    if prompt == "" or prompt.lower() == "y":
        print("Adding {} to Organization {}".format(user.username, org.name))
        UserOrganization.objects.create(user=user,organization=org)
    else:
        print("Skipping user {}".format(user.username))
    prompt = ""
try:
    prompt = input("Default all untied objects to Organization {}? (Y/n): ".format(org.name))
    parts = Part.objects.filter(tenant=None)
    storages = Storage.objects.filter(tenant=None)
    altskus = AlternateSKU.objects.filter(tenant=None)
    partstorages = PartStorage.objects.filter(tenant=None)
    partaltskus = PartAlternateSKU.objects.filter(tenant=None)
    assemblies = Assembly.objects.filter(tenant=None)
    kits = Kit.objects.filter(tenant=None)
    kitpartstorages = KitPartStorage.objects.filter(tenant=None)
    if prompt == "" or prompt.lower() == "y":
        print("\x1b[92mUpdating Part\x1b[39m")
        for part in parts:
            TenantPart.objects.create(organization=org,part=part)
        print("\x1b[92mUpdating Storage\x1b[39m")
        for storage in storages:
            TenantStorage.objects.create(organization=org,storage=storage)
        print("\x1b[92mUpdating AlternateSKU\x1b[39m")
        for altsku in altskus:
            TenantAlternateSKU.objects.create(organization=org,alternatesku=altsku)
        print("\x1b[92mUpdating PartStorage\x1b[39m")
        for ps in partstorages:
            TenantPartStorage.objects.create(organization=org,partstorage=ps)
        print("\x1b[92mUpdating PartAlternateSKU\x1b[39m")
        for pa in partaltskus:
            TenantPartAlternateSKU.objects.create(organization=org,partalternatesku=pa)
        print("\x1b[92mUpdating Assembly\x1b[39m")
        for assembly in assemblies:
            TenantAssembly.objects.create(organization=org,assembly=assembly)
        print("\x1b[92mUpdating Kit\x1b[39m")
        for kit in kits:
            TenantKit.objects.create(organization=org,kit=kit)
        print("\x1b[92mUpdating KitPartStorage\x1b[39m")
        for kitps in kitpartstorages:
            TenantKitPartStorage.objects.create(organization=org,kitpartstorage=kitps)
    elif prompt.lower() == "n":
        print("\x1b[92mReviewing Parts\x1b[39m")
        print("\x1b[94mSelect 'y' to add the listed part to the new Organization\x1b[39m")
        print("\x1b[94mSelect 'n' to skip the listed part\x1b[39m")
        print("\x1b[94mSelect 'd' to display more details about the listed part\x1b[39m")
        for part in parts:
            subprompt = ""
            while subprompt == "":
                print("\x1b[96mAdd Part '{}' to Organization {}?\x1b[39m".format(part.name, org.name))
                subprompt = input("y/n/d: ")
                if subprompt.lower() == "d":
                    print("\x1b[96mId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCost: '{}'\nCreated: '{}'\nLast Modified: '{}'\x1b[39m".format(part.id, part.name, part.description, part.sku, part.price, part.cost, part.created, part.lastmodified))
                    subprompt = ""
                if subprompt.lower() != "y" and subprompt.lower() != "n" and subprompt.lower() != "d" and subprompt != "":
                    subprompt = ""
            if subprompt.lower() == "y":
                TenantPart.objects.create(organization=org,part=part)
        print("\x1b[92mReviewing Storage\x1b[39m")
        print("\x1b[94mSelect 'y' to add the listed storage to the new Organization\x1b[39m")
        print("\x1b[94mSelect 'n' to skip the listed storage\x1b[39m")
        print("\x1b[94mSelect 'd' to display more details about the listed storage\x1b[39m")
        for storage in storages:
            subprompt = ""
            while subprompt == "":
                print("\x1b[96mAdd Storage '{}' to Organization {}?\x1b[39m".format(storage.name, org.name))
                subprompt = input("y/n/d: ")
                if subprompt.lower() == "d":
                    print("\x1b[96mId: '{}'\nName: '{}'\nDescription: '{}'\nParent: '{}'\nCreated: '{}'\nLast Modified: '{}'\x1b[39m".format(storage.id, storage.name, storage.description, storage.parent, storage.created, storage.lastmodified))
                    subprompt = ""
                if subprompt.lower() != "y" and subprompt.lower() != "n" and subprompt.lower() != "d" and subprompt != "":
                    subprompt = ""
            if subprompt.lower() == "y":
                TenantStorage.objects.create(organization=org,storage=storage)
        print("\x1b[92mReviewing AlternateSKU\x1b[39m")
        print("\x1b[94mSelect 'y' to add the listed altsku to the new Organization\x1b[39m")
        print("\x1b[94mSelect 'n' to skip the listed altsku\x1b[39m")
        print("\x1b[94mSelect 'd' to display more details about the listed altsku\x1b[39m")
        for altsku in altskus:
            subprompt = ""
            while subprompt == "":
                print("\x1b[96mAdd AlternateSKU '{}' to Organization {}?\x1b[39m".format(altsku.sku, org.name))
                subprompt = input("y/n/d: ")
                if subprompt.lower() == "d":
                    print("\x1b[96mId: '{}'\nManufacturer: '{}'\nSKU: '{}'\nLast Modified: '{}'\x1b[39m".format(altsku.id, altsku.manufacturer, altsku.sku, altsku.created, altsku.lastmodified))
                    subprompt = ""
                if subprompt.lower() != "y" and subprompt.lower() != "n" and subprompt.lower() != "d" and subprompt != "":
                    subprompt = ""
            if subprompt.lower() == "y":
                TenantAlternateSKU.objects.create(organization=org,alternatesku=altsku)
        print("\x1b[92mReviewing PartStorage\x1b[39m")
        print("\x1b[94mSelect 'y' to add the listed partstorage to the new Organization\x1b[39m")
        print("\x1b[94mSelect 'n' to skip the listed partstorage\x1b[39m")
        print("\x1b[94mSelect 'd' to display more details about the listed partstorage\x1b[39m")
        for ps in partstorages:
            ps.refresh_from_db()
            subprompt = ""
            validation = 0
            try:
                test = ps.part.tenant.organization
                validation += 1
            except Part.tenant.RelatedObjectDoesNotExist:
                print("\x1b[91mWarning! Part id {} is not bound to an Organization. Adding this PartStorage to Organization {} will lead to instability and data leakage.\x1b[39m".format(ps.part.id, org.name))
            try:
                test = ps.storage.tenant.organization
                validation += 1
            except Storage.tenant.RelatedObjectDoesNotExist:
                print("\x1b[91mWarning! Storage id {} is not bound to an Organization. Adding this PartStorage to Organization {} will lead to instability and data leakage.\x1b[39m".format(ps.storage.id, org.name))
            if validation == 2:
                if ps.part.tenant.organization != ps.storage.tenant.organization:
                    print("\x1b[91mWarning! Part id {} and Storage id {} are not part of the same Organization. Adding this PartStorage to Organization {} will lead to instability and data leakage.\x1b[39m".format(ps.part.id, ps.storage.id, org.name))
            while subprompt == "":
                print("\x1b[96mAdd PartStorage '{}' to Organization {}?\x1b[39m".format(ps, org.name))
                subprompt = input("y/n/d: ")
                if subprompt.lower() == "d":
                    try:
                        print("\x1b[96m__PART__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCost: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: '{}'\x1b[39m".format(ps.part.id, ps.part.name, ps.part.description, ps.part.sku, ps.part.price, ps.part.cost, ps.part.created, ps.part.lastmodified, ps.part.tenant.organization))
                    except Part.tenant.RelatedObjectDoesNotExist:
                        print("\x1b[96m__PART__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCost: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: N/A\x1b[39m".format(ps.part.id, ps.part.name, ps.part.description, ps.part.sku, ps.part.price, ps.part.cost, ps.part.created, ps.part.lastmodified))
                    try:
                        print("\x1b[96m__STORAGE__\nId: '{}'\nName: '{}'\nDescription: '{}'\nParent: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: '{}'\x1b[39m".format(ps.storage.id, ps.storage.name, ps.storage.description, ps.storage.parent, ps.storage.created, ps.storage.lastmodified, ps.storage.tenant.organization))
                    except Storage.tenant.RelatedObjectDoesNotExist:
                        print("\x1b[96m__STORAGE__\nId: '{}'\nName: '{}'\nDescription: '{}'\nParent: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: N/A\x1b[39m".format(ps.storage.id, ps.storage.name, ps.storage.description, ps.storage.parent, ps.storage.created, ps.storage.lastmodified))
                    print("\x1b[96m__PARTSTORAGE__\nId: '{}'\nCount: '{}'\nCreated: '{}'\nLast Modified: '{}'\x1b[39m".format(ps.id, ps.count, ps.created, ps.lastmodified))
                    subprompt = ""
                if subprompt.lower() != "y" and subprompt.lower() != "n" and subprompt.lower() != "d" and subprompt != "":
                    subprompt = ""
            if subprompt.lower() == "y":
                TenantPartStorage.objects.create(organization=org,partstorage=ps)
        print("\x1b[92mReviewing PartAlternateSKU\x1b[39m")
        print("\x1b[94mSelect 'y' to add the listed partalternatesku to the new Organization\x1b[39m")
        print("\x1b[94mSelect 'n' to skip the listed partalternatesku\x1b[39m")
        print("\x1b[94mSelect 'd' to display more details about the listed partalternatesku\x1b[39m")
        for pa in partaltskus:
            pa.refresh_from_db()
            subprompt = ""
            validation = 0
            try:
                test = pa.part.tenant.organization
                validation += 1
            except Part.tenant.RelatedObjectDoesNotExist:
                print("\x1b[91mWarning! Part id {} is not bound to an Organization. Adding this PartAlternateSKU to Organization {} will lead to instability and data leakage.\x1b[39m".format(pa.part.id, org.name))
            try:
                test = pa.alt_sku.tenant.organization
                validation += 1
            except AlternateSKU.tenant.RelatedObjectDoesNotExist:
                print("\x1b[91mWarning! AlternateSKU id {} is not bound to an Organization. Adding this PartAlternateSKU to Organization {} will lead to instability and data leakage.\x1b[39m".format(pa.alt_sku.id, org.name))
            if validation == 2:
                if pa.part.tenant.organization != pa.alt_sku.tenant.organization:
                    print("\x1b[91mWarning! Part id {} and AlternateSKU id {} are not part of the same Organization. Adding this PartAlternateSKU to Organization {} will lead to instability and data leakage.\x1b[39m".format(pa.part.id, pa.alt_sku.id, org.name))
            while subprompt == "":
                print("\x1b[96mAdd PartAlternateSKU '{}' to Organization {}?\x1b[39m".format(pa, org.name))
                subprompt = input("y/n/d: ")
                if subprompt.lower() == "d":
                    try:
                        print("\x1b[96m__PART__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCost: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: '{}'\x1b[39m".format(pa.part.id, pa.part.name, pa.part.description, pa.part.sku, pa.part.price, pa.part.cost, pa.part.created, pa.part.lastmodified, pa.part.tenant.organization))
                    except Part.tenant.RelatedObjectDoesNotExist:
                        print("\x1b[96m__PART__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCost: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: N/A\x1b[39m".format(pa.part.id, pa.part.name, pa.part.description, pa.part.sku, pa.part.price, pa.part.cost, pa.part.created, pa.part.lastmodified))
                    try:
                        print("\x1b[96m__ALTERNATESKU__\nId: '{}'\nManufacturer: '{}'\nSKU: '{}'\nLast Modified: '{}'\nOrganization: '{}'\x1b[39m".format(pa.alt_sku.id, pa.alt_sku.manufacturer, pa.alt_sku.sku, pa.alt_sku.created, pa.alt_sku.lastmodified, pa.alt_sku.tenant.organization))
                    except AlternateSKU.tenant.RelatedObjectDoesNotExist:
                        print("\x1b[96m__ALTERNATESKU__\nId: '{}'\nManufacturer: '{}'\nSKU: '{}'\nLast Modified: '{}'\nOrganization: N/A\x1b[39m".format(pa.alt_sku.id, pa.alt_sku.manufacturer, pa.alt_sku.sku, pa.alt_sku.created, pa.alt_sku.lastmodified))
                    print("\x1b[96m__PARTALTERNATESKU__\nId: '{}'\nCreated: '{}'\nLast Modified: '{}'\x1b[39m".format(pa.id, pa.created, pa.lastmodified))
                    subprompt = ""
                if subprompt.lower() != "y" and subprompt.lower() != "n" and subprompt.lower() != "d" and subprompt != "":
                    subprompt = ""
            if subprompt.lower() == "y":
                TenantPartAlternateSKU.objects.create(organization=org,partalternatesku=pa)
        print("\x1b[92mReviewing Assembly\x1b[39m")
        print("\x1b[94mSelect 'y' to add the listed assembly to the new Organization\x1b[39m")
        print("\x1b[94mSelect 'n' to skip the listed assembly\x1b[39m")
        print("\x1b[94mSelect 'd' to display more details about the listed assembly\x1b[39m")
        for assembly in assemblies:
            assembly.refresh_from_db()
            subprompt = ""
            validation = 0
            try:
                test = assembly.parent.tenant.organization
                validation += 1
            except Part.tenant.RelatedObjectDoesNotExist:
                print("\x1b[91mWarning! Parent Part id {} is not bound to an Organization. Adding this Assembly to Organization {} will lead to instability and data leakage.\x1b[39m".format(assembly.parent.id, org.name))
            try:
                test = assembly.part.tenant.organization
                validation += 1
            except Part.tenant.RelatedObjectDoesNotExist:
                print("\x1b[91mWarning! Part id {} is not bound to an Organization. Adding this Assembly to Organization {} will lead to instability and data leakage.\x1b[39m".format(assembly.part.id, org.name))
            if validation == 2:
                if assembly.parent.tenant.organization != assembly.part.tenant.organization:
                    print("\x1b[91mWarning! Parent Part id {} and Part id {} are not part of the same Organization. Adding this Assembly to Organization {} will lead to instability and data leakage.\x1b[39m".format(assembly.parent.id, assembly.part.id, org.name))
            while subprompt == "":
                print("\x1b[96mAdd Assembly '{}' to Organization {}?\x1b[39m".format(assembly, org.name))
                subprompt = input("y/n/d: ")
                if subprompt.lower() == "d":
                    try:
                        print("\x1b[96m__PARENT__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCost: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: '{}'\x1b[39m".format(assembly.parent.id, assembly.parent.name, assembly.parent.description, assembly.parent.sku, assembly.parent.price, assembly.parent.cost, assembly.parent.created, assembly.parent.lastmodified, assembly.parent.tenant.organization))
                    except Part.tenant.RelatedObjectDoesNotExist:
                        print("\x1b[96m__PARENT__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCost: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: N/A\x1b[39m".format(assembly.parent.id, assembly.parent.name, assembly.parent.description, assembly.parent.sku, assembly.parent.price, assembly.parent.cost, assembly.parent.created, assembly.parent.lastmodified))
                    try:
                        print("\x1b[96m__PART__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCost: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: '{}'\x1b[39m".format(assembly.part.id, assembly.part.name, assembly.part.description, assembly.part.sku, assembly.part.price, assembly.part.cost, assembly.part.created, assembly.part.lastmodified, assembly.part.tenant.organization))
                    except Part.tenant.RelatedObjectDoesNotExist:
                        print("\x1b[96m__PART__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCost: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: N/A\x1b[39m".format(assembly.part.id, assembly.part.name, assembly.part.description, assembly.part.sku, assembly.part.price, assembly.part.cost, assembly.part.created, assembly.part.lastmodified))
                    print("\x1b[96m__ASSEMBLY__\nId: '{}'\nCount: '{}'\nCreated: '{}'\nLast Modified: '{}'\x1b[39m".format(assembly.id, assembly.count, assembly.created, assembly.lastmodified))
                    subprompt = ""
                if subprompt.lower() != "y" and subprompt.lower() != "n" and subprompt.lower() != "d" and subprompt != "":
                    subprompt = ""
            if subprompt.lower() == "y":
                TenantAssembly.objects.create(organization=org,assembly=assembly)
        print("\x1b[92mReviewing Kit\x1b[39m")
        print("\x1b[94mSelect 'y' to add the listed kit to the new Organization\x1b[39m")
        print("\x1b[94mSelect 'n' to skip the listed kit\x1b[39m")
        print("\x1b[94mSelect 'd' to display more details about the listed kit\x1b[39m")
        for kit in kits:
            subprompt = ""
            while subprompt == "":
                print("\x1b[96mAdd Kit '{}' to Organization {}?\x1b[39m".format(kit.name, org.name))
                subprompt = input("y/n/d: ")
                if subprompt.lower() == "d":
                    print("\x1b[96mId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCreated: '{}'\nLast Modified: '{}'\x1b[39m".format(kit.id, kit.name, kit.description, kit.sku, kit.price, kit.created, kit.lastmodified))
                    subprompt = ""
                if subprompt.lower() != "y" and subprompt.lower() != "n" and subprompt.lower() != "d" and subprompt != "":
                    subprompt = ""
            if subprompt.lower() == "y":
                TenantKit.objects.create(organization=org,kit=kit)
        print("\x1b[92mReviewing KitPartStorage\x1b[39m")
        print("\x1b[94mSelect 'y' to add the listed kitpartstorage to the new Organization\x1b[39m")
        print("\x1b[94mSelect 'n' to skip the listed kitpartstorage\x1b[39m")
        print("\x1b[94mSelect 'd' to display more details about the listed kitpartstorage\x1b[39m")
        for kps in kitpartstorages:
            kps.refresh_from_db()
            subprompt = ""
            validation = 0
            try:
                test = kps.kit.tenant.organization
                validation += 1
            except Kit.tenant.RelatedObjectDoesNotExist:
                print("\x1b[91mWarning! Kit id {} is not bound to an Organization. Adding this KitPartStorage to Organization {} will lead to instability and data leakage.\x1b[39m".format(kps.kit.id, org.name))
            try:
                test = kps.partstorage.tenant.organization
                validation += 1
            except PartStorage.tenant.RelatedObjectDoesNotExist:
                print("\x1b[91mWarning! PartStorage id {} is not bound to an Organization. Adding this KitPartStorage to Organization {} will lead to instability and data leakage.\x1b[39m".format(kps.partstorage.id, org.name))
            if validation == 2:
                if kps.kit.tenant.organization != kps.partstorage.tenant.organization:
                    print("\x1b[91mWarning! Kit id {} and PartStorage id {} are not part of the same Organization. Adding this KitPartStorage to Organization {} will lead to instability and data leakage.\x1b[39m".format(kps.kit.id, kps.partstorage.id, org.name))
            while subprompt == "":
                print("\x1b[96mAdd KitPartStorage '{}' to Organization {}?\x1b[39m".format(kps, org.name))
                subprompt = input("y/n/d: ")
                if subprompt.lower() == "d":
                    try:
                        print("\x1b[96m__KIT__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: '{}'\x1b[39m".format(kps.kit.id, kps.kit.name, kps.kit.description, kps.kit.sku, kps.kit.price, kps.kit.created, kps.kit.lastmodified, kps.kit.tenant.organization))
                    except Kit.tenant.RelatedObjectDoesNotExist:
                        print("\x1b[96m__KIT__\nId: '{}'\nName: '{}'\nDescription: '{}'\nSku: '{}'\nPrice: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: N/A\x1b[39m".format(kps.kit.id, kps.kit.name, kps.kit.description, kps.kit.sku, kps.kit.price, kps.kit.created, kps.kit.lastmodified))
                    try:
                        print("\x1b[96m__PARTSTORAGE__\nId: '{}'\nPart Id: '{}'\nStorage Id: '{}'\nCount: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: '{}'\x1b[39m".format(kps.partstorage.id, kps.partstorage.part.id, kps.partstorage.storage.id, kps.partstorage.count, kps.partstorage.created, kps.partstorage.lastmodified, kps.partstorage.tenant.organization))
                    except PartStorage.tenant.RelatedObjectDoesNotExist:
                        print("\x1b[96m__PARTSTORAGE__\nId: '{}'\nPart Id: '{}'\nStorage Id: '{}'\nCount: '{}'\nCreated: '{}'\nLast Modified: '{}'\nOrganization: N/A\x1b[39m".format(kps.partstorage.id, kps.partstorage.part.id, kps.partstorage.storage.id, kps.partstorage.count, kps.partstorage.created, kps.partstorage.lastmodified))
                    print("\x1b[96m__KITPARTSTORAGE__\nId: '{}'\nCount: '{}'\nCreated: '{}'\nLast Modified: '{}'\x1b[39m".format(kps.id, kps.count, kps.created, kps.lastmodified))
                    subprompt = ""
                if subprompt.lower() != "y" and subprompt.lower() != "n" and subprompt.lower() != "d" and subprompt != "":
                    subprompt = ""
            if subprompt.lower() == "y":
                TenantKitPartStorage.objects.create(organization=org,kitpartstorage=kps)
except Exception as e:
    print(e)
    print("\x1b[91mAn error occurred and is preventing this script for continuing. Please contact support to assist with troubleshooting.\x1b[39m")

